import QtQuick 2.10
import QtQuick.Window 2.10

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    property bool localSelected : true

    Component.onCompleted: {
         var date = new Date();
        console.log("<<<<<<<<<<<<>>>>>>>>>>>")
        console.log(date.getUTCHours());
    }

    Rectangle {
        id: buttonRect
        anchors.top: parent.top
        anchors.margins: 20
        color: "blue"
        width: parent.width - 20
        height: 40

        Rectangle {
            border.color: "white"
          //  color: localSelected ? sourceSelector.selectedBackgroundColor : sourceSelector.deselectedBackgroundColor
            anchors.top: parent.top
            anchors.left: parent.left
            width: parent.width / 2
            height: 30
            Text {
                text: "Local"
                anchors.centerIn: parent
                color: "green"
                font.pixelSize: 26
            }
        }

        Rectangle {
            border.color: "red"
//            color: !localSelected ? sourceSelector.selectedBackgroundColor : sourceSelector.deselectedBackgroundColor
            anchors.top: parent.top
            anchors.right: parent.right
            width: parent.width / 2
            height: 30
            Text {
                text: "Remote"
                anchors.centerIn: parent
                color: "blue"
                font.pixelSize: 26
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                localSelected = !localSelected
                console.log("clicked localSelected"+localSelected);
            }
        }
    }

   ListModel {
       id: localProviderPool
       ListElement {
             providerId: "Local1"
         }
       ListElement {
             providerId: "Local2"
         }
       ListElement {
             providerId: "Local3"
         }
     }

    ListModel {
       id: remoteProviderPool
       ListElement {
             providerId: "Remote1"
         }
     }

    ListView {
        id: sensorListView
        model: localSelected ? (localProviderPool)
                             : (remoteProviderPool)

        width: buttonRect.width
        anchors.top: buttonRect.bottom
        anchors.bottom: parent.bottom
        focus: true
        clip: true

        delegate: Rectangle {
            border.color: "red"
//            color: ListView.isCurrentItem ? sourceSelector.selectedBackgroundColor
//                                          : sourceSelector.deselectedBackgroundColor
            radius: 5
            height: 30
            width: parent ? parent.width : buttonRect.width
            Text {
                text: providerId.toUpperCase()
                anchors.centerIn: parent
                color: "black"
                font.pixelSize: 26
            }
            MouseArea {
                anchors.fill: parent
                onClicked: sensorListView.currentIndex = index
                onDoubleClicked: {
                    sensorListView.currentIndex = index
//                    createSensorConnection()
                }
            }
        }
    }

}
