#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qDebug() << Q_FUNC_INFO << __LINE__;
    return a.exec();
}
