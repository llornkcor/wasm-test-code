#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QNetworkConfiguration>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QAuthenticator>
#include <QDateTime>
#include <QFileDialog>

#include <QDebug>
#include <QTimer>
#include <QCoreApplication>
#include <QProcess>

// http://192.168.1.122/smartgit-18_1_4.deb

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(qnamReplyFinished(QNetworkReply*)));

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_gpPushButton_clicked()
{
    ui->progressBar->setMaximum(100);
    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(true);

    if (networkreply && networkreply->isRunning()) {
        networkreply->abort();
        return;
    }

    QNetworkRequest request(QUrl(ui->urlLineEdit->text()));

    networkreply = manager->get(request);

    connect(networkreply, SIGNAL(finished()),
            this, SLOT(requestFinished()));

    connect(networkreply, SIGNAL(downloadProgress(qint64, qint64)),
            this, SLOT(downloadProgress(qint64, qint64)));

    connect(networkreply, SIGNAL(uploadProgress(qint64, qint64)),
            this, SLOT(uploadProgress(qint64, qint64)));

    connect(networkreply,SIGNAL(error(QNetworkReply::NetworkError)),
            this,SLOT(networkReplyError(QNetworkReply::NetworkError)));

    QObject::connect(networkreply, SIGNAL(readyRead()),
                     this, SLOT(readyRead()));
}

void MainWindow::on_fileTreeView_doubleClicked(const QModelIndex &index)
{

}

void MainWindow::timeout()
{
}


void MainWindow::qnamReplyFinished(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
        qDebug() << Q_FUNC_INFO << "no error condition.";
    else {
        qDebug() << Q_FUNC_INFO << reply->error() << reply->errorString();

    }
 //   qDebug() << "try again";
  //  QTimer::singleShot(TIMEOUT,this,SLOT(timeout()));
}

void MainWindow::requestFinished()
{
    if (networkreply->error() != QNetworkReply::NoError) {
        qWarning()<< Q_FUNC_INFO  << "Error state:" << networkreply->error() << networkreply->errorString();
    } else {
        qDebug() << Q_FUNC_INFO << "no error";

    }
    //  qDebug() << Q_FUNC_INFO << reply->readAll().constData();
    //  networkreply->deleteLater();
    //   doRequest();
}

void MainWindow::networkReplyError(QNetworkReply::NetworkError code)
{
    qDebug() << Q_FUNC_INFO << code;
}

void MainWindow::readyRead()
{
  //  qDebug() << Q_FUNC_INFO;
//    QNetworkReply *const s = static_cast<QNetworkReply *>(sender());
//    qint64 bAvailable;
//    while ((bAvailable = s->bytesAvailable())) {

//        qDebug() << Q_FUNC_INFO << bAvailable;

 //       const QString line(s->readLine());
//        qDebug() << s->readLine();
        //qDebug() << s->readAll();

        //        if (line.startsWith("Content-Length:"))
        //            bodyLength = line.mid(15).toInt();

        //        if (isBody) {
        //            body.append(line);
        //            bodyBytesRead += line.length();
        //        } else if (line == "\r\n") {
        //            isBody = true;
        //            if (bodyLength == -1) {
        //                qFatal("No length was specified in the header.");
        //            }
        //        }
  //  }
//    qDebug() << s->rawHeaderPairs();

    //    qDebug() << bodyBytesRead <<  bodyLength << (bodyBytesRead == bodyLength);
    //    if (bodyBytesRead == bodyLength) {
    ////        QDomDocument domDoc;
    ////        success = domDoc.setContent(body);
    //        qDebug() << "SUCCESZS!!!";
    //    }
}

void MainWindow::metaDataChanged()
{
    qDebug() << Q_FUNC_INFO << networkreply->rawHeaderList();

}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    qDebug() << Q_FUNC_INFO << "bytesReceived" << bytesReceived << "bytesTotal" << bytesTotal;
    if (ui->progressBar->maximum() != bytesTotal)
        ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
}

void MainWindow::uploadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    qDebug() << Q_FUNC_INFO  << "bytesSent" << bytesSent << "bytesTotal" << bytesTotal;
}

void MainWindow::on_pushButton_clicked()
{
   QFileDialog::getOpenFileName(this,
        tr("Any"), "/", tr("*"));
}
