#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDnsLookup>
#include <QHostAddress>

static int typeFromParameter(const QString &type)
{
    if (type == "a")
        return QDnsLookup::A;
    if (type == "aaaa")
        return QDnsLookup::AAAA;
    if (type == "any")
        return QDnsLookup::ANY;
    if (type == "cname")
        return QDnsLookup::CNAME;
    if (type == "mx")
        return QDnsLookup::MX;
    if (type == "ns")
        return QDnsLookup::NS;
    if (type == "ptr")
        return QDnsLookup::PTR;
    if (type == "srv")
        return QDnsLookup::SRV;
    if (type == "txt")
        return QDnsLookup::TXT;
    return -1;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
      dns(new QDnsLookup(this))
{
    ui->setupUi(this);
    connect(dns, &QDnsLookup::finished, this, &MainWindow::showResults);

    // https://cloudflare-dns.com/dns-query
    // https://dns.google.com/resolve"
    // https://doh-2.seby.io/dns-query

    qputenv("QT_QPA_WASM_DNS_HTTPS", "https://doh-2.seby.io/dns-query");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->textEdit->clear();
    dns->setType(static_cast<QDnsLookup::Type>(typeFromParameter(ui->comboBox->currentText())));

    dns->setName(ui->lineEdit->text());
    dns->lookup();
}

void MainWindow::showResults()
{
    if (dns->error() != QDnsLookup::NoError) {
        printf("Error: %i (%s)\n", dns->error(), qPrintable(dns->errorString()));
        ui->textEdit->append(QString("Error: %1 (%2)\n").arg( dns->error()).arg(dns->errorString()));
    }
    // CNAME records
    const QList<QDnsDomainNameRecord> cnameRecords = dns->canonicalNameRecords();
    for (const QDnsDomainNameRecord &record : cnameRecords) {
        printf("%s\t%i\tIN\tCNAME\t%s\n", qPrintable(record.name()), record.timeToLive(), qPrintable(record.value()));
        ui->textEdit->append(QString("%1\t%2\tIN\tCNAME\t%3\n").arg(qPrintable(record.name())).arg(record.timeToLive()).arg( qPrintable(record.value())));
    }

    // A and AAAA records
    const QList<QDnsHostAddressRecord> aRecords = dns->hostAddressRecords();
    for (const QDnsHostAddressRecord &record : aRecords) {
        const char *type = (record.value().protocol() == QAbstractSocket::IPv6Protocol) ? "AAAA" : "A";
        printf("%s\t%i\tIN\t%s\t%s\n", qPrintable(record.name()), record.timeToLive(), type, qPrintable(record.value().toString()));
        ui->textEdit->append(QString("%1\t%2\tIN\t%3\t%4\n").arg(qPrintable(record.name())).arg( record.timeToLive()).arg( type).arg( qPrintable(record.value().toString())));
    }

    // MX records
    const QList<QDnsMailExchangeRecord> mxRecords = dns->mailExchangeRecords();
    for (const QDnsMailExchangeRecord &record : mxRecords) {
        printf("%s\t%i\tIN\tMX\t%u %s\n", qPrintable(record.name()), record.timeToLive(), record.preference(), qPrintable(record.exchange()));
        ui->textEdit->append(QString("%1\t%2\tIN\tMX\t%3 %4\n").arg(qPrintable(record.name())).arg( record.timeToLive()).arg( record.preference()).arg( qPrintable(record.exchange())));
    }

    // NS records
    const QList<QDnsDomainNameRecord> nsRecords = dns->nameServerRecords();
    for (const QDnsDomainNameRecord &record : nsRecords) {
        printf("%s\t%i\tIN\tNS\t%s\n", qPrintable(record.name()), record.timeToLive(), qPrintable(record.value()));
        ui->textEdit->append(QString("%1\t%2\tIN\tNS\t%3\n").arg(qPrintable(record.name())).arg(record.timeToLive()).arg( qPrintable(record.value())));
    }

    // PTR records
    const QList<QDnsDomainNameRecord> ptrRecords = dns->pointerRecords();
    for (const QDnsDomainNameRecord &record : ptrRecords) {
        printf("%s\t%i\tIN\tPTR\t%s\n", qPrintable(record.name()), record.timeToLive(), qPrintable(record.value()));
        ui->textEdit->append(QString("%1\t%2\tIN\tPTR\t%3\n").arg(qPrintable(record.name())).arg( record.timeToLive()).arg( qPrintable(record.value())));
    }

    // SRV records
    const QList<QDnsServiceRecord> srvRecords = dns->serviceRecords();
    for (const QDnsServiceRecord &record : srvRecords) {
        printf("%s\t%i\tIN\tSRV\t%u %u %u %s\n", qPrintable(record.name()), record.timeToLive(), record.priority(), record.weight(), record.port(), qPrintable(record.target()));
        ui->textEdit->append(QString("%1\t%2\tIN\tSRV\t%3 %4 %5 %6\n").arg(qPrintable(record.name())).arg( record.timeToLive()).arg( record.priority()).arg( record.weight()).arg( record.port()).arg( qPrintable(record.target())));
    }

    // TXT records
    const QList<QDnsTextRecord> txtRecords = dns->textRecords();
    for (const QDnsTextRecord &record : txtRecords) {
        QStringList values;
        const QList<QByteArray> dnsRecords = record.values();
        for (const QByteArray &ba : dnsRecords)
            values << "\"" + QString::fromLatin1(ba) + "\"";
        printf("%s\t%i\tIN\tTXT\t%s\n", qPrintable(record.name()), record.timeToLive(), qPrintable(values.join(' ')));
        ui->textEdit->append(QString("%1\t%2\tIN\tTXT\t%3\n").arg(qPrintable(record.name())).arg( record.timeToLive()).arg( qPrintable(values.join(' '))));
    }
}