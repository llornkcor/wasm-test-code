#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPointF>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPointF pointF(9999999999, 2147483647);
    QPoint point = pointF.toPoint();
    qDebug() << Q_FUNC_INFO << pointF << point;
}

MainWindow::~MainWindow()
{
    delete ui;
}
