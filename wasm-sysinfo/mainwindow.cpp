#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSysInfo>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSysInfo sysinfo;
    ui->textEdit->append("Build API: "+sysinfo.buildAbi()+"\n");
    ui->textEdit->append("Build CPU Arch: "+sysinfo.buildCpuArchitecture()+"\n");
    ui->textEdit->append("Current CPU arch: "+sysinfo.currentCpuArchitecture()+"\n");
    ui->textEdit->append("Kernel type: "+sysinfo.kernelType()+"\n");
    ui->textEdit->append("Kernel version: "+sysinfo.kernelVersion()+"\n");
    ui->textEdit->append("Machine host name: "+sysinfo.machineHostName()+"\n");
    ui->textEdit->append("Machine Unique ID: "+sysinfo.machineUniqueId()+"\n");
    ui->textEdit->append("Product name: "+sysinfo.prettyProductName()+"\n");
    ui->textEdit->append("Product type: "+sysinfo.productType()+"\n");
    ui->textEdit->append("Product version: "+sysinfo.productVersion()+"\n");
 //   ui->textEdit->append("CLang version: "+ __clang_major__ +"\n");
// __clang_major__, __clang_minor__, __clang_patchlevel__
}

MainWindow::~MainWindow()
{
    delete ui;
}

