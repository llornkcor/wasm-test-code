import QtQuick 2.2
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            console.log(qsTr('Clicked on background. Text: "' + textEdit.text + '"'))
         //   console.log(Qt.formatTime(new Date, "HH:mm"));
        }
        Component.onCompleted: {
            console.log("<><><><><><>")
            var time = new Date();
            console.log(time) //"function() { [code] }"
        }
    }
}
