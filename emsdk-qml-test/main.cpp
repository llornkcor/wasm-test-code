#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    qDebug() << Q_FUNC_INFO;

    QQuickView *view = new QQuickView;
     view->setSource(QUrl(QStringLiteral("qrc:/main.qml")));
     view->show();
     //    QQmlApplicationEngine engine;
//    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
//    if (engine.rootObjects().isEmpty())
//        return -1;

    return app.exec();
}
