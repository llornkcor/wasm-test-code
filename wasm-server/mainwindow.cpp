#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <emscripten/emscripten.h>
//#include <emscripten/html5.h>
//#include <emscripten>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    EM_ASM({
        var port = 8080;
            WebSocketServer = require('ws').Server;
            wss = new WebSocketServer({ port: port });

        console.log('listening on port: ' + port);

        wss.on('connection', function connection(ws) {

            ws.on('message', function(message) {

                console.log('message: ' + message);
                ws.send('echo: ' + message);

            });

            console.log('new client connected!');
            ws.send('connected!');

        });
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}
