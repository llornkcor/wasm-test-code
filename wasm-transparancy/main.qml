import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    color: "lightgrey"

    Rectangle {

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        width: 400; height: 200
        color: "transparent"
        clip: true
        border.color: "blue"
        border.width: 15

        Rectangle {
id: innerRect
            width: 200; height: 100
            anchors.centerIn: parent
            color: "yellow"

            border.color: "green"
            border.width: 4
        }
        DropShadow {
            width: innerRect.width
            height: innerRect.height
            anchors.centerIn: parent
            horizontalOffset: 0
            verticalOffset: 2
        //    color: Style.cfShadow
            source: innerRect
        }


    }


    Rectangle {
        id: redRect
           anchors.centerIn: parent
           width: 200; height: 200
           radius: 30
           color: "transparent"
           border.width: 4; border.color: "red"
clip: true
           Button {
               id: headerConfigButton
               display: "IconOnly"

               //anchors.top: parent.top
                anchors.centerIn: parent
               width: 100
               height: 40
               padding: 0
               Text {
                   text: "XXXX"
                   color: "white"
                   anchors.centerIn: parent
               }

               background: Rectangle {  // gradient fill
                   id: headerConfigButtonGradientFill
                   x: 2

                   width: headerConfigButton.width - 4
                   height: headerConfigButton.height - 2
// clip: true
                  // color: "red"
                   Image {
                       id: img
                       anchors.fill: parent
                       source: "qrc:///gradient-3.png"
                       fillMode: Image.Stretch
                   }
               }
           }

           layer.enabled: true
           layer.effect: DropShadow {
               anchors.centerIn: parent
               width: headerConfigButton.width
               height: headerConfigButton.height
               horizontalOffset: 10
               verticalOffset: 20
               color: "#80020402"
               source: redRect
           }
    }

//    DropShadow {
//        width: redRect.width
//        height: redRect.height
//        anchors.centerIn: redRect

//        horizontalOffset: 10
//        verticalOffset: 20
//        color: Style.cfShadow
//        source: redRect
//        radius: 50
//    }

}
