import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    width: 600
    height: 400

    header: Label {
        text: qsTr("Page 1")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        text: qsTr("You are on Page 1.")
        anchors.centerIn: parent
    }

    TextInput {
        id: textInput
        x: 14
        y: 94
        width: 336
        height: 20
        text: qsTr("Text Input")
        font.pixelSize: 12
    }
}
