import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    width: 600
    height: 400

    header: Label {
        text: qsTr("Page 2")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        width: 110
        height: 0
        text: qsTr("You are on Page 2.")
        anchors.verticalCenterOffset: -172
        anchors.horizontalCenterOffset: -231
        anchors.centerIn: parent
    }
    TextInput {
    }

    TextArea {
        id: textArea
        x: 14
        y: 37
        width: 572
        height: 297
        text: qsTr("Text Area")
    }
}
