#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QDeadlineTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    time(0)
{
    ui->setupUi(this);
    // create a timer
    timer = new QTimer(this);

    // setup signal and slot
    connect(timer, SIGNAL(timeout()),
          this, SLOT(MyTimerSlot()));

    // msec
//    timer->start(100);

     QDeadlineTimer dtimer(100);
waitForDone(dtimer);
      qDebug() << "Remaining time "<< dtimer.remainingTime();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::MyTimerSlot()
{
    ++time;
    ui->lcdNumber->display(time);
   // qDebug() << "Timer..." << time;
}

void MainWindow::waitForDone(const QDeadlineTimer &timer)
{
    qDebug() << "Remaining time "<< timer.remainingTime();
     ++time;
    ui->lcdNumber->display(time);

}
