#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QTimer>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    doRead();
//    QSettings settings("MySoft", "Star Runner");
//    it = 0;
//    it =  settings.value("bogus",it).toInt();
//    qDebug() << "bogus value is" << it;

//    qDebug() << "Setting new value for bogus";
//    settings.setValue("bogus", ++it);
//    qDebug() << Q_FUNC_INFO;
 //   QTimer::singleShot(1500,this,SLOT(doRead()));
 //   qDebug() << Q_FUNC_INFO;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::doRead()
{
    on_readButton_clicked();
//    qDebug() << Q_FUNC_INFO;
//    QSettings settings("MySoft", "Star Runner");
//    qDebug() << "Value for bogus is now:" << settings.value("bogus", it);

//    settings.setValue("bogus", ++it);
//    qDebug() << "Setting new value for bogus";
   // if (it < 10)
   //     QTimer::singleShot(1500,this,SLOT(doRead()));
}


void MainWindow::on_readButton_clicked()
{
    qDebug() << Q_FUNC_INFO;
    QSettings settings("MySoft", "Star Runner");
    qDebug() << "Value for bogus is now:" << settings.value("bogus", -1);

    ui->spinBox->setValue(settings.value("bogus", -1).toInt());

    //    settings.setValue("bogus", ++it);
    //    qDebug() << "Setting new value for bogus";
}

void MainWindow::on_writeButton_clicked()
{
    qDebug() << Q_FUNC_INFO;
    QSettings settings("MySoft", "Star Runner");
    it = 0;
    it =  settings.value("bogus",it).toInt();
    qDebug() << "bogus value is" << it;

    qDebug() << "Setting new value for bogus";
    settings.setValue("bogus", ++it);
    ui->spinBox->setValue(settings.value("bogus", it).toInt());
    qDebug() << Q_FUNC_INFO;
}
