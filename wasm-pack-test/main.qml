import QtQuick 2.11
import QtQuick.Window 2.11

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    property string textString :"This is text."

    Text {
     id: text1
     text: textString
    }

    Text {
     id: text2
     text: textString.toUpperCase()
     anchors.top: text1.bottom
    }

    Text {
     id: text3
     text: textString.toLowerCase()
     anchors.top: text2.bottom
    }


}
