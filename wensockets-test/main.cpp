#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <QMetaProperty>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    bool hasGuiApp = (qobject_cast<QApplication*>(QCoreApplication::instance())!=0);
    qDebug() << "QCoreApplication::instance()" << hasGuiApp;

//    const QMetaObject *metaobject = QCoreApplication::instance()->metaObject();
//    int count = metaobject->propertyCount();
//    for (int i=0; i<count; ++i) {
//        QMetaProperty metaproperty = metaobject->property(i);
//        const char *name = metaproperty.name();
//        QVariant value = QCoreApplication::instance()->property(name);
//        qDebug() << name << " : " << value;
//       // ...
//    }
    MainWindow w;
    w.show();

    return a.exec();
}
