#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <emscripten.h>
#include <emscripten/html5.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    EM_ASM_ARGS({
        // ws://echo.websocket.org
        var socket = new WebSocket('ws://127.0.0.1:8080');

        // Handle any errors that occur.
        socket.onerror = function(error) {
          console.log('WebSocket Error: ' + error);
        };


        // Show a connected message when the WebSocket is opened.
        socket.onopen = function(event) {
          console.log('Connected');// + event.currentTarget.URL;
        };


        // Handle messages sent by the server.
        socket.onmessage = function(event) {
          console.log(message );
        };

        // Show a disconnected message when the WebSocket is closed.
        socket.onclose = function(event) {
          console.log('closed');
        };

        });

}

MainWindow::~MainWindow()
{
    delete ui;
}
