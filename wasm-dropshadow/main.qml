//import QtQuick 2.11
//import QtQuick.Window 2.11

//Window {
//    visible: true
//    width: 640
//    height: 480
//    title: qsTr("Hello World")
//}

import QtQuick 2.11
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    onActiveChanged: {
        console.log("active changed "+ active)
    }

    Text {
        id: label
        anchors.centerIn: parent
        text: "Hello, WASM!"
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        property int clicks
        drag.target: rectangle
        onClicked: {
            console.log("Mouse2 click1 " + clicks)
            clicks++
        }
        onEntered: {
            console.log("mouse2 entered")
        }
        onExited: {
            console.log("mouse2 exited")

        }
        onPositionChanged: {
            console.log("mouse2 position " + mouse.x +" "+mouse.y +" "+mouse.accepted)
            label.text = "mouse2 position " + mouse.x +" "+mouse.y
        }

    }

    Rectangle {
        id: rectangle
        width: 100
        height: 100
        color: "red"
        visible: true
        enabled: true
        onActiveFocusChanged: {
            console.og("focus changed " + activeFocus)
        }
    }

    MouseArea {
        anchors.fill: rectangle
        property int clicks
        hoverEnabled: true
  //      preventStealing: true
   //     drag.target: rectangle
    //    propagateComposedEvents: true
        onPositionChanged: {
            console.log("Mouse1 position " + mouse.x +" "+mouse.y +" "+mouse.accepted)
            label.text = "Mouse1 position " + mouse.x +" "+mouse.y
        }

        onClicked: {
            console.log("Mouse1 click " + clicks)
            clicks++
        }
        onEntered: {
            console.log("mouse1 entered")
        }
        onExited: {
            console.log("mouse1 exited")

        }

    }

    DropShadow {
        anchors.fill: rectangle
        source: rectangle
        verticalOffset: 4
        horizontalOffset: 4
        radius: 10

        clip: false
        enabled: true
        smooth: true
        visible: true
        antialiasing: false
        opacity: 1.0

        onActiveFocusChanged: {
            console.og("focus changed " + activeFocus)
        }
    }

}

