#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString text = "This is text";
    ui->lineEdit->setText(text);
    ui->lineEdit_2->setText(text.toUpper());
}

MainWindow::~MainWindow()
{
    delete ui;
}
