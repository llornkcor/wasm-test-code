#ifndef WSOCK_H
#define WSOCK_H

#include <QObject>

class WSock : public QObject
{
    Q_OBJECT
public:
    explicit WSock(QObject *parent = nullptr);

    void connectToSocket();
    void calling(const QByteArray &message);
    quintptr currentThread;
    quintptr getCurrentThread() { return currentThread;};

signals:
    void newWMessage(const QString &message);

public slots:
};

#endif // WSOCK_H
