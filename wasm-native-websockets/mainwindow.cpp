#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "websocketthread.h"
#include <QDebug>
#include <QThread>
#include <emscripten.h>
#include <emscripten/websocket.h>
#include <emscripten/html5.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();

    ui->setupUi(this);
    WebSocketThread *thread;
    thread = new WebSocketThread();

    connect(thread, &WebSocketThread::newMessage,
        this, &MainWindow::incomingMessage);

    thread->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    // em_websocket_open_callback_func
//    // emscripten_websocket_set_onopen_callback_on_thread
//    EmscriptenWebSocketCreateAttributes attr;
//    emscripten_websocket_init_create_attributes(&attr);
//    attr.url = "ws://127.0.0.1:8080";
//    //        att.protocols
//    EMSCRIPTEN_WEBSOCKET_T socket = emscripten_websocket_new(&attr);

//    if (socket <= 0) {
//        qDebug() << "WebSocket creation failed, error code " << (EMSCRIPTEN_RESULT)socket;

//    } else {
//        emscripten_websocket_set_onopen_callback(socket, (void*)42, WebSocketOpen);
//        emscripten_websocket_set_onmessage_callback(socket, (void*)45, WebSocketMessage);
//    }
//    // emscripten_websocket_set_onopen_callback_on_thread
}

void MainWindow::incomingMessage(const QString &message)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    qDebug() << Q_FUNC_INFO << message;
}
