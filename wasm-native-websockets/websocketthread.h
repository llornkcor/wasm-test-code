#ifndef WEBSOCKETTHREAD_H
#define WEBSOCKETTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <emscripten/websocket.h>

class WebSocketThread : public QThread
{
    Q_OBJECT
public:
    WebSocketThread();
    ~WebSocketThread();
    void run() override;

signals:
    void newMessage(const QString &message);
public slots:
    void incomingMessage(const QString &message);

private:
    QMutex mutex;
    QWaitCondition cond;
    bool quit;

};

#endif // WEBSOCKETTHREAD_H
