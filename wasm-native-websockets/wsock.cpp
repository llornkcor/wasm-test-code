#include "wsock.h"

#include <QDebug>
#include <QThread>
#include <emscripten/websocket.h>
#include <emscripten/val.h>

#include <emscripten/threading.h>

EM_BOOL WebSocketOpen(int eventType, const EmscriptenWebSocketOpenEvent *e, void *userData)
{
    qDebug() << "open(eventType=" << eventType, (int)userData;
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    //    WebSocketThread *socket = reinterpret_cast< WebSocketThread*>(userData);
    //    socket->calling();

    //    emscripten_websocket_send_utf8_text(e->socket, "hello on the other side");

    //    char data[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    //    emscripten_websocket_send_binary(e->socket, data, sizeof(data));

    //    emscripten_websocket_close(e->socket, 0, 0);
    return 0;
}

bool remotely( void *userData)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    WSock *socket = reinterpret_cast< WSock*>(userData);

}

EM_BOOL WebSocketMessage(int eventType, const EmscriptenWebSocketMessageEvent *e, void *userData)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();

    WSock *socket = reinterpret_cast< WSock*>(userData);

    qDebug() << socket->getCurrentThread();
    emscripten_async_queue_on_thread((pthread_t)socket->getCurrentThread(), EM_FUNC_SIG_II, remotely, nullptr, userData);

    if (e->isText) {
        QByteArray ba(reinterpret_cast<const char *>(e->data));
        socket->calling(ba);
        qDebug() << Q_FUNC_INFO << __LINE__ << ba;
    } else {
        QByteArray ba = QByteArray::fromRawData(reinterpret_cast<const char *>(e->data), e->numBytes);
        socket->calling(ba);
        qDebug() << Q_FUNC_INFO << __LINE__ << ba;
    }

    //        emscripten_websocket_delete(e->socket);

    //    }
    return 0;
}

EM_BOOL WebSocketError(
        int eventType, const EmscriptenWebSocketErrorEvent *e, void *userData)
{
    printf("error(eventType=%d, userData=%d)\n", eventType, (int)userData);
    return 0;
}

WSock::WSock(QObject *parent) : QObject(parent)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
}

void WSock::connectToSocket()
{
    EmscriptenWebSocketCreateAttributes attr;
    emscripten_websocket_init_create_attributes(&attr);
    attr.url = "ws://192.168.1.106:8080";
    attr.createOnMainThread = false;
    //        attr.protocols

    // connect
    EMSCRIPTEN_WEBSOCKET_T socket = emscripten_websocket_new(&attr);

    quintptr thisThread = (quintptr)QThread::currentThreadId();

    currentThread = thisThread;

    //pthread_t thisThread = pthread_self();
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << thisThread;

    if (socket <= 0) {
        qDebug() << "WebSocket creation failed, error code " << (EMSCRIPTEN_RESULT)socket;

    } else {
//        emscripten_websocket_set_onopen_callback(socket, (void*)this, WebSocketOpen);
//        emscripten_websocket_set_onmessage_callback(socket, (void*)this, WebSocketMessage);
//        emscripten_websocket_set_onerror_callback(socket, (void*)this, WebSocketError);

         (socket, (void*)this, WebSocketOpen, thisThread);
        emscripten_websocket_set_onmessage_callback_on_thread(socket, (void*)this, WebSocketMessage, (pthread_t)thisThread);
        emscripten_websocket_set_onerror_callback_on_thread(socket, (void*)this, WebSocketError, (pthread_t)thisThread);
    }

}

void WSock::calling(const QByteArray &message)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();

    emit newWMessage(message);
}
