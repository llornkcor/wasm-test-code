#include "websocketthread.h"
#include "wsock.h"

#include <QDebug>

WebSocketThread::WebSocketThread() :
    quit(false)
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
}

WebSocketThread::~WebSocketThread()
{
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    QMutexLocker locker(&mutex);
    quit = true;
    mutex.unlock();
}

void WebSocketThread::run()
{
    //    QMutexLocker locker(&mutex);
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    WSock sock;
    connect(&sock, &WSock::newWMessage,
            this, &WebSocketThread::incomingMessage);

    sock.connectToSocket();
    //  mutex.unlock();

    while (!quit) {
            qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
        ;;
    }
    qDebug() << Q_FUNC_INFO << "thread stopped";
}

void WebSocketThread::incomingMessage(const QString &message)
{
    QMutexLocker locker(&mutex);
    qDebug() << Q_FUNC_INFO << "QThread::currentThreadId()" << (quintptr)QThread::currentThreadId();
    emit newMessage(message);
    quit = true;
    mutex.unlock();
}
