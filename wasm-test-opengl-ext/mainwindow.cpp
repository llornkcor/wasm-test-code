#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui/qopenglcontext.h>
#include <QOpenGLFunctions>
#include <QOpenGLContext>
#include <QtGui/private/qopenglextensions_p.h>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QOpenGLExtensions *e = static_cast<QOpenGLExtensions *>(QOpenGLContext::currentContext()->functions());
    qWarning() << "Has TextureSwizzle?" << (e->hasOpenGLExtension(QOpenGLExtensions::TextureSwizzle) ? "Yes" : "No");
    qWarning() << e->hasOpenGLExtension(QOpenGLExtensions::TextureSwizzle);
}

MainWindow::~MainWindow()
{
    delete ui;
}

