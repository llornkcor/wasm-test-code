#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <emscripten/val.h>
#include <emscripten/bind.h>

namespace Ui {
class MainWindow;
}
using namespace emscripten;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void webSocketOpened(val event);
    static void emitSend();
    static MainWindow *instance() { return self; }
signals:
    void sendIt();
private:
    Ui::MainWindow *ui;
    val webWocket = val::global("WebSocket");
    val socketContext = val::null();
 //   static MainWindow *mainWindow;
     static MainWindow *self;
private slots:
    void doSend();
};

#endif // MAINWINDOW_H
