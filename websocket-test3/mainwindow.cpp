#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>

#include <QDebug>
#include <QTimer>

//Q_GLOBAL_STATIC(MainWindow, mainWindow);

using namespace emscripten;
MainWindow *MainWindow::self = 0;

extern "C" {
    void webSocketError(val event)
    {
        val target = event["target"];
        MainWindow *mw = reinterpret_cast<MainWindow*>(target["data-context"].as<size_t>());
        qDebug() << Q_FUNC_INFO <<mw;
        int code =  event["error"].as<int>();
        qDebug() << Q_FUNC_INFO << code;
    }

    void webSocketMessageReceived(val event)
    {
        val target = event["target"];
        MainWindow *mw = reinterpret_cast<MainWindow*>(target["data-context"].as<size_t>());
        qDebug() << Q_FUNC_INFO <<mw;
        qDebug() << Q_FUNC_INFO << target["binaryType"].as<std::string>().data()
                 << event["data"].typeOf().as<std::string>().data();
        std::string message;

        if (event["data"].typeOf().as<std::string>() == "string") {
            message = event["data"].as<std::string>();
            qDebug() << Q_FUNC_INFO << message.data();
        } else {

            val reader = val::global("FileReader").new_();

//            reader.call<void>("addEventListener", std::string("load"),
//                                     val::module_property("readBlob"));
            reader.set("onload", val::module_property("readBlob"));

//            reader.call<void>("readAsArrayBuffer", event["data"]);
            reader.set("data-context", target["data-context"]);
            reader.call<void>("readAsArrayBuffer", event["data"]);
        }
    }

    void webSocketClosed(val event)
    {
//        val target = event["target"];
//        MainWindow *mw = reinterpret_cast<MainWindow*>(target["data-context"].as<size_t>());
        int code =  event["code"].as<int>();
        qDebug() << Q_FUNC_INFO << code;
    }

    void webSocketOpened(val event)
    {
        val target = event["target"];
        MainWindow *mw = reinterpret_cast<MainWindow*>(target["data-context"].as<size_t>());
        mw->emitSend();
    }

    void readBlob(val event)
    {
    //    val sourceTypedArray = val::global("sourceTypedArray").new_(event["target"]["result"]);
    //    size_t size = sourceTypedArray["byteLength"].as<size_t>();

        val fileReader = event["target"];

        MainWindow *mw = reinterpret_cast<MainWindow*>(fileReader["data-context"].as<size_t>());
        qDebug() << Q_FUNC_INFO << mw;
        // Set up source typed array
        val result = fileReader["result"]; // ArrayBuffer
        val Uint8Array = val::global("Uint8Array");

        val sourceTypedArray = Uint8Array.new_(result);

        size_t size = result["byteLength"].as<size_t>();
        void *buffer = malloc(size);

        val destinationTypedArray = Uint8Array.new_(val::module_property("HEAPU8")["buffer"], size_t(buffer), size);
        destinationTypedArray.call<void>("set", sourceTypedArray);

        char *strPtr = static_cast<char *>(buffer);
        QByteArray bArray = QByteArray::fromRawData(strPtr,size);
        qDebug() << Q_FUNC_INFO << bArray;
        free(strPtr);

        qDebug() << Q_FUNC_INFO //<< fileReader["binaryType"].as<std::string>().data()
                 << event["data"].typeOf().as<std::string>().data();

    }
}

EMSCRIPTEN_BINDINGS(wasm_module) {
    function("webSocketOpened", webSocketOpened);
    function("webSocketError", webSocketError);
    function("webSocketClosed", webSocketClosed);
    function("webSocketMessageReceived", webSocketMessageReceived);
    function("readBlob", readBlob);
//    register_vector<int>("VectorInt");
    register_vector<int>("vector<int>");
};


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    MainWindow::self = this;
    qDebug() << Q_FUNC_INFO << __LINE__;

    ui->setupUi(this);


    socketContext = webWocket.new_(std::string("ws://10.0.0.4:6931"));

//    socketContext.call<void>("addEventListener", std::string("open"),
//                             val::module_property("webSocketOpened"));

qDebug() << Q_FUNC_INFO << this;
    socketContext.set("onopen", val::module_property("webSocketOpened"));
    socketContext.set("onclose", val::module_property("webSocketClosed"));
    socketContext.set("onerror", val::module_property("webSocketError"));
    socketContext.set("onmessage", val::module_property("webSocketMessageReceived"));
    socketContext.set("data-context", val(size_t((void *)this)));

//    socketContext.call<void>("addEventListener", std::string("error"),
//                             val::module_property("webSocketError"));

//    socketContext.call<void>("addEventListener", std::string("close"),
//                             val::module_property("webSocketClosed"));

//    socketContext.call<void>("addEventListener", std::string("message"),
//                             val::module_property("webSocketMessageReceived"));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::doSend()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
//      webWocket = val::global("socketContext");
    QByteArray ba;// = new QByteArray();
    ba.resize(6);
    ba[0] = 0x3c;
    ba[1] = 0xb8;
    ba[2] = 0x64;
    ba[3] = 0x18;
    ba[4] = 0xca;
    ba[5] = 0xca;
    qDebug() << Q_FUNC_INFO << ba;

    socketContext.call<void>("send",std::string("hello socket"));
    socketContext.call<void>("send",val(typed_memory_view(ba.size(),
                                                              reinterpret_cast<const unsigned char *>(ba.constData()))));
qDebug() << Q_FUNC_INFO;
}

void MainWindow::emitSend()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    MainWindow::instance()->doSend();
}//
