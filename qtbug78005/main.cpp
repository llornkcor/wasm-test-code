#include <QtNetwork>
#include <QtWidgets>
#include <QHttpMultiPart>

class MyWidget : public QWidget
{
    Q_OBJECT
public:
    MyWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        QVBoxLayout *vbox = new QVBoxLayout;
        QPushButton *button = new QPushButton;
        button->setText("Go ahead... make my day");
        connect(button, SIGNAL(clicked()), this, SLOT(postData()));
        vbox->addWidget(button);
        setLayout(vbox);
    }
public slots:
    void postData()
    {
        qDebug() << Q_FUNC_INFO;
        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

        QHttpPart textPart;
        textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
        textPart.setBody("my text");

        QHttpPart imagePart;
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"image\""));
        imagePart.setBody("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        multiPart->append(textPart);
        multiPart->append(imagePart);

        QUrl url("http://192.168.1.124/qtbug78005.html");
        QNetworkRequest request(url);

        QNetworkAccessManager manager;
        QNetworkReply *reply = manager.post(request, multiPart);
        multiPart->setParent(reply); // delete the multiPart with the reply

        // here connect signals etc.
      //  connect(reply, SIGNAL(finished()), multiPart, SLOT(deleteLater()));
        connect(reply,
           QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
           [=](QNetworkReply::NetworkError code){

               qDebug() << Q_FUNC_INFO << code << reply->errorString(); });

        QObject::connect(
            reply, &QNetworkReply::finished,
            [reply]() { qDebug() << "GOT DATA " << reply->readAll(); reply->deleteLater(); });

    }
};

#include "main.moc"

int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    MyWidget w;
    w.show();
    return a.exec();
}
