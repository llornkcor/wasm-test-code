#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint);
    w.setAttribute(Qt::WA_TranslucentBackground);
//    QSurfaceFormat format;
//     format.setAlphaBufferSize(8);
//     QSurfaceFormat::setDefaultFormat(format);
    w.show();
    return a.exec();
}
