TEMPLATE = subdirs

SUBDIRS += qtbug78005 \
    wasm-droparea \
    wasm-glow \
    wasm-sysinfo \
    wasm-test-opengl-ext \
    wasm-testlookup \
    wasm-transparent
SUBDIRS += qt-console-emsdk
SUBDIRS += qt-wasm-bug-pointer
SUBDIRS += qt-wasm-render-bug
SUBDIRS += qt-wasm-test
SUBDIRS += scroll
SUBDIRS += settings-test
SUBDIRS += test-sockets
SUBDIRS += test-threadsocket
SUBDIRS += wasm-args
SUBDIRS += wasm-clip2
SUBDIRS += wasm-clipboard
SUBDIRS += wasm-combotest
SUBDIRS += wasm-dropshadow
SUBDIRS += wasm-fetch
SUBDIRS += wasm-fonts
SUBDIRS += wasm-material-test
SUBDIRS += wasm-menu
SUBDIRS += wasm-native-websockets
SUBDIRS += wasm-net-image
SUBDIRS += wasm-openglwidget
SUBDIRS += wasm-pack-2
SUBDIRS += wasm-pack-test
SUBDIRS += wasm-profile
SUBDIRS += wasm-qmllist-test
SUBDIRS += wasm-qml-quit
SUBDIRS += wasm-resource-test
SUBDIRS += wasm-server
SUBDIRS += wasm-test-code.pro
SUBDIRS += wasm-test-material
SUBDIRS += wasm-test-multipart
SUBDIRS += wasm-test-opengl
SUBDIRS += wasm-testPoint
SUBDIRS += wasm-testwindow
SUBDIRS += wasm-threads
SUBDIRS += wasm-thread-socket
SUBDIRS += wasm-thread-test
SUBDIRS += wasm-timer
SUBDIRS += wasm-transparancy
SUBDIRS += wasm-uploader
SUBDIRS += wasm-vkdb
SUBDIRS += wasm-websocket-server
SUBDIRS += wasm-widget
SUBDIRS += wasm-windows
SUBDIRS += websockets-client
SUBDIRS += websockets-echoclient
SUBDIRS += websockets-test2
SUBDIRS += websocket-test3
SUBDIRS += wensockets-test
SUBDIRS += etm-fs-test
SUBDIRS += emsdk-qml-test
SUBDIRS += wasm-download
SUBDIRS += wasm-settings-test

