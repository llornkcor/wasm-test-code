#include "mainwindow.h"
#include <QtCore/qconfig.h>
#include <QApplication>
#ifdef QT_EMCC_VERSION
#pragma message ( QT_EMCC_VERSION )
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
