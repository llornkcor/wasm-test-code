#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/bind.h>
#include <emscripten/val.h>

using namespace emscripten;
static void q_onErrorCallback(val event)
{
    qDebug() << Q_FUNC_INFO << (event.as<bool>() ? "Bluetooth is available" : "Bluetooth is NOT available");
    if (event.as<bool>()) {
        emscripten::val options = emscripten::val::array();
        options.set("acceptAllDevices", val("true"));
        val::global("navigator")["bluetooth"]
                .call<val>("requestDevice", options)
                .call<void>("then",
                            val::module_property("q_onDeviceRequest"));
        // Uncaught (in promise) DOMException: Failed to execute 'requestDevice' on 'Bluetooth':
        // Must be handling a user gesture to show a permission request.

    }
}
static void q_onDeviceRequest(val bluetoothdevice)
{
      qDebug() << Q_FUNC_INFO << QString::fromStdString(bluetoothdevice["name"].as<std::string>());
}

EMSCRIPTEN_BINDINGS(wasm_module) {
    function("q_onErrorCallback", q_onErrorCallback);
    function("q_onDeviceRequest", q_onDeviceRequest);
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

     val jsBluetoothContext = val::global("navigator")["bluetooth"];
     if (!jsBluetoothContext.as<bool>()) {
         qDebug() << "No Bluetooth context";
         return;
     }
    val isBluetoothAvailablePromise = jsBluetoothContext.call<val>("getAvailability");
    isBluetoothAvailablePromise.call<val>("then", val::module_property("q_onErrorCallback"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::availablilityChanged()
{

}


