#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtNetwork>
#include <QHttpMultiPart>
#include <QFileDialog>
#include <QBuffer>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    manager = new QNetworkAccessManager(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::postData()
{
    qDebug() << Q_FUNC_INFO << "m_fileName" << m_fileName;
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    qDebug() << Q_FUNC_INFO << __LINE__;
    QHttpPart sendPart;
 //   sendPart.setHeader(QNetworkRequest::ContentDispositionHeader, QString("form-data; name=\"submit\""));
    sendPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"action\""));

    qDebug() << Q_FUNC_INFO << __LINE__;
    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QString("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QString("form-data; name=\"image\"; filename=\"%1\"").arg(m_fileName));

    qDebug() << Q_FUNC_INFO << __LINE__;
    QBuffer *buffer = new QBuffer(this);;
    buffer->open(QIODevice::ReadWrite);
    buffer->setBuffer(&m_fileContent);

    qDebug() << Q_FUNC_INFO << __LINE__;
    imagePart.setBodyDevice(buffer);

    multiPart->append(sendPart);
    multiPart->append(imagePart);

    QUrl url("http://192.168.1.122/upload.php");

    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentLengthHeader, buffer->size());

    qDebug() << Q_FUNC_INFO << __LINE__;

    reply = manager->post(request, multiPart);
    multiPart->setParent(reply);

    qDebug() << Q_FUNC_INFO << __LINE__;
 //   multiPart->setParent(reply); // delete the multiPart with the reply
  // here connect signals etc.
  //  connect(reply, SIGNAL(finished()), multiPart, SLOT(deleteLater()));

    connect(reply,
       QOverload<QNetworkReply::NetworkError>::of(&QNetworkReply::error),
       [=](QNetworkReply::NetworkError code) {
        qDebug() << Q_FUNC_INFO << __LINE__;

           qDebug() << Q_FUNC_INFO << code << reply->errorString();
    });

    qDebug() << Q_FUNC_INFO << __LINE__;

    QObject::connect(
        reply, &QNetworkReply::finished,
        [=]() {
        qDebug() << Q_FUNC_INFO << __LINE__;
        qDebug() << "GOT DATA " << reply->readAll(); reply->deleteLater();
    });

}

void MainWindow::on_pushButton_clicked()
{
#ifdef Q_OS_WASM
    auto fileContentReady = [=](const QString &fileName, const QByteArray &fileContent) {
        if (fileName.isEmpty()) {
            // No file was selected
        } else {
            // Use fileName and fileContent
            m_fileContent = fileContent;
            m_fileName = fileName;
            postData();
        }
    };
    QFileDialog::getOpenFileContent("Images (*.png *.xpm *.jpg)",  fileContentReady);
#else
    auto filename = QFileDialog::getOpenFileName(this,
                                                 tr("Open Image"), "~", tr("Image Files (*.jpeg *.jpg)"));

    QFile *file = new QFile(filename);
    file->open(QIODevice::ReadOnly);
    QFileInfo fi(file->fileName());
    m_fileName = fi.fileName();
    m_fileContent = file->readAll();
    qDebug() << m_fileContent;
    postData();
#endif
}
