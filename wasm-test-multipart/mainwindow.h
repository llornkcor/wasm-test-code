#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void postData();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
     QNetworkReply *reply;
     QNetworkAccessManager *manager;
     QByteArray  m_fileContent;
     QString m_fileName;

};

#endif // MAINWINDOW_H
