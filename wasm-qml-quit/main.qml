import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.4

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    MouseArea {
        anchors.fill: parent
        onClicked: messageDialog.open()
    }

    Dialog {
        id: messageDialog
        title: "May I have your attention please"
        Label {
            text: "It's so cool that you are using Qt Quick."
            font.pixelSize: 22
            font.italic: true
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            console.log("And of course you could only agree.")
            Qt.quit()
        }
        // Component.onCompleted: visible = true
    }

}
