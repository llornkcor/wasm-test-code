#include <QApplication>
#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>
#include <emscripten/websocket.h>

#include <QDebug>

using namespace emscripten;

//QByteArray g_messageArray;
// easiest way to transliterate binary data to js/wasm

//void getBinaryMessage(int *arg)
//{
//// void (*f)(void *) = reinterpret_cast<void (*)(void *)>(arg);
//qDebug() << arg << reinterpret_cast<char *>(&arg);
////int* ptr =
////return val(typed_memory_view(g_messageArray.size(),
////                             reinterpret_cast<const unsigned char *>(g_messageArray.constData())));
//}

//void webSocketOpened(val event)
//{
//    qDebug() << Q_FUNC_INFO;
//}

//EMSCRIPTEN_BINDINGS(wasm_module) {
////    function("getBinaryMessage", &getBinaryMessage, allow_raw_pointers());
//    function("webSocketOpened", &webSocketOpened);
//   // register_vector<int>("VectorInt");
//};

static EM_BOOL q_onSocketOpenCallback(int /*eventType*/, const EmscriptenWebSocketOpenEvent */*e*/, void *userData)
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    return 1;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
// EM_ASM(

//    FS.mount(IDBFS, {}, '/home/web_user');
//    FS.syncfs(true, function(err) {
//        if (err)
//            Module.print(err);
//    });
//    );
//    QByteArray message;// = new QByteArray();
//    message.resize(5);
//    message[0] = 0x3c; //60
//    message[1] = 0xb8; // 184
//    message[2] = 0x64; // 100
//    message[3] = 0x18; // 24
//    message[4] = 0xca; // 202

//    EM_ASM_ARGS({
//console.log($0);

//       //var array =
//                    Module.getBinaryMessage($0);
//     }, message.constData());

//    val webWocket = val::global("WebSocket");

//    std::string protocolHeader = "binary";

//    val socketContext = webWocket.new_(std::string("ws://127.0.0.1:8080"), protocolHeader);

//    socketContext.set("binaryType", "arraybuffer");

    // int socketId =
    //    socketContext.set("onopen", val::module_property("webSocketOpened"));
//    socketContext.set("data-context", val(quintptr(reinterpret_cast<void *>(&a))));

        EmscriptenWebSocketCreateAttributes attr;
        emscripten_websocket_init_create_attributes(&attr);
        attr.url = "ws://127.0.0.1:8080";//hostAddress.toUtf8();
        const char *protocol = "binary, base64"; // "base64"
        attr.protocols = &protocol; // ?Sec-WebSocket-Protocol

    QString hostUrl = "ws:127.0.0.1:8080";
    QString protocols = "binary";
    EM_ASM_ARGS({
                 var url = UTF8ToString($0);
                 var protocols = UTF8ToString($1);
                    console.log(url+" "+protocols);

                 var socket = new WebSocket(url, protocols);
                 socket.binaryType = 'arraybuffer';
                 var socketId = WS.sockets.length;
                 LibraryWebSocket.WS.sockets[socketId] = socket;
           },hostUrl.toLatin1().data(),
           protocols.toLatin1().data()
           );


    emscripten_websocket_set_onopen_callback(EMSCRIPTEN_WEBSOCKET_T(1), (void*)25, q_onSocketOpenCallback);


    // create and connect
//    EMSCRIPTEN_WEBSOCKET_T socket = emscripten_websocket_new(&attr);

//    val webWocketW = val::global("WS");
//if (webWocketW.isNull() || webWocketW.isUndefined())
//    qDebug() << "NOT WS!";

//   int length =  webWocketW["sockets"]["length"].as<int>();

//   qDebug() << Q_FUNC_INFO << "length" << length;

//    var socket = new WebSocket(url);
//    socket.binaryType = 'arraybuffer';

//     webWocket["sockets"][1] = socketContext.as<object>();
    // TODO: While strictly not necessary, this ID would be good to be unique across all threads to avoid confusion.

//    var socketId = WS.sockets.length;
//    WS.sockets[socketId] = socket;

//    EM_ASM({
//               console.log(WS.sockets.length);
//           });
//    val webWocket = val::global("WS");
//quintptr socketNum =
//        std::string wSocketUrl = webWocket["sockets"][1]["url"].as<std::string>();

//    qDebug() << Q_FUNC_INFO << "socket descrt"
//             << socket
//             << QString::fromStdString(wSocketUrl);

   // socketContext.call<void>("send",std::string("hello socket"));

//    val FS = val::global("FS").new_(3, std::string("IDBFS, {}, '/home/web_user'"));

//    FS.call(std::string("unmount"), std::string("/home/web_user"));

    //    EM_ASM_ARGS({
//        // ws://echo.websocket.org
//        var socket = new WebSocket('ws://10.0.0.4:1234');

//        // Handle any errors that occur.
//        socket.onerror = function(error) {
//          console.log('WebSocket Error: ' + error);
//        };


//        // Show a connected message when the WebSocket is opened.
//        socket.onopen = function(event) {
//          console.log('Connected');// + event.currentTarget.URL;
//        };


//        // Handle messages sent by the server.
//        socket.onmessage = function(event) {
//          console.log(message );
//        };

//        // Show a disconnected message when the WebSocket is closed.
//        socket.onclose = function(event) {
//          console.log('closed');
//        };

//        });

    return a.exec();
}
