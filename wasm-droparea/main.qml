import QtQuick 2.12
import QtQuick.Controls 2.3

ApplicationWindow {
    id: window
    title: "droparea"
    visible: true

    DropArea {
        id: dropArea
        anchors.fill: parent
        keys: ["text/plain"]
        onEntered: print('entered')
        onDropped: {
            textArea.text = drop.text
        }
    }
    Text {
        id: textArea

    }
}
