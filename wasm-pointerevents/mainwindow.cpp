#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <emscripten/val.h>
#include <emscripten/bind.h>

using namespace emscripten;

void downHandler(val event)
{
    val pointerType = event["pointerType"];
    val id = event["identifier"];

    bool isPrimary = event["isPrimary"].as<bool>();
    qDebug() << Q_FUNC_INFO
             << QString::fromStdString(pointerType.as<std::string>())
             << id.as<int>()
             << (isPrimary ? "primary" : "secondary");
}

void upHandler(val event)
{
    qDebug() << Q_FUNC_INFO;
}

void moveHandler(val event)
{
    qDebug() << Q_FUNC_INFO;
}

// over
// enter
// cancel
// out
// leave
// gotcapture
// lostcapture


EMSCRIPTEN_BINDINGS(wasm_pointerevents) {
    function("qtPointerDown", downHandler);
    function("qtPointerUp", upHandler);
    function("qtPointerMove", moveHandler);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initHandlers();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initHandlers()
{
    val document = val::global("document");
    val canvas = document.call<emscripten::val>("getElementById", std::string("qtcanvas"));
    canvas.call<void>("addEventListener", val("pointerdown"),
                        val::module_property("qtPointerDown"), true);
    canvas.call<void>("addEventListener", val("pointerup"),
                        val::module_property("qtPointerUp"), true);
    canvas.call<void>("addEventListener", val("pointermove"),
                        val::module_property("qtPointerMove"), true);
}

