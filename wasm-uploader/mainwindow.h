#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QNetworkAccessManager *manager;
    void connectReply(QNetworkReply *reply);

private slots:
    void on_uploadButton_clicked();

    void replyFinished(QNetworkReply*);

 //   void redirectAllowed()  { qDebug() << Q_FUNC_INFO; }
//    void redirected(const QUrl &url);
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void uploadProgress(qint64 bytesSent, qint64 bytesTotal);
 //   void metaDataChanged() { qDebug() << Q_FUNC_INFO; }
    void finished() { qDebug() << Q_FUNC_INFO; }

    void multipartFormData();
    void formData();
    void getFormUrlencoded();
    void postFormUrlencoded();

    void on_getUrlencodeButton_clicked();

    void on_postUrlencodeButton_clicked();

    void on_jsonButton_clicked();

    void on_webpageButton_clicked();


    void on_getJsonButtong_clicked();

    void on_pushButton_clicked();
    void uploadImageFile();
    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QUrl url;
};

#endif // MAINWINDOW_H
