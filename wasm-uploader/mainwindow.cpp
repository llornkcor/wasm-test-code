#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtWidgets/QtWidgets>

//#include <QNetworkConfiguration>
//#include <QNetworkConfigurationManager>
#include <QDebug>

//#if QT_CONFIG(http)
#include <QHttpPart>
//#endif

#include <QUrlQuery>
#include <QJsonObject>
#include <QJsonDocument>

#define SERVER "http://192.168.1.137"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    url(SERVER)
{
    ui->setupUi(this);
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
//#if !QT_CONFIG(http)
    ui->uploadButton->setEnabled(true);
//#endif
    ui->label->setText(QString("Using server at %1").arg(SERVER));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_uploadButton_clicked()
{
    multipartFormData();
}

void MainWindow::formData()
{

}

void MainWindow::multipartFormData()
{
//#if QT_CONFIG(http)
    // upload
    ui->textEdit->clear();
    QNetworkRequest requestF;
    url.setPath("/upload.php");
    requestF.setUrl(url);

    QFile *file = new QFile(":/heart.png");
    if (!file->open(QIODevice::ReadOnly))
        return;

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
    textPart.setBody("my text");

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader,"image/png");
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file\"; filename=\"heart.png\""));

    imagePart.setBodyDevice(file);
    file->setParent(multiPart);

    multiPart->append(textPart);
    multiPart->append(imagePart);

    QNetworkReply *replyF = manager->post(requestF, multiPart);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
//#endif
}

void MainWindow::getFormUrlencoded()
{
    ui->textEdit->clear();

    QNetworkRequest request;
    url.setPath("/form.php");

    //    QString file = "otherpage.php";

    QByteArray postData;
    QUrlQuery formData;

    formData.addQueryItem("login", "me");
    formData.addQueryItem("password", "123");
    url.setQuery(formData);

    request.setUrl(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    qDebug() << Q_FUNC_INFO << "POST";
    QNetworkReply *replyF = manager->get(request/*, formData*/);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
}

void MainWindow::postFormUrlencoded()
{
    ui->textEdit->clear();

    QNetworkRequest request;

    url.setPath("/form.php");

    //    QString file = "otherpage.php";

    QByteArray postData;

    postData.append("login=me&password=123&submit=Send+data");

    request.setUrl(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    qDebug() << Q_FUNC_INFO << "POST";
    QNetworkReply *replyF = manager->post(request, postData);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
}

void MainWindow::replyFinished(QNetworkReply *reply)
{
    QVariant statusCodeV =
           reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

    QString message1 = QString("status code: %1").arg(statusCodeV.toInt());
    ui->textEdit->insertPlainText(message1 + "\n");

    if (reply->error()) {
        QString message2 = QString("error: %1").arg(reply->errorString());
        ui->textEdit->insertPlainText(message2 + "\n");

//        qDebug() << Q_FUNC_INFO  << reply->errorString()
//                 << reply->rawHeaderList();
    } else {

        QByteArray response_data = reply->readAll();

//        QFile *file = new QFile("smartgit-18_1_4.deb");
//        file.open(QIODevice::WriteOnly);
//        file.write(response_data);
//        file.close();
        QByteArray hash = QCryptographicHash::hash(response_data, QCryptographicHash::Sha256);
      //  fileHash->setText(QString("Sha256: %1").arg(QString(hash.toHex())));

     //message3   QString message3 = QString("checksum: %1").arg(qChecksum(response_data, 1024));
        ui->textEdit->insertPlainText(QString("Sha256: %1").arg(QString(hash.toHex())) + "\n");

        QString message5 = QString("response size code: %1").arg(response_data.size());
        ui->textEdit->insertPlainText(message5 + "\n");


        QString message6 = QString("status code: %1").arg(statusCodeV.toInt());
        ui->textEdit->insertPlainText(message1 + "\n");

        QList<QByteArray> headerList = reply->rawHeaderList();
        foreach(QByteArray head, headerList) {

            QString message7 = QString("%1 : %2").arg(QString(head)).arg(QString(reply->rawHeader(head)));
            ui->textEdit->insertPlainText(message7 + "\n");

            if (reply->rawHeader(head).contains("json")) {
                QJsonDocument json = QJsonDocument::fromJson(response_data);
                ui->textEdit->insertPlainText(json.toJson() + "\n");
            }
        }
        ui->textEdit->insertPlainText(response_data + "\n");
    }
    reply->deleteLater();
}

void MainWindow::connectReply(QNetworkReply *reply)
{
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)),
            this,SLOT(downloadProgress(qint64,qint64)));
    connect(reply,
            SIGNAL(uploadProgress(qint64,qint64)),this,SLOT(uploadProgress(qint64,qint64)));
    connect(reply, SIGNAL(finished()),this,SLOT(finished()));
   // connect(reply, SIGNAL(redirected(QUrl)),this,SLOT(redirected(QUrl)));

    connect(reply, &QNetworkReply::errorOccurred,
            [=](QNetworkReply::NetworkError code) {

        QString message5 = QString("error code: %1, %2").arg(code).arg(reply->errorString());
        ui->textEdit->insertPlainText(message5 + "\n");

    });
}

void MainWindow::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesReceived);
    ui->progressBar->setFormat("%v of %m");
//qDebug() << Q_FUNC_INFO << "bytesReceived" << bytesReceived << "bytesTotal" << bytesTotal;
}

void MainWindow::uploadProgress(qint64 bytesSent, qint64 bytesTotal)
{
    ui->progressBar->setMaximum(bytesTotal);
    ui->progressBar->setValue(bytesSent);
    ui->progressBar->setFormat("%v of %m");
//    qDebug() << Q_FUNC_INFO << "bytesSent" << bytesSent << "bytesTotal" << bytesTotal;
}

void MainWindow::on_getUrlencodeButton_clicked()
{
    getFormUrlencoded();
}

void MainWindow::on_postUrlencodeButton_clicked()
{
    postFormUrlencoded();
}

void MainWindow::on_jsonButton_clicked()
{
    ui->textEdit->clear();

    QNetworkRequest requestP;
    url.setPath("/echojson.php");
    requestP.setUrl(url);

    requestP.setRawHeader("Content-Type", "application/json");
    QJsonObject json;
    json.insert("item1", "value1");
    json.insert("item2", "value2");
    QByteArray jsonDoc = QJsonDocument(json).toJson();
    qDebug() << Q_FUNC_INFO << jsonDoc.size() << jsonDoc;

    QNetworkReply *replyP = manager->post(requestP, jsonDoc);
    connectReply(replyP);
    if (replyP->error())
        ui->textEdit->insertPlainText(replyP->errorString() + "\n");

//    QByteArray data = QtJson::Json::serialize(collectSyncData());

//    xmlhttp.send(JSON.stringify({name:"John Rambo", time:"2pm"}));}}}}

}

void MainWindow::on_webpageButton_clicked()
{
    ui->textEdit->clear();

    QNetworkRequest requestF;
    url.setPath("/index.html");
    requestF.setUrl(url);

    QNetworkReply *replyF = manager->get(requestF);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
}

void MainWindow::on_getJsonButtong_clicked()
{
    ui->textEdit->clear();

    QNetworkRequest requestP;
    url.setPath("/configure.json");
    requestP.setUrl(url);

    requestP.setRawHeader("Content-Type", "application/json");

    QNetworkReply *replyF = manager->get(requestP);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
}

void MainWindow::on_pushButton_clicked()
{
    ui->textEdit->clear();

    QNetworkRequest requestF;
    // target-file-1000M
    // QTBUG-74123.zip
    //  smartgit-18_1_4.deb

    url.setPath("/United.tar.gz");
    requestF.setUrl(url);

    QNetworkReply *replyF = manager->get(requestF);
    connectReply(replyF);
    if (replyF->error())
        ui->textEdit->insertPlainText(replyF->errorString() + "\n");
}

void MainWindow::uploadImageFile()
{
qDebug() << Q_FUNC_INFO;
    auto fileContentReady = [this](const QString &fileName, const QByteArray &fileContent) {
        if (fileName.isEmpty()) {
            // No file was selected
            qDebug() << "No file selected";
        } else {

            QFileInfo fi(fileName);
            QString fileExtension = fi.suffix();
qDebug() << Q_FUNC_INFO << QString("Opened file: %1 size: %2").arg(fileName).arg(fileContent.size());
            if  ((fileExtension == "mp3") || (fileExtension == "MP3")) {
//                callJS_startRotatingPodcastReloadImage();

                QByteArray boundary="mp3files";

                QByteArray datas(QString("--" + boundary + "\r\n").toStdString().c_str());

                datas += ("Content-Disposition: form-data; name=\"file\"; filename=\""+fileName+"\"\r\n").toStdString().c_str();

                datas += "Content-Type: audio/mpeg\r\n\r\n";
                datas += fileContent;
                datas += "\r\n";
                datas += QString("--" + boundary + "\r\n\r\n").toStdString().c_str();
                datas += "Content-Disposition: form-data; name=\"upload\"\r\n\r\n";
                datas += "Uploader\r\n";
                datas += QString("--" + boundary + "--\r\n").toStdString().c_str();

                QNetworkRequest req;
                url.setPath("/upload2.php");
//                req.setUrl(url);

             //   req.setUrl(QUrl("https://xxxxx.com/admin/upload.php"));
                req.setRawHeader("Content-Type", "multipart/form-data; boundary=" + boundary);

                QNetworkAccessManager *manager = new QNetworkAccessManager;

                QNetworkReply *reply = manager->post(req,datas);

                connect(reply, &QNetworkReply::finished, this, [this, reply] {
                  //  callJS_stopRotatingPodcastReloadImage();

                    QVariant statusCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
                    int status = statusCode.toInt();

                    if (status == 200) {
                        qDebug() << "Uploaded file succesfully";
                    }
                });
            }
        }
    };

    QFileDialog::getOpenFileContent("MP3 Files (*.mp3 *.MP3)",  fileContentReady);

}

void MainWindow::on_pushButton_3_clicked()
{
    uploadImageFile();
}

