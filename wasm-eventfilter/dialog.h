#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTouchEvent>
#include <QPointerEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

private:
    Ui::Dialog *ui;
    bool eventFilter(QObject* obj, QEvent* event);
    void tabletEvent(QTabletEvent *event);
    void touchEvent(QTouchEvent *event);
   // void pointerEvent(QPointerEvent *event);
};
#endif // DIALOG_H
