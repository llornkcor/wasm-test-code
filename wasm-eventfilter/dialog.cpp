#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <QWindow>
#include <QHoverEvent>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    qApp->installEventFilter( this );
        //setAttribute( Qt::WA_AcceptTouchEvents);
    ui->lineEdit->setAttribute(Qt::WA_Hover);
    setAttribute(Qt::WA_AcceptTouchEvents);
    ui->lineEdit->setAttribute(Qt::WA_AcceptTouchEvents);
}

Dialog::~Dialog()
{
    delete ui;
}


bool Dialog::eventFilter(QObject* obj, QEvent* event)
{
    switch (event->type())
    {
        //    if (event->type() == QEvent::CursorChange) {
        //        qDebug() << Q_FUNC_INFO << "Cursor change";
        //        QWidget *wid = qobject_cast<QWidget *>(obj);
        //        if (wid) {
        //            qDebug() <<"widget"  << wid->cursor().shape();
        //        }
        //        QWindow *win = qobject_cast<QWindow *>(obj);
        //        if (win) {
        //            qDebug() <<"Window" << win->cursor().shape();
        //        }

        //    }
    case QEvent::KeyboardLayoutChange:
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    {
        qWarning() << Q_FUNC_INFO << event;
    }
    break;
    case QEvent::Resize:
    {
        qDebug() << Q_FUNC_INFO << "resize";
        QWidget *wid = qobject_cast<QWidget *>(obj);
        //        if (wid) {
        //            qDebug() << "widget" << wid->cursor().shape();
        //        }
        QWindow *win = qobject_cast<QWindow *>(obj);
        //        if (win) {
        //            qDebug() <<"Window" << win->cursor().shape();
        //        }
    }
    break;

    case  QEvent::Enter:
    {
        QWidget *wid = qobject_cast<QWidget *>(obj);
        if (!wid)
            return false;
        qDebug() << Q_FUNC_INFO <<"widget"  << wid;
        qDebug() << Q_FUNC_INFO << "enter";
        QString str = QString("Enter : %1\n").arg(wid->objectName());
        ui->textEdit->append(str);
    }
    break;
    case QEvent::Leave:
    {
        QWidget *wid = qobject_cast<QWidget *>(obj);
        if (!wid)
            return false;
        qDebug()  << Q_FUNC_INFO <<"widget"  << wid;
        qDebug() << Q_FUNC_INFO << "leave";
        QString str = QString("Leave : %1\n").arg(wid->objectName());
        ui->textEdit->append(str);

    }
    break;
    case QEvent::HoverEnter:
    {
        QWidget *wid = qobject_cast<QWidget *>(obj);
        if (!wid)
            return false;
        qDebug() << Q_FUNC_INFO <<"widget"  << wid;
        qDebug() << Q_FUNC_INFO << "HoverEnter";
        QString str = QString("HoverEnter : %1\n").arg(wid->objectName());
        ui->textEdit->append(str);

    }
    break;
    case QEvent::HoverLeave:
    {
        QWidget *wid = qobject_cast<QWidget *>(obj);
        if (!wid)
            return false;
        qDebug()  << Q_FUNC_INFO <<"widget"  << wid;
        qDebug() << Q_FUNC_INFO << "HoverLeave";
        QString str = QString("HoverLeave : %1\n").arg(wid->objectName());
        ui->textEdit->append(str);

    }
    break;
        // touch

    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    {
        qDebug() << " e->type " <<  event->type();

    }
    break;
    case QEvent::MouseButtonRelease:
        qDebug() << " e->type " <<  event->type();
        break;
    case QEvent::MouseButtonPress:
        qDebug() << " e->type " <<  event->type();
        break;
    default:
        break;
    };

    return false;
}

void Dialog::tabletEvent(QTabletEvent *event)
{
    switch (event->type())
    {
    case QEvent::TabletPress:
    case QEvent::TabletMove:
    {
        qDebug() << " e->type " <<  event->type();
        break;
    }
    case QEvent::TabletRelease:
    {
        qDebug() << " e->type " <<  event->type();
        update();
        break;
    }
    default:
    {
        break;
    }
    }
    QDialog::tabletEvent(event);
}

void Dialog::touchEvent(QTouchEvent *event)
{
    QTouchEvent *touchEvent = static_cast<QTouchEvent *>(event);

    if (event->isPointerEvent()) {
        QPointerEvent *pev = static_cast<QPointerEvent *>(event);

        QString str = QString("PointerEvent : %1\n").arg(pev->pointCount());
        ui->textEdit->append(str);
    }

    QString str;
    switch(touchEvent->type()) {
    case QEvent::TouchBegin:
        str = QString("TouchBegin : %1 \n").arg(str);
        break;
    case QEvent::TouchUpdate:
        str = QString("TouchUpdate : %1 \n").arg(str);
        break;
    case QEvent::TouchEnd:
        str = QString("TouchEnd : %1 \n").arg(str);
        break;
    case QEvent::TouchCancel:
        str = QString("TouchCancel : %1 \n").arg(str);
        break;

    };
    ui->textEdit->append(str);
}

