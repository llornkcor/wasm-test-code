#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimeZone>

#ifdef Q_OS_WASM
#include <emscripten.h>
#include <emscripten/bind.h>
using namespace emscripten;
#endif



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    getTimeZone();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::getTimeZone()
{ // timedatectl list-timezones
//#!/bin/bash
//#!/bin/sh
//# Timezones and current offsets
//ZONES="$(timedatectl list-timezones)"

//for ZONE in $ZONES
//do
//    OFFSET="$(env TZ=${ZONE} date +%:z)"
//        echo "$ZONE $OFFSET"
//        done


//#ifdef Q_OS_WASM
//        emscripten::val intl = emscripten::val::global("Intl");
//        emscripten::val dtformat = intl.call<val>("DateTimeFormat");
//        emscripten::val options = dtformat.call<val>("resolvedOptions");
//        emscripten::val tz = options["timeZone"];

//        if (!tz.isUndefined()) {
//        std::string tzStr = tz.as<std::string>();
//        qDebug() << Q_FUNC_INFO << "current timezone:" << QString::fromStdString(tzStr);
//        }

//        emscripten::val tzdate = emscripten::val::global("Date").new_();
//        emscripten::val tzOffset = tzdate.call<val>("getTimezoneOffset");

//        if (!tzOffset.isUndefined()) {
//        int tzOffsetStr = tzOffset.as<int>() * -60;
//        qDebug() << Q_FUNC_INFO << "current timezone offset:" << tzOffsetStr;
//        }
/*
 * daylight offset
 * Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}
Date.prototype.isDstObserved = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}
var today = new Date();
if (today.isDstObserved()) {
    alert ("Daylight saving time!");
}
 * */

//#else
qDebug() << QTimeZone::systemTimeZoneId();
qDebug() << QTimeZone::systemTimeZone().standardTimeOffset(QDateTime::currentDateTime());
//#endif
}
