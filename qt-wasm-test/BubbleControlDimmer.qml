import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0
import com.bj.freeathome.cf 1.0

Popup {
    property point openAtPos: "0,0"

    width: 300
    height: 500
    padding: 0
    modal: true
    background.visible: false

    transformOrigin: Popup.TopLeft

    Overlay.modal: Rectangle {
        color: Style.modalOverlay
    }

    enter: Transition {
        NumberAnimation { property: "opacity"; from: 0.0; to: 1.0 }
        NumberAnimation { property: "scale"; from: 0.0; to: 1.0 }
        NumberAnimation { property: "x"; from: openAtPos.x; to: parent.width / 2 - width / 2 }
        NumberAnimation { property: "y"; from: openAtPos.y; to: parent.height / 2 - height / 2 }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
        NumberAnimation { property: "scale"; from: 1.0; to: 0.0 }
        NumberAnimation { property: "x"; from: parent.width / 2 - width / 2; to: openAtPos.x }
        NumberAnimation { property: "y"; from: parent.height / 2 - height / 2; to: openAtPos.y }
    }

    DimmerController {
        id: bcController
    }

    Image {
        id: bgImg
        source: "images/gradient-1.png"
        anchors.fill: parent
        fillMode: Image.Stretch

        layer.enabled: true
        layer.effect: OpacityMask {
            maskSource: Rectangle {
                color: "black"
                radius: 12
                border.width: 2
                width: bgImg.width
                height: bgImg.height
            }
        }
    }

    Rectangle {
        id: popupBackground
        anchors.fill: parent
        color: "transparent"
        radius: 12
        border.color: Style.bcBorder
        border.width: 2
    }

    Text {
        id: header
        text: "\u24B7 Dimmitri (AKP)"
        x: 18
        y: 16
        font.pixelSize: 16
        font.family: Style.font.name
        color: "white"
    }

    // Border between header text and bubble
    Rectangle {
        height: 1
        width: parent.width - 2 * popupBackground.border.width
        color: Style.bcSplitline
        y: 50
        x: popupBackground.border.width
    }

    Bubble {
        anchors.horizontalCenter: parent.horizontalCenter
        y: 100 - height / 2
        iconSource: bcController.on ? "\uEEDD" : "\uEEDC"
        glowOpacity: bcController.dimmingValue / 100

        MouseArea {
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            onPressed: bcController.toggle()
        }
    }

    // Border between bubble and slide area
    Rectangle {
        id: slideSeparator
        height: 1
        width: parent.width - 2 * popupBackground.border.width
        color: Style.bcSplitline
        y: 150
        x: popupBackground.border.width
    }

    SlideArea {
        id: bcDimmerSlider
        anchors.top: slideSeparator.bottom
        anchors.bottom: footerSeparator.top
        width: parent.width
        channelController: bcController
    }

    // Border between slide area and favorite toggling footer
    Rectangle {
        id: footerSeparator
        height: 1
        width: parent.width - 2 * popupBackground.border.width
        color: Style.bcSplitline
        y: 450
        x: popupBackground.border.width
    }

    // Footer; toggles favorite
    Row {
        anchors.top: footerSeparator.bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter

        FontIcon {
            icon: "15_035_menu_dashboard"
            size: 40
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
        }

        Text {
            text: qsTr("Add to favorites")
            anchors.verticalCenter: parent.verticalCenter
            color: "white"
        }
    }

    MouseArea {
        anchors.top: footerSeparator.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        cursorShape: "PointingHandCursor"
        onPressed: console.log("TODO: toggle favorite")
    }
}
