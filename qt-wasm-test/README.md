# QT WASM Playground

Here's a list of problems I found so far:

 * The background of the scroll container should be a gradient / image
 * The background of the top right button should be a gradient / image
 * The decoration of the list items on the left is missing - until you select one. They disappear again when scrolling the bubble scroll view.
 * If you *long tap* one of the round bubbles, a popup appears:
   * markers in front of the numbers on the left missing
   * a gradientcombination on the bottom of the mouse area missing
   * the font character (icon) is sometimes optically broken
 * Scrolling does not work with a touch screen. Maybe mouse vs pointer events. Should use pointer events, because every WASM capable browser will be able to send them.
 * When scrolling with the Mouse by dragging the bubble view, it fails to detect the "mouse up" outside the window (drag out of the window and release the mouse button there). I guess it should listen on cancel events, too.
