import QtQuick 2.0

Rectangle {
    property string label

    height: 60
    width: parent.width
    color: Style.appMenuListItemBg

    Text {
        text: label
        anchors.verticalCenter: parent.verticalCenter
        font.family: Style.font.name
        font.pixelSize: 16
        color: appMenuItemMouseArea.containsMouse ? "white" : Style.appMenuLabel
        x: 20
    }
    Rectangle {
        color: Style.appMenuItemBorderDark
        height: 1
        width: parent.width
        y: parent.height - 2
    }
    Rectangle {
        color: Style.appMenuItemBorderLight
        height: 1
        width: parent.width
        y: parent.height - 1
    }
    MouseArea {
        id: appMenuItemMouseArea
        anchors.fill: parent
        cursorShape: "PointingHandCursor"
        hoverEnabled: true
    }
}
