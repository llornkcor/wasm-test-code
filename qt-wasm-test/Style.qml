pragma Singleton
import QtQuick 2.11

QtObject {
    function rgb(r, g, b) {
        return Qt.rgba(r/255, g/255, b/255)
    }

    function rgba(r, g, b, alpha) {
        return Qt.rgba(r/255, g/255, b/255, alpha)
    }

    // Background
    readonly property color mainBackground: rgb(33, 43, 53)
    readonly property color modalOverlay: rgba(36, 100, 109, 0.5)
    readonly property color appMenuBackground: rgb(27, 38, 50)
    readonly property color appMenuListItemBg: rgb(36, 46, 58)
    readonly property color slideAreaTooltip: rgba(0, 0, 0, 0.6)

    // Function
    readonly property color functionLight: "#dde641"

    // Foreground
    readonly property color cfLabel: rgb(105, 125, 150)
    readonly property color appMenuLabel: rgb(152, 182, 219)
    readonly property color fgS09: rgba(245, 245, 245, 1.0)

    // Gradients
    readonly property color cfGradientTop: rgb(47, 72, 99)
    readonly property color cfGradientBottom: rgb(28, 39, 56)

    // Borders
    readonly property color bcBorder: rgba(255, 255, 255, 0.4)
    readonly property color bcSplitline: rgba(2, 2, 4, 0.4)
    readonly property color slideAreaBlock: rgb(69, 85, 99)
    readonly property color appMenuItemBorderDark: rgb(17, 21, 26)
    readonly property color appMenuItemBorderLight: rgb(51, 64, 77)

    // Shadows
    readonly property color cfShadow: "#80020402"

    // Fonts
    readonly property FontLoader font: FontLoader {
        source: "fonts/FreeSans.ttf"
    }

    readonly property FontLoader icons: FontLoader {
        source: "fonts/IconFont.ttf"
    }
}
