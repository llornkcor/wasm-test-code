import QtQuick 2.11
import QtGraphicalEffects 1.0
import "util.js" as Util
import com.bj.freeathome.cf 1.0

Rectangle {
    property DimmerController channelController
    color: "transparent"

    LinearGradient {
        opacity: Util.mapRange(channelController.dimmingValue, 0, 100, 0.1, 0.9)
        anchors.fill: parent
        start: Qt.point(0, 0)
        end: Qt.point(0, parent.height)
        gradient: Gradient {
            GradientStop { position: 0.0; color: Qt.rgba(1, 1, 1, 1) }
            GradientStop { position: 1.0; color: Qt.rgba(1, 1, 1, 0) }
        }
        visible: slide.isDragging
    }

    // Block border
    Rectangle {
        id: blockBorder
        color: Style.slideAreaBlock
        height: 2
        width: parent.width - 4
        x: 2
        y: parent.height - parent.height / 12
    }

    // Block
    Row {
        id: blockRect
        anchors.top: blockBorder.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 2
        anchors.rightMargin: 2
        anchors.topMargin: -2
        clip: false

        // repeating-linear-gradient(-45deg, #455563, #455563 3px, transparent 3px, transparent 6px);

        Repeater {
            model: 25
            Row {
                Rectangle {
                    height: 3 * blockRect.height
                    width: 3
                    color: "#455563"
                    transform: Rotation {
                        angle: 45
                        origin.x: 0
                        origin.y: 0
                    }
                }
                Rectangle {
                    height: 3 * blockRect.height
                    width: 3
                    color: "transparent"
                    transform: Rotation {
                        angle: 45
                        origin.x: 0
                        origin.y: 0
                    }
                }
                Rectangle {
                    height: 3 * blockRect.height
                    width: 3
                    color: "#455563"
                    transform: Rotation {
                        angle: 45
                        origin.x: 0
                        origin.y: 0
                    }
                }
                Rectangle {
                    height: 3 * blockRect.height
                    width: 6
                    color: "transparent"
                    transform: Rotation {
                        angle: 45
                        origin.x: 0
                        origin.y: 0
                    }
                }
            }
        }
    }

    // Ruler
    Column {
        x: 8
        height: parent.height

        ScaleRuler {
            label: "100 %"
            height: parent.height / 6
        }
        ScaleRuler {
            label: "80 %"
            height: parent.height / 6
        }
        ScaleRuler {
            label: "60 %"
            height: parent.height / 6
        }
        ScaleRuler {
            label: "40 %"
            height: parent.height / 6
        }
        ScaleRuler {
            label: "20 %"
            height: parent.height / 6
        }
        ScaleRuler {
            label: "0 %"
            height: parent.height / 6
        }
    }

    Slide {
        id: slide
        anchors.fill: parent
        labelPostfix: " %"
        channelController: parent.channelController
    }
}
