var ICON_MAP = {
      "01": {
        "001": [
          {
            "icon": "01_001_housesetup",
            "index": 61000,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "003": [
          {
            "icon": "01_003_identifiction",
            "index": 61001,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "004": [
          {
            "icon": "01_004_pairobjects",
            "index": 61002,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "005": [
          {
            "icon": "01_005_timer",
            "index": 61003,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "006": [
          {
            "icon": "01_006_equippanels",
            "index": 61004,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "007": [
          {
            "icon": "01_007_alerts",
            "index": 61005,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "008": [
          {
            "icon": "01_008_homesecurity",
            "index": 61006,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ]
      },
      "02": {
        "001": [
          {
            "icon": "02_001_user",
            "index": 61007,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "01"
          }
        ],
        "002": [
          {
            "icon": "02_002_deviceconfig",
            "index": 61008,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "01"
          }
        ],
        "003": [
          {
            "icon": "02_003_settings",
            "index": 61009,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "01"
          }
        ],
        "004": [
          {
            "icon": "02_004_help",
            "index": 61010,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "01"
          }
        ]
      },
      "03": {
        "001": [
          {
            "icon": "03_001_housesetup_floor_attic",
            "index": 61011,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "002": [
          {
            "icon": "03_002_housesetup_floor_regular",
            "index": 61012,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "003": [
          {
            "icon": "03_003_housesetup_floor_basement",
            "index": 61013,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "004": [
          {
            "icon": "03_004_housesetup_room_rectangle",
            "index": 61014,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "005": [
          {
            "icon": "03_005_housesetup_room_square",
            "index": 61015,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "006": [
          {
            "icon": "03_006_housesetup_room_corner",
            "index": 61016,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "007": [
          {
            "icon": "03_007_timer",
            "index": 61019,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "008": [
          {
            "icon": "03_008_timer_add",
            "index": 61020,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "009": [
          {
            "icon": "03_009_panel",
            "index": 61021,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "011": [
          {
            "icon": "03_011_timer_active",
            "index": 61022,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "012": [
          {
            "icon": "03_012_alerts",
            "index": 61023,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "013": [
          {
            "icon": "03_013_alerts_active",
            "index": 61024,
            "states": [
              "def",
              "foc",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "020": [
          {
            "icon": "03_020_dropdown_up",
            "index": 61025,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "021": [
          {
            "icon": "03_021_dropdown_down",
            "index": 61026,
            "states": [
              "sel"
            ],
            "style": "04"
          }
        ],
        "022": [
          {
            "icon": "03_022_timerview_selected",
            "index": 61027,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "023": [
          {
            "icon": "03_023_timerview_activated",
            "index": 61028,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "024": [
          {
            "icon": "03_024_timerview_all",
            "index": 61029,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "025": [
          {
            "icon": "03_025_perimetral_zone",
            "index": 61030,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "026": [
          {
            "icon": "03_026_indoor_zone",
            "index": 61031,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "027": [
          {
            "icon": "03_027_outdoor_zone",
            "index": 61032,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "028": [
          {
            "icon": "03_028_custom_zone",
            "index": 61033,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "080": [
          {
            "icon": "03_080_housesetup_room_p",
            "index": 61017,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ],
        "081": [
          {
            "icon": "03_081_housesetup_room_t",
            "index": 61018,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "06"
          }
        ]
      },
      "04": {
        "003": [
          {
            "icon": "04_003_library",
            "index": 61034,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "004": [
          {
            "icon": "04_004_floorplan",
            "index": 61035,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "005": [
          {
            "icon": "04_005_timeline",
            "index": 61036,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "006": [
          {
            "icon": "04_006_panel",
            "index": 61037,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "007": [
          {
            "icon": "04_007_settings",
            "index": 61038,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "008": [
          {
            "icon": "04_008_device_configuration",
            "index": 61039,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "009": [
          {
            "icon": "04_009_maximize",
            "index": 61040,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "010": [
          {
            "icon": "04_010_editmode",
            "index": 61041,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "011": [
          {
            "icon": "04_011_delete",
            "index": 61042,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "012": [
          {
            "icon": "04_012_alerts",
            "index": 61043,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "013": [
          {
            "icon": "04_013_zone_plan",
            "index": 61044,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ]
      },
      "05": {
        "002": [
          {
            "icon": "05_002_application_switching",
            "index": 61045,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "003": [
          {
            "icon": "05_003_button_cancel",
            "index": 61046,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "004": [
          {
            "icon": "05_004_button_save",
            "index": 61047,
            "states": [
              "def",
              "foc",
              "prs",
              "dis",
              "act"
            ],
            "style": "01.05"
          }
        ],
        "005": [
          {
            "icon": "05_005_button_houseview",
            "index": 61048,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "006": [
          {
            "icon": "05_006_button_add",
            "index": 61049,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "007": [
          {
            "icon": "05_007_button_previous",
            "index": 61050,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "008": [
          {
            "icon": "05_008_button_next",
            "index": 61051,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "009": [
          {
            "icon": "05_009_button_menu",
            "index": 61052,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "012": [
          {
            "icon": "05_012_dropdown_up",
            "index": 61053,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "013": [
          {
            "icon": "05_013_dropdown_down",
            "index": 61054,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "014": [
          {
            "icon": "05_014_radiobutton-plane",
            "index": 61055,
            "states": [
              "def-sec-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-line",
            "index": 61056,
            "states": [
              "def-sec-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-dot",
            "index": 61057,
            "states": [
              "def-sec-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-plane",
            "index": 61058,
            "states": [
              "def-pri-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-line",
            "index": 61059,
            "states": [
              "def-pri-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-dot",
            "index": 61060,
            "states": [
              "def-pri-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-plane",
            "index": 61061,
            "states": [
              "foc-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-line",
            "index": 61062,
            "states": [
              "foc-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-dot",
            "index": 61063,
            "states": [
              "foc-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-plane",
            "index": 61064,
            "states": [
              "prs-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-line",
            "index": 61065,
            "states": [
              "prs-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-dot",
            "index": 61066,
            "states": [
              "prs-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-plane",
            "index": 61067,
            "states": [
              "dis-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-line",
            "index": 61068,
            "states": [
              "dis-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_014_radiobutton-dot",
            "index": 61069,
            "states": [
              "dis-3"
            ],
            "style": "07"
          }
        ],
        "015": [
          {
            "icon": "05_015_checkbox-plane",
            "index": 61070,
            "states": [
              "def-sec-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61071,
            "states": [
              "def-sec-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61072,
            "states": [
              "def-pri-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61073,
            "states": [
              "def-pri-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61074,
            "states": [
              "foc-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61075,
            "states": [
              "foc-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61076,
            "states": [
              "prs-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61077,
            "states": [
              "prs-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61078,
            "states": [
              "dis-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61079,
            "states": [
              "dis-2"
            ],
            "style": "07"
          }
        ],
        "016": [
          {
            "icon": "05_016_checkbox-plane",
            "index": 61080,
            "states": [
              "def-sec-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61081,
            "states": [
              "def-sec-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61082,
            "states": [
              "def-sec-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61083,
            "states": [
              "def-pri-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61084,
            "states": [
              "def-pri-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61085,
            "states": [
              "def-pri-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61086,
            "states": [
              "foc-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61087,
            "states": [
              "foc-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61088,
            "states": [
              "foc-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61089,
            "states": [
              "prs-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61090,
            "states": [
              "prs-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61091,
            "states": [
              "prs-3"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61092,
            "states": [
              "dis-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61093,
            "states": [
              "dis-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61094,
            "states": [
              "dis-3"
            ],
            "style": "07"
          }
        ],
        "017": [
          {
            "icon": "05_017_button_delete",
            "index": 61095,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "018": [
          {
            "icon": "05_018_button_transfer_timer",
            "index": 61096,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "019": [
          {
            "icon": "05_019_minus",
            "index": 61097,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "020": [
          {
            "icon": "05_020_plus",
            "index": 61098,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "021": [
          {
            "icon": "05_021_button_add_object",
            "index": 61099,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04.01"
          }
        ],
        "022": [
          {
            "icon": "05_022_dimmer_handle",
            "index": 61100,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "025": [
          {
            "icon": "05_025_button_back",
            "index": 61101,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "026": [
          {
            "icon": "05_026_button_swap",
            "index": 61102,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "030": [
          {
            "icon": "05_030_radiobutton-plane",
            "index": 61103,
            "states": [
              "def-sec-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-line",
            "index": 61104,
            "states": [
              "def-sec-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-plane",
            "index": 61105,
            "states": [
              "def-pri-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-line",
            "index": 61106,
            "states": [
              "def-pri-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-plane",
            "index": 61107,
            "states": [
              "foc-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-line",
            "index": 61108,
            "states": [
              "foc-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-plane",
            "index": 61109,
            "states": [
              "prs-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-line",
            "index": 61110,
            "states": [
              "prs-2"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-plane",
            "index": 61111,
            "states": [
              "dis-1"
            ],
            "style": "07"
          },
          {
            "icon": "05_030_radiobutton-line",
            "index": 61112,
            "states": [
              "dis-2"
            ],
            "style": "07"
          }
        ]
      },
      "06": {
        "005": [
          {
            "icon": "06_005_delete",
            "index": 61113,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "006": [
          {
            "icon": "06_006_minus",
            "index": 61114,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "007": [
          {
            "icon": "06_007_plus",
            "index": 61115,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "008": [
          {
            "icon": "06_008_drilldown",
            "index": 61116,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "009": [
          {
            "icon": "06_009_edit",
            "index": 61117,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "010": [
          {
            "icon": "06_010_device",
            "index": 61118,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "011": [
          {
            "icon": "06_011_backup",
            "index": 61119,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "012": [
          {
            "icon": "06_012_import",
            "index": 61120,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "015": [
          {
            "icon": "06_015_cancel",
            "index": 61121,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "016": [
          {
            "icon": "06_016_fitinview",
            "index": 61122,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "017": [
          {
            "icon": "06_017_check",
            "index": 61123,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "100": [
          {
            "icon": "06_100_filter_lights",
            "index": 61124,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_100_filter_lights-on",
            "index": 61125,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "101": [
          {
            "icon": "06_101_filter_blinds",
            "index": 61126,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_101_filter_blinds-on",
            "index": 61127,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "102": [
          {
            "icon": "06_102_filter_scenes",
            "index": 61128,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_102_filter_scenes-on",
            "index": 61129,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "103": [
          {
            "icon": "06_103_filter_temperature",
            "index": 61130,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_103_filter_temperature-on",
            "index": 61131,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "104": [
          {
            "icon": "06_104_filter_sensors",
            "index": 61132,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_104_filter_sensors-on",
            "index": 61133,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "105": [
          {
            "icon": "06_105_filter_remain",
            "index": 61134,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          },
          {
            "icon": "06_105_filter_remain-on",
            "index": 61135,
            "states": [
              "act"
            ],
            "style": "04"
          }
        ],
        "106": [
          {
            "icon": "06_106_cable_device",
            "index": 61136,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "107": [
          {
            "icon": "06_107_wireless_device",
            "index": 61137,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "108": [
          {
            "icon": "06_108_failure_device",
            "index": 61138,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "109": [
          {
            "icon": "06_109_actualize_device",
            "index": 61139,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "110": [
          {
            "icon": "06_110_filter",
            "index": 61140,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ],
        "120": [
          {
            "icon": "06_120_external_device",
            "index": 61141,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "205": [
          {
            "icon": "15_205_access",
            "index": 61142,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "04"
          }
        ]
      },
      "07": {
        "001": [
          {
            "icon": "07_001_warning",
            "index": 61143,
            "states": [
              "foc"
            ],
            "style": "09"
          }
        ],
        "002": [
          {
            "icon": "07_002_astro_day",
            "index": 61144,
            "states": [
              "def"
            ],
            "style": "03.02"
          }
        ],
        "003": [
          {
            "icon": "07_003_astro_night",
            "index": 61145,
            "states": [
              "def"
            ],
            "style": "03.02"
          }
        ],
        "004": [
          {
            "icon": "07_004_alerts_label_arrow",
            "index": 61146,
            "states": [
              "def"
            ],
            "style": "02"
          }
        ],
        "005": [
          {
            "icon": "07_005_alerts_arrow",
            "index": 61147,
            "states": [
              "def"
            ],
            "style": "02"
          }
        ]
      },
      "08": {
        "001": [
          {
            "icon": "08_001_light-line",
            "index": 61148,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_001_light-plane",
            "index": 61149,
            "states": [
              "act"
            ],
            "style": "01.01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          }
        ],
        "003": [
          {
            "icon": "08_003_blind-ani-1",
            "index": 61150,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-ani-2",
            "index": 61151,
            "states": [
              "ani-2"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-ani-3",
            "index": 61152,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-ani-4",
            "index": 61153,
            "states": [
              "ani-4"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-ani-5",
            "index": 61154,
            "states": [
              "ani-5"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-ani-6",
            "index": 61155,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-up",
            "index": 61156,
            "states": [
              "up"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-down",
            "index": 61157,
            "states": [
              "down"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-unkn",
            "index": 61158,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "004": [
          {
            "icon": "08_004_lightmode-inter",
            "index": 61159,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_004_lightmode-plane",
            "index": 61160,
            "states": [
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_004_lightmode-line",
            "index": 61161,
            "states": [
              "deact"
            ],
            "style": "01"
          }
        ],
        "005": [
          {
            "icon": "08_005_radiator_sensor",
            "index": 61162,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "006": [
          {
            "icon": "08_006_radiator-line",
            "index": 61163,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_006_radiator-plane",
            "index": 61164,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "007": [
          {
            "icon": "08_007_heating",
            "index": 61165,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_007_heating",
            "index": 61166,
            "states": [
              "heating"
            ],
            "style": "01.02"
          }
        ],
        "008": [
          {
            "icon": "08_008_cooling",
            "index": 61167,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_008_cooling",
            "index": 61168,
            "states": [
              "cooling"
            ],
            "style": "01.02"
          }
        ],
        "009": [
          {
            "icon": "08_009_fan",
            "index": 61169,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-1",
            "index": 61170,
            "states": [
              "ani-1"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-2",
            "index": 61171,
            "states": [
              "ani-2"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-3",
            "index": 61172,
            "states": [
              "ani-3"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-4",
            "index": 61173,
            "states": [
              "ani-4"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-5",
            "index": 61174,
            "states": [
              "ani-5"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-6",
            "index": 61175,
            "states": [
              "ani-6"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-7",
            "index": 61176,
            "states": [
              "ani-7"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-8",
            "index": 61177,
            "states": [
              "ani-8"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-9",
            "index": 61178,
            "states": [
              "ani-9"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-10",
            "index": 61179,
            "states": [
              "ani-10"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-11",
            "index": 61180,
            "states": [
              "ani-11"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-12",
            "index": 61181,
            "states": [
              "ani-12"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-13",
            "index": 61182,
            "states": [
              "ani-13"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-14",
            "index": 61183,
            "states": [
              "ani-14"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-15",
            "index": 61184,
            "states": [
              "ani-15"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-16",
            "index": 61185,
            "states": [
              "ani-16"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-17",
            "index": 61186,
            "states": [
              "ani-17"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-18",
            "index": 61187,
            "states": [
              "ani-18"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-19",
            "index": 61188,
            "states": [
              "ani-19"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-20",
            "index": 61189,
            "states": [
              "ani-20"
            ],
            "style": "01"
          },
          {
            "icon": "08_009_fan-ani-21",
            "index": 61190,
            "states": [
              "ani-21"
            ],
            "style": "01"
          }
        ],
        "010": [
          {
            "icon": "08_010_outlet-line",
            "index": 61191,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_010_outlet-power",
            "index": 61192,
            "states": [
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_010_outlet-plane",
            "index": 61193,
            "states": [
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          }
        ],
        "011": [
          {
            "icon": "08_011_actuator-on-off",
            "index": 61194,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_011_actuator-on",
            "index": 61195,
            "states": [
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_011_actuator-off",
            "index": 61196,
            "states": [
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          }
        ],
        "012": [
          {
            "icon": "08_012_scene-line",
            "index": 61197,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_012_scene-plane",
            "index": 61198,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "013": [
          {
            "icon": "08_013_group_light-line",
            "index": 61199,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          },
          {
            "icon": "08_013_group_light-plane",
            "index": 61200,
            "states": [
              "act"
            ],
            "style": "01.01",
            "tags": [
              "lock-closed",
              "lock-opened"
            ]
          }
        ],
        "014": [
          {
            "icon": "08_014_group_blind-ani-1",
            "index": 61201,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-ani-2",
            "index": 61202,
            "states": [
              "ani-2"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-ani-3",
            "index": 61203,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-ani-4",
            "index": 61204,
            "states": [
              "ani-4"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-ani-5",
            "index": 61205,
            "states": [
              "ani-5",
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-up",
            "index": 61206,
            "states": [
              "up"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-down",
            "index": 61207,
            "states": [
              "down"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_014_group_blind-unkn",
            "index": 61208,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "015": [
          {
            "icon": "08_015_heating_extra",
            "index": 61209,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "016": [
          {
            "icon": "08_016_temperature_sensor",
            "index": 61210,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "017": [
          {
            "icon": "08_017_actuator_impulse",
            "index": 61211,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_017_actuator_impulse-on",
            "index": 61212,
            "states": [
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_017_actuator_impulse-off",
            "index": 61213,
            "states": [
              "deact"
            ],
            "style": "01"
          }
        ],
        "020": [
          {
            "icon": "08_020_door-line",
            "index": 61214,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_020_door-plane",
            "index": 61215,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "021": [
          {
            "icon": "08_021_light_timed-line",
            "index": 61216,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01"
          },
          {
            "icon": "08_021_light_timed-plane",
            "index": 61217,
            "states": [
              "act"
            ],
            "style": "01.01"
          }
        ],
        "022": [
          {
            "icon": "08_022_light_forced-line",
            "index": 61218,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01"
          },
          {
            "icon": "08_022_light_forced-plane",
            "index": 61219,
            "states": [
              "act"
            ],
            "style": "01.01"
          }
        ],
        "024": [
          {
            "icon": "08_024_blind_forced-ani-1",
            "index": 61220,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-ani-2",
            "index": 61221,
            "states": [
              "ani-2"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-ani-3",
            "index": 61222,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-ani-4",
            "index": 61223,
            "states": [
              "ani-4"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-ani-5",
            "index": 61224,
            "states": [
              "ani-5"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-ani-6",
            "index": 61225,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-up",
            "index": 61226,
            "states": [
              "up"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-down",
            "index": 61227,
            "states": [
              "down"
            ],
            "style": "01"
          },
          {
            "icon": "08_024_blind_forced-unkn",
            "index": 61228,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "025": [
          {
            "icon": "08_025_blind_unforced-ani-1",
            "index": 61229,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-ani-2",
            "index": 61230,
            "states": [
              "ani-2"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-ani-3",
            "index": 61231,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-ani-4",
            "index": 61232,
            "states": [
              "ani-4"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-ani-5",
            "index": 61233,
            "states": [
              "ani-5"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-ani-6",
            "index": 61234,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-up",
            "index": 61235,
            "states": [
              "up"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-down",
            "index": 61236,
            "states": [
              "down"
            ],
            "style": "01"
          },
          {
            "icon": "08_025_blind_unforced-unkn",
            "index": 61237,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "026": [
          {
            "icon": "08_026_light_unforced-line",
            "index": 61238,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01"
          },
          {
            "icon": "08_026_light_unforced-plane",
            "index": 61239,
            "states": [
              "act"
            ],
            "style": "01.01"
          }
        ],
        "030": [
          {
            "icon": "08_030_switch_regular-line",
            "index": 61240,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_030_switch_regular-plane",
            "index": 61241,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "031": [
          {
            "icon": "08_031_switch_rockerswitch-line",
            "index": 61242,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_031_switch_rockerswitch-plane",
            "index": 61243,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "032": [
          {
            "icon": "08_032_switch_panel",
            "index": 61244,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "033": [
          {
            "icon": "08_033_switch_rtc-comfortmode-regular",
            "index": 61245,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-comfortmode-heating",
            "index": 61246,
            "states": [
              "heating"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-comfortmode-cooling",
            "index": 61247,
            "states": [
              "cooling"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-comfortmode-fan",
            "index": 61248,
            "states": [
              "fan"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-eco",
            "index": 61249,
            "states": [
              "eco"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-off",
            "index": 61250,
            "states": [
              "deact"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-antifreeze",
            "index": 61251,
            "states": [
              "freeze"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-dewpoint",
            "index": 61252,
            "states": [
              "dew"
            ],
            "style": "01.02"
          },
          {
            "icon": "08_033_switch_rtc-condensedwater",
            "index": 61253,
            "states": [
              "cond"
            ],
            "style": "01.02"
          }
        ],
        "041": [
          {
            "icon": "08_041_pushbutton_double-line",
            "index": 61254,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_041_pushbutton_double-plane",
            "index": 61255,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "042": [
          {
            "icon": "08_042_pushbutton_quad-line",
            "index": 61256,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_042_pushbutton_quad-plane",
            "index": 61257,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "044": [
          {
            "icon": "08_044_rtc_sensor_heating",
            "index": 61258,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01.02"
          }
        ],
        "045": [
          {
            "icon": "08_045_rtc_sensor_cooling",
            "index": 61259,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01.03"
          }
        ],
        "046": [
          {
            "icon": "08_046_rtc_sensor_antifreeze",
            "index": 61260,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "047": [
          {
            "icon": "08_047_rtc_sensor_dewpoint",
            "index": 61261,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "048": [
          {
            "icon": "08_048_rtc_sensor_condensedwater",
            "index": 61262,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "049": [
          {
            "icon": "08_049_rtc_sensor_fan",
            "index": 61263,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "050": [
          {
            "icon": "08_050_windalarm-act",
            "index": 61264,
            "states": [
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_050_windalarm-deact",
            "index": 61265,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          }
        ],
        "051": [
          {
            "icon": "08_051_notification",
            "index": 61266,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "052": [
          {
            "icon": "08_052_lined_unit",
            "index": 61267,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "053": [
          {
            "icon": "08_053_built_in_unit",
            "index": 61268,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "054": [
          {
            "icon": "08_054_undefined_unit",
            "index": 61269,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "055": [
          {
            "icon": "08_055_weatherstation-line",
            "index": 61270,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_055_weatherstation-plane",
            "index": 61271,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "056": [
          {
            "icon": "08_056_scene_lighton-line",
            "index": 61272,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_056_scene_lighton-plane",
            "index": 61273,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "057": [
          {
            "icon": "08_057_scene_lightoff-line",
            "index": 61274,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_057_scene_lightoff-plane",
            "index": 61275,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "058": [
          {
            "icon": "08_058_scene_blindup-line",
            "index": 61276,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_058_scene_blindup-plane",
            "index": 61277,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "059": [
          {
            "icon": "08_059_scene_blinddown-line",
            "index": 61278,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_059_scene_blinddown-plane",
            "index": 61279,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "060": [
          {
            "icon": "08_060_scene_day-line",
            "index": 61280,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_060_scene_day-plane",
            "index": 61281,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "061": [
          {
            "icon": "08_061_scene_night-line",
            "index": 61282,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_061_scene_night-plane",
            "index": 61283,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "062": [
          {
            "icon": "08_062_scene_sleep-line",
            "index": 61284,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_062_scene_sleep-plane",
            "index": 61285,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "063": [
          {
            "icon": "08_063_scene_party",
            "index": 61286,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_063_scene_party-line",
            "index": 61287,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_063_scene_party-plane",
            "index": 61288,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "064": [
          {
            "icon": "08_064_scene_cinema-line",
            "index": 61289,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_064_scene_cinema-plane",
            "index": 61290,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "065": [
          {
            "icon": "08_065_scene_dinner-line",
            "index": 61291,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_065_scene_dinner-plane",
            "index": 61292,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "067": [
          {
            "icon": "08_067_scene_leaving_home-line",
            "index": 61293,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_067_scene_leaving_home-plane",
            "index": 61294,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "080": [
          {
            "icon": "08_080_sensor_binary",
            "index": 61295,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "081": [
          {
            "icon": "08_081_sensor_window-act",
            "index": 61296,
            "states": [
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_081_sensor_window-deact",
            "index": 61297,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          }
        ],
        "082": [
          {
            "icon": "08_082_sensor_motion",
            "index": 61298,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "083": [
          {
            "icon": "08_083_sensor_doorbell",
            "index": 61299,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "100": [
          {
            "icon": "08_100_des_open-line",
            "index": 61300,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_100_des_open-plane",
            "index": 61301,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "101": [
          {
            "icon": "08_101_des_open_door-line",
            "index": 61302,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_101_des_open_door-plane",
            "index": 61303,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "102": [
          {
            "icon": "08_102_des_open_auto-line",
            "index": 61304,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_102_des_open_auto-plane",
            "index": 61305,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "103": [
          {
            "icon": "08_103_des_light-line",
            "index": 61306,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01.01"
          },
          {
            "icon": "08_103_des_light-plane",
            "index": 61307,
            "states": [
              "act"
            ],
            "style": "01.01"
          }
        ],
        "104": [
          {
            "icon": "08_104_des_levelcall-on",
            "index": 61308,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_104_des_levelcall-off",
            "index": 61309,
            "states": [
              "deact"
            ],
            "style": "01"
          }
        ],
        "105": [
          {
            "icon": "08_105_des_levelcall_sensor-on",
            "index": 61310,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_105_des_levelcall_sensor-off",
            "index": 61311,
            "states": [
              "deact"
            ],
            "style": "01"
          }
        ],
        "106": [
          {
            "icon": "08_106_rgb_light",
            "index": 61312,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_106_rgb_light-inter",
            "index": 61313,
            "states": [
              "inter"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_106_rgb_light-off",
            "index": 61314,
            "states": [
              "deact"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_106_rgb_light-on",
            "index": 61315,
            "states": [
              "act-1"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_106_rgb_light-addon",
            "index": 61316,
            "states": [
              "act-3"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_106_rgb_light-on",
            "index": 61317,
            "states": [
              "act-4"
            ],
            "style": "01.06"
          }
        ],
        "109": [
          {
            "icon": "08_109_rgb_grouplight",
            "index": 61318,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-inter",
            "index": 61319,
            "states": [
              "inter"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-off",
            "index": 61320,
            "states": [
              "deact"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-on-1",
            "index": 61321,
            "states": [
              "act-1"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-on-2",
            "index": 61322,
            "states": [
              "act-2"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-addon",
            "index": 61323,
            "states": [
              "act-3"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-on-1",
            "index": 61324,
            "states": [
              "act-4"
            ],
            "style": "01.06"
          },
          {
            "icon": "08_109_rgb_grouplight-on-2",
            "index": 61325,
            "states": [
              "act-5"
            ],
            "style": "01.06"
          }
        ],
        "114": [
          {
            "icon": "08_114_alert",
            "index": 61326,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_114_alert-on",
            "index": 61327,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "115": [
          {
            "icon": "08_115_timer",
            "index": 61328,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_115_timer-on",
            "index": 61329,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "140": [
          {
            "icon": "08_140_blocked_blind",
            "index": 61330,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "141": [
          {
            "icon": "08_141_blocked_light",
            "index": 61331,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "142": [
          {
            "icon": "08_142_blocked_shortcircuit",
            "index": 61332,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "143": [
          {
            "icon": "08_143_blocked_protect",
            "index": 61333,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "144": [
          {
            "icon": "08_144_blocked_general",
            "index": 61334,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01",
            "tags": [
              "blind-locked",
              "marquee-locked",
              "window-locked",
              "garagedoor-locked"
            ]
          }
        ],
        "145": [
          {
            "icon": "08_145_blocked_cablebreak",
            "index": 61335,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "146": [
          {
            "icon": "08_146_sensor_temperature",
            "index": 61336,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "147": [
          {
            "icon": "08_147_sensor_rain",
            "index": 61337,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "148": [
          {
            "icon": "08_148_sensor_wind",
            "index": 61338,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "149": [
          {
            "icon": "08_149_sensor_sun",
            "index": 61339,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "150": [
          {
            "icon": "08_150_radiator_fan",
            "index": 61340,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "151": [
          {
            "icon": "08_151_heating_fan",
            "index": 61341,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "152": [
          {
            "icon": "08_152_cooling_fan",
            "index": 61342,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "153": [
          {
            "icon": "08_153_heating_cooling_fan",
            "index": 61343,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "154": [
          {
            "icon": "08_154_blind_slat_control",
            "index": 61344,
            "states": [
              "act",
              "dis"
            ],
            "style": "01"
          }
        ],
        "155": [
          {
            "icon": "08_155_home_cooling",
            "index": 61345,
            "states": [
              "def",
              "deact",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "156": [
          {
            "icon": "08_156_home_heating",
            "index": 61346,
            "states": [
              "def",
              "deact",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "157": [
          {
            "icon": "08_157_home_climate",
            "index": 61347,
            "states": [
              "def",
              "deact",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "158": [
          {
            "icon": "08_158_switch_plus",
            "index": 61348,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "deact"
            ],
            "style": "01"
          }
        ],
        "160": [
          {
            "icon": "08_160_group_actuator",
            "index": 61349,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "161": [
          {
            "icon": "08_161_group_sensor",
            "index": 61350,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "162": [
          {
            "icon": "08_162_group_des",
            "index": 61351,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "163": [
          {
            "icon": "08_163_group_lighting",
            "index": 61352,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "164": [
          {
            "icon": "08_164_group_rtc",
            "index": 61353,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "165": [
          {
            "icon": "08_165_group_binary",
            "index": 61354,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "166": [
          {
            "icon": "08_166_group_remain",
            "index": 61355,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "167": [
          {
            "icon": "08_167_group_blind",
            "index": 61356,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "200": [
          {
            "icon": "08_200_solar_power_production",
            "index": 61357,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "201": [
          {
            "icon": "08_201_solar_energy_today",
            "index": 61358,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "202": [
          {
            "icon": "08_202_consumed_power",
            "index": 61359,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "203": [
          {
            "icon": "08_203_consumed_energy_today",
            "index": 61360,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "204": [
          {
            "icon": "08_204_self_consumption",
            "index": 61361,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "205": [
          {
            "icon": "08_205_self_sufficiency",
            "index": 61362,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "206": [
          {
            "icon": "08_206_battery_emperty",
            "index": 61363,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "207": [
          {
            "icon": "08_207_battery_inter",
            "index": 61364,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "208": [
          {
            "icon": "08_208_battery_full",
            "index": 61365,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "209": [
          {
            "icon": "08_209_battery_health",
            "index": 61366,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "210": [
          {
            "icon": "08_210_disarming-plane",
            "index": 61367,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_210_disarming",
            "index": 61368,
            "states": [
              "pending"
            ],
            "style": "01"
          },
          {
            "icon": "08_210_disarming-plane",
            "index": 61369,
            "states": [
              "deact",
              "disarmed"
            ],
            "style": "01.10"
          },
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61370,
            "states": [
              "armed-night"
            ],
            "style": "01"
          },
          {
            "icon": "08_212_arming_internal",
            "index": 61371,
            "states": [
              "armed-home"
            ],
            "style": "01"
          },
          {
            "icon": "08_213_arming_external",
            "index": 61372,
            "states": [
              "armed-away"
            ],
            "style": "01"
          }
        ],
        "211": [
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61373,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_211_pre_alarm_setting-line",
            "index": 61374,
            "states": [
              "pending"
            ],
            "style": "01"
          },
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61375,
            "states": [
              "deact"
            ],
            "style": "01.10"
          }
        ],
        "212": [
          {
            "icon": "08_212_arming_internal",
            "index": 61376,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act",
              "armed-home"
            ],
            "style": "01"
          },
          {
            "icon": "08_212_arming_internal-line",
            "index": 61377,
            "states": [
              "pending"
            ],
            "style": "01"
          },
          {
            "icon": "08_212_arming_internal",
            "index": 61378,
            "states": [
              "deact"
            ],
            "style": "01.10"
          },
          {
            "icon": "08_210_disarming-plane",
            "index": 61379,
            "states": [
              "disarmed"
            ],
            "style": "01"
          },
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61380,
            "states": [
              "armed-night"
            ],
            "style": "01"
          },
          {
            "icon": "08_213_arming_external",
            "index": 61381,
            "states": [
              "armed-away"
            ],
            "style": "01"
          },
          {
            "icon": "08_215_arming_partial",
            "index": 61382,
            "states": [
              "mixed"
            ],
            "style": "01"
          }
        ],
        "213": [
          {
            "icon": "08_213_arming_external",
            "index": 61383,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_213_arming_external-line",
            "index": 61384,
            "states": [
              "pending"
            ],
            "style": "01"
          },
          {
            "icon": "08_213_arming_external",
            "index": 61385,
            "states": [
              "deact"
            ],
            "style": "01.10"
          }
        ],
        "214": [
          {
            "icon": "08_221_centralunit",
            "index": 61386,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_213_arming_external",
            "index": 61387,
            "states": [
              "armed"
            ],
            "style": "01"
          },
          {
            "icon": "08_210_disarming-plane",
            "index": 61388,
            "states": [
              "disarmed"
            ],
            "style": "01"
          },
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61389,
            "states": [
              "armed-pre"
            ],
            "style": "01"
          },
          {
            "icon": "08_212_arming_internal",
            "index": 61390,
            "states": [
              "armed-int"
            ],
            "style": "01"
          },
          {
            "icon": "08_215_arming_partial",
            "index": 61391,
            "states": [
              "mixed"
            ],
            "style": "01"
          }
        ],
        "215": [
          {
            "icon": "08_215_arming_partial",
            "index": 61392,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "216": [
          {
            "icon": "08_216_group_zones",
            "index": 61393,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "217": [
          {
            "icon": "08_217_group_sysdevices",
            "index": 61394,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "218": [
          {
            "icon": "08_218_group_safety",
            "index": 61395,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "219": [
          {
            "icon": "08_219_group_indoorsensor",
            "index": 61396,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "220": [
          {
            "icon": "08_220_group_outdoorsensor",
            "index": 61397,
            "states": [
              "def",
              "foc",
              "prs",
              "dis"
            ],
            "style": "01"
          }
        ],
        "221": [
          {
            "icon": "08_221_centralunit",
            "index": 61398,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "222": [
          {
            "icon": "08_222_siren",
            "index": 61399,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "223": [
          {
            "icon": "08_223_repeaters",
            "index": 61400,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "224": [
          {
            "icon": "08_224_intrusion_keypad",
            "index": 61401,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "225": [
          {
            "icon": "08_225_sensor-smoke",
            "index": 61402,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "226": [
          {
            "icon": "08_226_sensor-gas",
            "index": 61403,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "227": [
          {
            "icon": "08_227_sensor-flood",
            "index": 61404,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "228": [
          {
            "icon": "08_228_sensor-PIR",
            "index": 61405,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "229": [
          {
            "icon": "08_229_sensor-PIR-pet",
            "index": 61406,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "230": [
          {
            "icon": "08_230_sensor-glassbreak",
            "index": 61407,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "231": [
          {
            "icon": "08_231_sensor-magnetic",
            "index": 61408,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "232": [
          {
            "icon": "08_232_sensor-dual-PIR",
            "index": 61409,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "233": [
          {
            "icon": "08_233_sensor-compact-PIR",
            "index": 61410,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "234": [
          {
            "icon": "08_234_sensor-distance-PIR",
            "index": 61411,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "235": [
          {
            "icon": "08_235_sensor-2way-PIR",
            "index": 61412,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "236": [
          {
            "icon": "08_236_group_groups-scenes",
            "index": 61413,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "237": [
          {
            "icon": "08_237_dishwasher-line",
            "index": 61414,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_237_dishwasher-plane",
            "index": 61415,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "238": [
          {
            "icon": "08_238_hob-line",
            "index": 61416,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_238_hob-plane",
            "index": 61417,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "239": [
          {
            "icon": "08_239_oven-line",
            "index": 61418,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_239_oven-plane",
            "index": 61419,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "240": [
          {
            "icon": "08_240_steamoven-line",
            "index": 61420,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_240_steamoven-plane",
            "index": 61421,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "241": [
          {
            "icon": "08_241_coffeesystem-line",
            "index": 61422,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_241_coffeesystem-plane",
            "index": 61423,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "242": [
          {
            "icon": "08_242_fridge-line",
            "index": 61424,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_242_fridge-open",
            "index": 61425,
            "states": [
              "open"
            ],
            "style": "01"
          },
          {
            "icon": "08_242_fridge-plane",
            "index": 61426,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "243": [
          {
            "icon": "08_243_freezer-line",
            "index": 61427,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_243_freezer-plane",
            "index": 61428,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "244": [
          {
            "icon": "08_244_fridge_freezer-line",
            "index": 61429,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_244_fridge_freezer-plane",
            "index": 61430,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "245": [
          {
            "icon": "08_245_hood-line",
            "index": 61431,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_245_hood-plane",
            "index": 61432,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "246": [
          {
            "icon": "08_246_washingmachine-line",
            "index": 61433,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_246_washingmachine-plane",
            "index": 61434,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "247": [
          {
            "icon": "08_247_tumbledryer-line",
            "index": 61435,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_247_tumbledryer-plane",
            "index": 61436,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "248": [
          {
            "icon": "08_248_sonos_group",
            "index": 61437,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01.01"
          }
        ],
        "249": [
          {
            "icon": "08_249_sonos_play",
            "index": 61438,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          },
          {
            "icon": "08_250_sonos_group_play",
            "index": 61439,
            "states": [
              "act+"
            ],
            "style": "01"
          },
          {
            "icon": "08_249_sonos_pause",
            "index": 61440,
            "states": [
              "pause"
            ],
            "style": "01.09"
          },
          {
            "icon": "08_249_sonos_stop",
            "index": 61441,
            "states": [
              "deact"
            ],
            "style": "01"
          }
        ],
        "251": [
          {
            "icon": "08_251_hiphop_rnb-line",
            "index": 61442,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_251_hiphop_rnb-plane",
            "index": 61443,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "252": [
          {
            "icon": "08_252_alternativerock-line",
            "index": 61444,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_252_alternativerock-plane",
            "index": 61445,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "253": [
          {
            "icon": "08_253_latin-line",
            "index": 61446,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_253_latin-plane",
            "index": 61447,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "254": [
          {
            "icon": "08_254_jazz-line",
            "index": 61448,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_254_jazz-plane",
            "index": 61449,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "255": [
          {
            "icon": "08_255_classical-line",
            "index": 61450,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_255_classical-plane",
            "index": 61451,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "256": [
          {
            "icon": "08_256_techno-line",
            "index": 61452,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_256_techno-plane",
            "index": 61453,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "258": [
          {
            "icon": "08_258_romantic-line",
            "index": 61454,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_258_romantic-plane",
            "index": 61455,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "259": [
          {
            "icon": "08_259_kids_party-line",
            "index": 61456,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_259_kids_party-plane",
            "index": 61457,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "260": [
          {
            "icon": "08_260_metal-line",
            "index": 61458,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_260_metal-plane",
            "index": 61459,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "261": [
          {
            "icon": "08_261_window-up",
            "index": 61460,
            "states": [
              "up"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-down",
            "index": 61461,
            "states": [
              "down"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-1",
            "index": 61462,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-1",
            "index": 61463,
            "states": [
              "ani-2"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-1",
            "index": 61464,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-1",
            "index": 61465,
            "states": [
              "ani-4"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-1",
            "index": 61466,
            "states": [
              "ani-5"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_261_window-ani-2",
            "index": 61467,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-unkn",
            "index": 61468,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "262": [
          {
            "icon": "08_262_marquee-up",
            "index": 61469,
            "states": [
              "up"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-down",
            "index": 61470,
            "states": [
              "down"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-1",
            "index": 61471,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-2",
            "index": 61472,
            "states": [
              "ani-2"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-2",
            "index": 61473,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-2",
            "index": 61474,
            "states": [
              "ani-4"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-2",
            "index": 61475,
            "states": [
              "ani-5"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_262_marquee-ani-3",
            "index": 61476,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_003_blind-unkn",
            "index": 61477,
            "states": [
              "unkn"
            ],
            "style": "01"
          }
        ],
        "263": [
          {
            "icon": "08_263_garagedoor-up",
            "index": 61478,
            "states": [
              "up"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-down",
            "index": 61479,
            "states": [
              "down"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-1",
            "index": 61480,
            "states": [
              "ani-1",
              "deact"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-2",
            "index": 61481,
            "states": [
              "ani-2"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-2",
            "index": 61482,
            "states": [
              "ani-3",
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "inter"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-2",
            "index": 61483,
            "states": [
              "ani-4"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-2",
            "index": 61484,
            "states": [
              "ani-5"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          },
          {
            "icon": "08_263_garagedoor-ani-3",
            "index": 61485,
            "states": [
              "ani-6",
              "act"
            ],
            "style": "01",
            "tags": [
              "lock-closed",
              "lock-opened",
              "questionmark"
            ]
          }
        ],
        "270": [
          {
            "icon": "08_270_gowest_4_button-line",
            "index": 61498,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_270_gowest_4_button-plane",
            "index": 61499,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "271": [
          {
            "icon": "08_271_gowest_in_wall_simple-line",
            "index": 61500,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_271_gowest_in_wall_simple-plane",
            "index": 61501,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "272": [
          {
            "icon": "08_272_gowest_in_wall_dimmer-line",
            "index": 61502,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_272_gowest_in_wall_dimmer-plane",
            "index": 61503,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "273": [
          {
            "icon": "08_273_gowest_thermostat-line",
            "index": 61504,
            "states": [
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_273_gowest_thermostat-plane",
            "index": 61505,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "300": [
          {
            "icon": "08_300_mute-line",
            "index": 61486,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "deact"
            ],
            "style": "01"
          },
          {
            "icon": "08_300_mute_muted-plane",
            "index": 61487,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "301": [
          {
            "icon": "08_301_mute-sensor-line",
            "index": 61488,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_301_mute-sensor-plane",
            "index": 61489,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "302": [
          {
            "icon": "08_302_display-switch-sensor-line",
            "index": 61490,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_302_display-switch-sensor-plane",
            "index": 61491,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "303": [
          {
            "icon": "08_303_bell-indicator-sensor-line",
            "index": 61492,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_303_bell-indicator-sensor-plane",
            "index": 61493,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "304": [
          {
            "icon": "08_304_door-open-sensor-line",
            "index": 61494,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "01"
          },
          {
            "icon": "08_304_door-open-sensor-plane",
            "index": 61495,
            "states": [
              "act"
            ],
            "style": "01"
          }
        ],
        "310": [
          {
            "icon": "08_310_additional-actuator",
            "index": 61496,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "311": [
          {
            "icon": "08_311_additional-heating-source",
            "index": 61497,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ]
      },
      "10": {
        "001": [
          {
            "icon": "10_001_rtc_fan",
            "index": 61506,
            "states": [
              "def",
              "act"
            ],
            "style": "02.04"
          }
        ],
        "002": [
          {
            "icon": "10_002_rain",
            "index": 61507,
            "states": [
              "def"
            ],
            "style": "01"
          }
        ],
        "003": [
          {
            "icon": "10_003_wind",
            "index": 61508,
            "states": [
              "def"
            ],
            "style": "01"
          }
        ],
        "004": [
          {
            "icon": "10_004_sun",
            "index": 61509,
            "states": [
              "def"
            ],
            "style": "01"
          }
        ],
        "005": [
          {
            "icon": "10_005_temperature",
            "index": 61510,
            "states": [
              "def",
              "foc"
            ],
            "style": "02"
          }
        ],
        "030": [
          {
            "icon": "10_030_timeline_day",
            "index": 61511,
            "states": [
              "def"
            ],
            "style": "02"
          }
        ],
        "031": [
          {
            "icon": "10_031_timeline_night",
            "index": 61512,
            "states": [
              "def"
            ],
            "style": "02"
          }
        ]
      },
      "12": {
        "001": [
          {
            "icon": "12_001_minus",
            "index": 61513,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "002": [
          {
            "icon": "12_002_plus",
            "index": 61514,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "003": [
          {
            "icon": "12_003_up",
            "index": 61515,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "004": [
          {
            "icon": "12_004_down",
            "index": 61516,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "005": [
          {
            "icon": "12_005_scale_up",
            "index": 61517,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "006": [
          {
            "icon": "12_006_scale_down",
            "index": 61518,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "007": [
          {
            "icon": "12_007_scale_right",
            "index": 61519,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "008": [
          {
            "icon": "12_008_scale_left",
            "index": 61520,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "009": [
          {
            "icon": "12_009_rotate",
            "index": 61521,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "010": [
          {
            "icon": "12_010_fit_in_view",
            "index": 61522,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "011": [
          {
            "icon": "12_011_right",
            "index": 61523,
            "states": [
              "def",
              "dis",
              "act"
            ],
            "style": "02"
          }
        ],
        "012": [
          {
            "icon": "12_012_left",
            "index": 61524,
            "states": [
              "def",
              "dis",
              "act"
            ],
            "style": "02"
          }
        ],
        "013": [
          {
            "icon": "12_013_edit",
            "index": 61525,
            "states": [
              "def",
              "foc"
            ],
            "style": "02"
          }
        ],
        "014": [
          {
            "icon": "12_014_roomshape_L",
            "index": 61526,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "015": [
          {
            "icon": "12_015_roomshape_rect",
            "index": 61527,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "016": [
          {
            "icon": "12_016_attic",
            "index": 61530,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "017": [
          {
            "icon": "12_017_floor",
            "index": 61531,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "018": [
          {
            "icon": "12_018_basement",
            "index": 61532,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "019": [
          {
            "icon": "12_019_rgb_colour_def",
            "index": 61533,
            "states": [
              "def"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-1",
            "index": 61534,
            "states": [
              "foc-1"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-2",
            "index": 61535,
            "states": [
              "foc-2"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-3",
            "index": 61536,
            "states": [
              "foc-3"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-4",
            "index": 61537,
            "states": [
              "foc-4"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-5",
            "index": 61538,
            "states": [
              "foc-5"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-6",
            "index": 61539,
            "states": [
              "foc-6"
            ],
            "style": "13"
          },
          {
            "icon": "12_019_rgb_colour_foc-7",
            "index": 61540,
            "states": [
              "foc-7"
            ],
            "style": "13"
          }
        ],
        "020": [
          {
            "icon": "12_020_rgb_white",
            "index": 61541,
            "states": [
              "def",
              "foc"
            ],
            "style": "02.01"
          }
        ],
        "021": [
          {
            "icon": "12_021_header_houseview",
            "index": 61542,
            "states": [
              "def",
              "foc"
            ],
            "style": "02.01"
          }
        ],
        "040": [
          {
            "icon": "12_040_alerts_precondition",
            "index": 61543,
            "states": [
              "def",
              "dis"
            ],
            "style": "05"
          }
        ],
        "041": [
          {
            "icon": "12_041_alerts_event",
            "index": 61544,
            "states": [
              "def",
              "dis"
            ],
            "style": "05"
          }
        ],
        "042": [
          {
            "icon": "12_042_alerts_action",
            "index": 61545,
            "states": [
              "def",
              "dis"
            ],
            "style": "05"
          }
        ],
        "043": [
          {
            "icon": "12_043_alerts_notification",
            "index": 61546,
            "states": [
              "def",
              "dis"
            ],
            "style": "05"
          }
        ],
        "044": [
          {
            "icon": "12_044_precondition_day",
            "index": 61547,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "045": [
          {
            "icon": "12_045_precondition_night",
            "index": 61548,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "046": [
          {
            "icon": "12_046_precondition_time_periode",
            "index": 61549,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "047": [
          {
            "icon": "12_047_precondition_date_periode",
            "index": 61550,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "048": [
          {
            "icon": "12_048_precondition_temperature",
            "index": 61551,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "049": [
          {
            "icon": "12_049_floor_alignment",
            "index": 61552,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis"
            ],
            "style": "04"
          }
        ],
        "050": [
          {
            "icon": "12_050_precondition_location",
            "index": 61553,
            "states": [
              "def",
              "foc",
              "prs",
              "sel",
              "dis",
              "act"
            ],
            "style": "01"
          }
        ],
        "080": [
          {
            "icon": "12_080_roomshape_P",
            "index": 61528,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "081": [
          {
            "icon": "12_081_roomshape_T",
            "index": 61529,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ]
      },
      "13": {
        "001": [
          {
            "icon": "13_001_alarm",
            "index": 61554,
            "states": [
              "def",
              "act"
            ],
            "style": "05"
          }
        ],
        "002": [
          {
            "icon": "13_002_warning",
            "index": 61555,
            "states": [
              "def",
              "act"
            ],
            "style": "05"
          }
        ],
        "003": [
          {
            "icon": "13_003_info",
            "index": 61556,
            "states": [
              "def",
              "act"
            ],
            "style": "05"
          }
        ],
        "004": [
          {
            "icon": "13_004_confirmation",
            "index": 61557,
            "states": [
              "def",
              "act"
            ],
            "style": "05"
          }
        ],
        "020": [
          {
            "icon": "13_020_identification_switch",
            "index": 61558,
            "states": [
              "def"
            ],
            "style": "05"
          }
        ],
        "021": [
          {
            "icon": "13_021_identification_rtc",
            "index": 61559,
            "states": [
              "def"
            ],
            "style": "05"
          }
        ],
        "022": [
          {
            "icon": "13_022_turn_device",
            "index": 61560,
            "states": [
              "def",
              "act"
            ],
            "style": "05"
          }
        ],
        "024": [
          {
            "icon": "13_024_systemap",
            "index": 61561,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "025": [
          {
            "icon": "13_025_web",
            "index": 61562,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "026": [
          {
            "icon": "13_026_mobile",
            "index": 61563,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ]
      },
      "14": {
        "001": [
          {
            "icon": "14_001_previous",
            "index": 61564,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "002": [
          {
            "icon": "14_002_next",
            "index": 61565,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "003": [
          {
            "icon": "14_003_save",
            "index": 61566,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.02"
          }
        ],
        "004": [
          {
            "icon": "14_004_user",
            "index": 61567,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "005": [
          {
            "icon": "14_005_plus",
            "index": 61568,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "006": [
          {
            "icon": "14_006_edit",
            "index": 61569,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "007": [
          {
            "icon": "14_007_configuration",
            "index": 61570,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "008": [
          {
            "icon": "14_008_application_switching",
            "index": 61571,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ]
      },
      "15": {
        "001": [
          {
            "icon": "15_001_floorplan",
            "index": 61572,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.02"
          }
        ],
        "002": [
          {
            "icon": "15_002_matrix",
            "index": 61573,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.02"
          }
        ],
        "003": [
          {
            "icon": "15_003_configuration",
            "index": 61574,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.02"
          }
        ],
        "004": [
          {
            "icon": "15_004_windowstatus",
            "index": 61575,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "005": [
          {
            "icon": "15_005_sdcard",
            "index": 61576,
            "states": [
              "def"
            ],
            "style": "02"
          }
        ],
        "006": [
          {
            "icon": "15_006_weatherstation",
            "index": 61577,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "007": [
          {
            "icon": "15_007_header_edit",
            "index": 61578,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "008": [
          {
            "icon": "15_008_header_voicecontrol",
            "index": 61579,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "009": [
          {
            "icon": "15_009_header_menu",
            "index": 61580,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "010": [
          {
            "icon": "15_010_header_floorplan",
            "index": 61581,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "011": [
          {
            "icon": "15_011_header_floor_down",
            "index": 61582,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "012": [
          {
            "icon": "15_012_header_floor_up",
            "index": 61583,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "013": [
          {
            "icon": "15_013_header_dashboard",
            "index": 61584,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "014": [
          {
            "icon": "15_014_header_matrix",
            "index": 61585,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "015": [
          {
            "icon": "15_015_header_configuration",
            "index": 61586,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "016": [
          {
            "icon": "15_016_sidebar_dashboard",
            "index": 61587,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "017": [
          {
            "icon": "15_017_sidebar_timer",
            "index": 61588,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "018": [
          {
            "icon": "15_018_menu",
            "index": 61589,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "019": [
          {
            "icon": "15_019_voicecontrol-plane",
            "index": 61590,
            "states": [
              "def",
              "foc",
              "dis",
              "act"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_019_voicecontrol-line",
            "index": 61591,
            "states": [
              "deact"
            ],
            "style": "02.03"
          }
        ],
        "020": [
          {
            "icon": "15_020_login",
            "index": 61592,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "021": [
          {
            "icon": "15_021_access_configuration",
            "index": 61593,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "022": [
          {
            "icon": "15_022_header_sidebar_open",
            "index": 61594,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "023": [
          {
            "icon": "15_023_header_sidebar_close",
            "index": 61595,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "024": [
          {
            "icon": "15_024_header_houseview",
            "index": 61596,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "025": [
          {
            "icon": "15_025_header_cancel",
            "index": 61597,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "026": [
          {
            "icon": "15_026_header_zones",
            "index": 61598,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "030": [
          {
            "icon": "15_030_menu_floorplan",
            "index": 61599,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "031": [
          {
            "icon": "15_031_menu_matrix",
            "index": 61600,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "032": [
          {
            "icon": "15_032_menu_status",
            "index": 61601,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "033": [
          {
            "icon": "15_033_menu_event_history",
            "index": 61602,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "034": [
          {
            "icon": "15_034_menu_voicecontrol",
            "index": 61603,
            "states": [
              "def",
              "dis"
            ],
            "style": "03.01"
          }
        ],
        "035": [
          {
            "icon": "15_035_menu_dashboard",
            "index": 61604,
            "states": [
              "def",
              "dis",
              "act",
              "deact"
            ],
            "style": "03.01"
          }
        ],
        "036": [
          {
            "icon": "15_036_menu_timeprofiles",
            "index": 61605,
            "states": [
              "def",
              "dis",
              "act",
              "deact"
            ],
            "style": "03.01"
          }
        ],
        "047": [
          {
            "icon": "15_047_close",
            "index": 61606,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "048": [
          {
            "icon": "15_048_glossary",
            "index": 61607,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.02"
          }
        ],
        "049": [
          {
            "icon": "15_049_expand",
            "index": 61608,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "050": [
          {
            "icon": "15_050_delete_entry",
            "index": 61609,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02"
          }
        ],
        "051": [
          {
            "icon": "15_051_status_light-plane",
            "index": 61610,
            "states": [
              "act",
              "zone"
            ],
            "style": "10"
          },
          {
            "icon": "15_051_status_light-line",
            "index": 61611,
            "states": [
              "deact",
              "zone-deact"
            ],
            "style": "10"
          }
        ],
        "052": [
          {
            "icon": "15_052_status_window-plane",
            "index": 61612,
            "states": [
              "act"
            ],
            "style": "11"
          },
          {
            "icon": "15_052_status_window-tilt",
            "index": 61613,
            "states": [
              "tilt"
            ],
            "style": "11"
          },
          {
            "icon": "15_052_status_window-line",
            "index": 61614,
            "states": [
              "deact"
            ],
            "style": "11"
          }
        ],
        "053": [
          {
            "icon": "15_053_delete",
            "index": 61615,
            "states": [
              "def",
              "dis"
            ],
            "style": "02"
          }
        ],
        "054": [
          {
            "icon": "15_054_favorite_bubblecontrol-1",
            "index": 61616,
            "states": [
              "def-1"
            ],
            "style": "08"
          },
          {
            "icon": "15_054_favorite_bubblecontrol-2",
            "index": 61617,
            "states": [
              "def-2"
            ],
            "style": "08"
          }
        ],
        "055": [
          {
            "icon": "15_055_event_alarm",
            "index": 61618,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "056": [
          {
            "icon": "15_056_event_warning",
            "index": 61619,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "062": [
          {
            "icon": "15_062_plus",
            "index": 61620,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "063": [
          {
            "icon": "15_063_minus",
            "index": 61621,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "064": [
          {
            "icon": "15_064_fitinview",
            "index": 61622,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "084": [
          {
            "icon": "15_084_rain",
            "index": 61623,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "085": [
          {
            "icon": "15_085_wind",
            "index": 61624,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "086": [
          {
            "icon": "15_086_sun",
            "index": 61625,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "087": [
          {
            "icon": "15_087_temperature",
            "index": 61626,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "088": [
          {
            "icon": "15_088_snow",
            "index": 61627,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "090": [
          {
            "icon": "15_090_back",
            "index": 61628,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "100": [
          {
            "icon": "15_100_filter_lights",
            "index": 61629,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_100_filter_lights-on",
            "index": 61630,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "101": [
          {
            "icon": "15_101_filter_blinds",
            "index": 61631,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_101_filter_blinds-on",
            "index": 61632,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "102": [
          {
            "icon": "15_102_filter_scenes",
            "index": 61633,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_102_filter_scenes-on",
            "index": 61634,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "103": [
          {
            "icon": "15_103_filter_temperature",
            "index": 61635,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_103_filter_temperature-on",
            "index": 61636,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "104": [
          {
            "icon": "15_104_filter_sensors",
            "index": 61637,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_104_filter_sensors-on",
            "index": 61638,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "105": [
          {
            "icon": "15_105_filter_remain",
            "index": 61639,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          },
          {
            "icon": "15_105_filter_remain-on",
            "index": 61640,
            "states": [
              "act"
            ],
            "style": "02.03"
          }
        ],
        "115": [
          {
            "icon": "08_210_disarming-plane",
            "index": 61641,
            "states": [
              "pnl"
            ],
            "style": "03"
          }
        ],
        "116": [
          {
            "icon": "08_211_pre_alarm_setting",
            "index": 61642,
            "states": [
              "pnl"
            ],
            "style": "03"
          }
        ],
        "117": [
          {
            "icon": "08_212_arming_internal",
            "index": 61643,
            "states": [
              "pnl"
            ],
            "style": "03"
          }
        ],
        "118": [
          {
            "icon": "08_213_arming_external",
            "index": 61644,
            "states": [
              "pnl"
            ],
            "style": "03"
          }
        ],
        "119": [
          {
            "icon": "08_055_weatherstation-plane",
            "index": 61645,
            "states": [
              "pnl"
            ],
            "style": "03"
          }
        ],
        "120": [
          {
            "icon": "15_120_solar_power_production",
            "index": 61646,
            "states": [
              "def",
              "pnl",
              "foc"
            ],
            "style": "03"
          }
        ],
        "121": [
          {
            "icon": "15_121_solar_energy_today",
            "index": 61647,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "122": [
          {
            "icon": "15_122_consumed_power",
            "index": 61648,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "123": [
          {
            "icon": "15_123_consumed_energy_today",
            "index": 61649,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "124": [
          {
            "icon": "15_124_self_consumption",
            "index": 61650,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "125": [
          {
            "icon": "15_125_self_sufficiency",
            "index": 61651,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "126": [
          {
            "icon": "15_126_battery_emperty",
            "index": 61652,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "127": [
          {
            "icon": "15_127_battery_inter",
            "index": 61653,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "128": [
          {
            "icon": "15_128_battery_full",
            "index": 61654,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "129": [
          {
            "icon": "15_129_battery_health",
            "index": 61655,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "130": [
          {
            "icon": "15_130_network",
            "index": 61656,
            "states": [
              "def"
            ],
            "style": "03"
          }
        ],
        "131": [
          {
            "icon": "15_131_refresh",
            "index": 61657,
            "states": [
              "def"
            ],
            "style": "03"
          }
        ],
        "132": [
          {
            "icon": "15_132_reset",
            "index": 61658,
            "states": [
              "def"
            ],
            "style": "03"
          }
        ],
        "133": [
          {
            "icon": "15_133_change_password",
            "index": 61659,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "134": [
          {
            "icon": "15_134_report_error",
            "index": 61660,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "135": [
          {
            "icon": "15_135_media_play",
            "index": 61661,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "136": [
          {
            "icon": "15_136_media_pause",
            "index": 61662,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "137": [
          {
            "icon": "15_137_media_stop",
            "index": 61663,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "138": [
          {
            "icon": "15_138_media_previous",
            "index": 61664,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "139": [
          {
            "icon": "15_139_media_next",
            "index": 61665,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "140": [
          {
            "icon": "15_140_media_speaker",
            "index": 61666,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "141": [
          {
            "icon": "15_141_media_speaker-off",
            "index": 61667,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "142": [
          {
            "icon": "15_142_media_audio",
            "index": 61668,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "143": [
          {
            "icon": "15_143_media_shuffle",
            "index": 61669,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "144": [
          {
            "icon": "15_144_media_repeat-all",
            "index": 61670,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "145": [
          {
            "icon": "15_145_media_repeat-one",
            "index": 61671,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "146": [
          {
            "icon": "15_146_menu_switching",
            "index": 61672,
            "states": [
              "def",
              "dis",
              "act",
              "deact"
            ],
            "style": "03.01"
          }
        ],
        "148": [
          {
            "icon": "15_148_media-control",
            "index": 61673,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "149": [
          {
            "icon": "15_149_favorite-playlist",
            "index": 61674,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "150": [
          {
            "icon": "15_150_sonos_group-list",
            "index": 61675,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "160": [
          {
            "icon": "05_012_dropdown_up",
            "index": 61676,
            "states": [
              "prs",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "161": [
          {
            "icon": "05_013_dropdown_down",
            "index": 61677,
            "states": [
              "prs",
              "dis"
            ],
            "style": "03.03"
          }
        ],
        "162": [
          {
            "icon": "06_017_check",
            "index": 61678,
            "states": [
              "def",
              "prs",
              "dis",
              "foc"
            ],
            "style": "03.04"
          }
        ],
        "200": [
          {
            "icon": "15_008_header_voicecontrol",
            "index": 61679,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "201": [
          {
            "icon": "04_010_editmode",
            "index": 61680,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "03"
          }
        ],
        "202": [
          {
            "icon": "15_120_solar_power_production",
            "index": 61681,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "204": [
          {
            "icon": "15_204_house_functions",
            "index": 61682,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "206": [
          {
            "icon": "15_012_header_floor_up",
            "index": 61683,
            "states": [
              "def",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "222": [
          {
            "icon": "02_004_help",
            "index": 61684,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "223": [
          {
            "icon": "15_020_login",
            "index": 61685,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "224": [
          {
            "icon": "15_206_time_and_logic",
            "index": 61686,
            "states": [
              "def",
              "foc",
              "dis"
            ],
            "style": "02.03"
          }
        ],
        "240": [
          {
            "icon": "05_015_checkbox-plane",
            "index": 61687,
            "states": [
              "def-pri-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61688,
            "states": [
              "def-pri-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61689,
            "states": [
              "foc-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61690,
            "states": [
              "foc-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61691,
            "states": [
              "prs-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61692,
            "states": [
              "prs-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-plane",
            "index": 61693,
            "states": [
              "dis-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_015_checkbox-line",
            "index": 61694,
            "states": [
              "dis-2"
            ],
            "style": "14"
          }
        ],
        "241": [
          {
            "icon": "05_016_checkbox-plane",
            "index": 61695,
            "states": [
              "def-pri-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61696,
            "states": [
              "def-pri-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61697,
            "states": [
              "def-pri-3"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61698,
            "states": [
              "foc-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61699,
            "states": [
              "foc-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61700,
            "states": [
              "foc-3"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61701,
            "states": [
              "prs-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61702,
            "states": [
              "prs-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61703,
            "states": [
              "prs-3"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-plane",
            "index": 61704,
            "states": [
              "dis-1"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-line",
            "index": 61705,
            "states": [
              "dis-2"
            ],
            "style": "14"
          },
          {
            "icon": "05_016_checkbox-check",
            "index": 61706,
            "states": [
              "dis-3"
            ],
            "style": "14"
          }
        ],
        "242": [
          {
            "icon": "15_242_marquee-border",
            "index": 61707,
            "states": [
              "def"
            ],
            "style": "01"
          }
        ],
        "243": [
          {
            "icon": "15_243_car",
            "index": 61708,
            "states": [
              "def"
            ],
            "style": "01"
          }
        ],
        "300": [
          {
            "icon": "15_300_bug",
            "index": 61709,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "301": [
          {
            "icon": "15_301_cloud",
            "index": 61710,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "302": [
          {
            "icon": "15_302_geofencing",
            "index": 61711,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "303": [
          {
            "icon": "15_303_imprint",
            "index": 61712,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "304": [
          {
            "icon": "15_304_reset_widget",
            "index": 61713,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ],
        "305": [
          {
            "icon": "15_305_sysaps",
            "index": 61714,
            "states": [
              "def",
              "foc"
            ],
            "style": "03"
          }
        ]
      },
      "20": {
        "007": [
          {
            "icon": "20_007_indicator",
            "index": 61715,
            "states": [
              "def",
              "foc",
              "new",
              "online",
              "offline"
            ],
            "style": "02"
          }
        ],
        "008": [
          {
            "icon": "20_008_indicator_config",
            "index": 61716,
            "states": [
              "def",
              "foc"
            ],
            "style": "12"
          }
        ],
        "100": [
          {
            "icon": "20_100_lock_closed",
            "index": 61717,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "101": [
          {
            "icon": "20_101_lock_opened",
            "index": 61718,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "102": [
          {
            "icon": "20_102_questionmark",
            "index": 61719,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "103": [
          {
            "icon": "20_103_blind_locked",
            "index": 61720,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "104": [
          {
            "icon": "20_104_marquee_locked",
            "index": 61721,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "105": [
          {
            "icon": "20_105_window_locked",
            "index": 61722,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ],
        "106": [
          {
            "icon": "20_106_garagedoor",
            "index": 61723,
            "states": [
              "def"
            ],
            "style": "12"
          }
        ]
      }
    }

function getChar(iconPath) {
    var splitted = iconPath.split("_")
    var char = ""

    if (splitted.length >= 2) {
        char = String.fromCharCode(ICON_MAP[splitted[0]][splitted[1]][0].index)
    }
    return char || iconPath
}
