import QtQuick 2.11
import "iconMap.js" as IconMap

Text {
    property int size: 22
    property string icon: ""

    text: IconMap.getChar(icon)
    width: size
    height: size
    font.family: Style.icons.name
    font.pixelSize: size
}
