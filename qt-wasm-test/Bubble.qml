import QtQuick 2.11
import QtGraphicalEffects 1.0

Rectangle {
    property string iconSource: "08_001_light-line"
    property double glowOpacity: 0  // value in [0,1]

    width: 60
    height: 60
    radius: width / 2
    color: "black"  // mask for gradient

    Image {
        id: img
        anchors.fill: parent
        source: "images/gradient-2.png"
        fillMode: Image.Stretch
        clip: false

        layer.enabled: true
        layer.effect: OpacityMask {
            maskSource: Item {
                width: img.width
                height: img.height
                Rectangle {
                    anchors.centerIn: parent
                    width: img.width
                    height: img.height
                    radius: Math.min(width, height)
                }
            }
        }
    }

//    DropShadow {
//        width: icon.width
//        height: icon.height
//        anchors.centerIn: icon
//        horizontalOffset: 0
//        verticalOffset: 2
//        color: Style.cfShadow
//        source: icon
//        radius: 2
//    }

    FontIcon {
        id: icon
        icon: parent.iconSource
        size: 40
        anchors.centerIn: parent
    }

    ColorOverlay {
        anchors.fill: icon
        source: icon
        color: "white"
    }

    Image {
        id: glow
        clip: false
        opacity: glowOpacity
        source: "icons/glow.png"
        anchors.top: whiteCircle.top
        anchors.right: whiteCircle.right
        anchors.bottom: whiteCircle.bottom
        anchors.left: whiteCircle.left
        anchors.bottomMargin: 16
        fillMode: Image.PreserveAspectCrop

        Behavior on opacity {
            NumberAnimation {
                duration: 250
            }
        }
    }

    Rectangle {
        id: whiteCircle
        width: parent.width
        height: parent.height
        radius: width/2
        color: "transparent"
        border.width: 4
        border.color: "white"
    }
}
