import QtQuick 2.11

Rectangle {
    property string label

    width: 171
    height: 143
    color: "transparent"

    Rectangle {  // Function color bar
        height: 2
        width: parent.width - 1
        color: Qt.rgba(222/255, 232/255, 22/255)
    }

    Rectangle {  // Function name
        width: parent.width
        y: 2
        height: 25
        color: "#2a3c50"

        Label {
            color: Qt.rgba(105/255, 125/255, 150/255)
            font.pixelSize: 14
            anchors.centerIn: parent
            text: "Licht"
        }
    }

    Rectangle {  // Border right
        width: 1
        height: parent.height
        x: parent.width - width
        color: "#293c50"
    }

    Rectangle {  // Border bottom
        width: parent.width
        height: 2
        y: parent.height - height
        color: "#293c50"
    }

    Label {
        text: parent.label
        anchors.horizontalCenter: parent.horizontalCenter
        y: 115
        font.pixelSize: 14
        color: Qt.rgba(152/255, 182/255, 219/255);
    }

    Bubble {
        anchors.centerIn: parent

        MouseArea {
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            pressAndHoldInterval: 250
            onPressAndHold: {
                bc.openAtPos = mapToItem(mainWindow.contentItem, mouse.x, mouse.y)
                bc.open()
            }
        }
    }
}
