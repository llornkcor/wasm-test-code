import QtQuick 2.11

Rectangle {
    property string label

    height: 25
    color: "#28394d"
    width: parent.width

    Label {
        text: parent.label
        anchors.left: parent.left
        anchors.leftMargin: 18
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize: 14
        color: Qt.rgba(105 / 255, 125 / 255, 150 / 255)
    }
}
