
/**
  Maps the value x from the range [fromStart,fromEnd] to the range [toStart,toEnd]
  */
function mapRange(x, fromStart, fromEnd, toStart, toEnd) {
    return toStart + ((toEnd - toStart) / (fromEnd - fromStart)) * (x - fromStart)
}

/**
  Limits the number x to the range [low,high]
  */
function limit(x, low, high) {
    return Math.min(Math.max(x, low), high)
}
