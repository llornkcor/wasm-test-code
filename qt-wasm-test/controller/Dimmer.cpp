#include "Dimmer.h"

DimmerController::DimmerController(QObject *parent) : QObject(parent)
{
    mOn = true;
    mValue = 50;
    mOnValue = 50;
    randomTimer = new QTimer(this);

    //connect(randomTimer, &QTimer::timeout, this, &DimmerController::randomValue);
    //randomTimer->start(5000);
}

int DimmerController::dimmingValue()
{
    return mValue;
}

void DimmerController::setDimmingValue(int value)
{
    int sanitizedValue = value;
    if (sanitizedValue > 100) {
        sanitizedValue = 100;
    }
    else if (sanitizedValue < 0) {
        sanitizedValue = 0;
    }

    if (sanitizedValue != mValue) {
        mValue = sanitizedValue;
        emit dimmingValueChanged();

        if (sanitizedValue == 0) {
            setOn(false);
        } else {
            setOn(true);
        }
    }
}

bool DimmerController::on()
{
    return mOn;
}

void DimmerController::setOn(bool value)
{
    if (value != mOn) {
        mOn = value;
        emit onChanged();

        if (value) {
            setDimmingValue(mOnValue);
        }
        else {
            mOnValue = mValue == 0 ? 10 : mValue;
            setDimmingValue(0);
        }
    }
}

void DimmerController::toggle()
{
    setOn(!mOn);
}

void DimmerController::randomValue()
{
    setDimmingValue(rand() % 60 + 20);
}
