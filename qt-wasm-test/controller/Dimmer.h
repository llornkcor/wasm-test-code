#ifndef DIMMER_CONTROLLER_H
#define DIMMER_CONTROLLER_H

#include <QObject>
#include <QTimer>

class DimmerController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int dimmingValue READ dimmingValue WRITE setDimmingValue NOTIFY dimmingValueChanged)
    Q_PROPERTY(bool on READ on WRITE setOn NOTIFY onChanged)

public:
    explicit DimmerController(QObject *parent = nullptr);

    int dimmingValue();
    void setDimmingValue(int value);

    bool on();
    void setOn(bool value);

    Q_INVOKABLE void toggle();

private:
    int mValue;
    int mOnValue;
    bool mOn;
    QTimer* randomTimer;

signals:
    void dimmingValueChanged();
    void onChanged();

public slots:
    void randomValue();
};

#endif // DIMMER_CONTROLLER_H
