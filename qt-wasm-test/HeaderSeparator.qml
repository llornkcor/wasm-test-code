import QtQuick 2.11
import QtQuick.Controls 2.4

Rectangle {
    width: 2
    color: "transparent"

    Rectangle {
        width: 1
        color: "#13171d"
    }

    Rectangle {
        x: 1
        width: 1
        height: parent.height
        color: "#2e3947"
    }
}
