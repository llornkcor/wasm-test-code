import QtQuick 2.11
import QtQuick.Controls 2.4
import QtGraphicalEffects 1.0

Button {
    id: headerConfigButton
    display: "IconOnly"
    anchors.top: parent.top
    width: 100
    height: 40
    padding: 0

    FontIcon {
        anchors.centerIn: parent
        icon: "15_021_access_configuration"
        size: 38
        color: headerConfigButton.hovered ? "white" : "#6a7c93"
    }

    background: Rectangle {
        color: "transparent"

        Rectangle {  // left outer border
            width: 1
            height: headerConfigButton.height
            color: "#181e26"
        }

        Rectangle {  // left inner border
            x: 1
            width: 1
            height: headerConfigButton.height - 1
            color: "#4d637d"
        }

        Rectangle {  // bottom outer border
            x: 1
            y: headerConfigButton.height - 1
            width: headerConfigButton.width - 2
            height: 1
            color: "#344151"
        }

        Rectangle {  // bottom inner border
            x: 2
            y: headerConfigButton.height - 2
            width: headerConfigButton.width - 4
            height: 1
            color: "#181e26"
        }

        Rectangle {  // right inner border
            x: headerConfigButton.width - 2
            width: 1
            height: headerConfigButton.height - 1
            color: "#181e26"
        }

        Rectangle {  // right outer border
            x: headerConfigButton.width - 1
            width: 1
            height: headerConfigButton.height
            color: "#344151"
        }

        Rectangle {  // gradient fill
            id: headerConfigButtonGradientFill
            x: 2
            width: headerConfigButton.width - 4
            height: headerConfigButton.height - 2

            Image {
                id: img
                anchors.fill: parent
                source: "images/gradient-3.png"
                fillMode: Image.Stretch
            }
        }
    }

    /*
    background: Rectangle {
        color: "transparent"

        Rectangle {
            height: headerConfigButton.height - headerConfigButtonLowerBackground.radius
            width: headerConfigButton.width
        }

        Rectangle {
            id: headerConfigButtonLowerBackground
            radius: 3
            height: headerConfigButton.height
            width: headerConfigButton.width
            color: "#242e3a"
        }

        LinearGradient {
            source: parent
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(0, parent.height)
            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    color: "#344a62"
                }
                GradientStop {
                    position: 1.0
                    color: "#293749"
                }
            }
        }
    }
    */

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
    }
}

/* zwei Runde Ecken unten links und rechts + gradient
        color: "transparent"

        Rectangle {
            height: headerConfigButton.height - headerConfigButtonLowerBackground.radius
            width: headerConfigButton.width
        }

        Rectangle {
            id: headerConfigButtonLowerBackground
            radius: 3
            height: headerConfigButton.height
            width: headerConfigButton.width
            color: "#242e3a"
        }

        LinearGradient {
            source: parent
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(0, parent.height)
            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    color: "#344a62"
                }
                GradientStop {
                    position: 1.0
                    color: "#293749"
                }
            }
        }
  */
