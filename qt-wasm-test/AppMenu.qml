import QtQuick 2.0
import QtQuick.Controls 2.4

Popup {
    width: 350
    height: parent.height
    padding: 0
    modal: true
    background.visible: false

    enter: Transition {
        NumberAnimation { target: appMenuIcon; property: "rotation"; from: 0; to: 180 }
        NumberAnimation { target: foldRect; property: "height"; from: 0; to: parent.height - appMenuToggleButton.height }
    }

    exit: Transition {
        NumberAnimation { target: appMenuIcon; property: "rotation"; from: 180; to: 0 }
        NumberAnimation { target: foldRect; property: "height"; from: height; to: 0 }
    }

    Rectangle {
        id: foldRect
        anchors.top: appMenuToggleButton.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        color: Style.appMenuBackground

        Column {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: appMenuFooter.top

            AppMenuGroupItem { label: qsTr("General") }
            AppMenuItem { label: qsTr("Report bug...") }

            AppMenuGroupItem { label: qsTr("System") + " 'Laborwand Arnsberg'" }
            AppMenuItem { label: qsTr("Change password...") }
            AppMenuItem { label: qsTr("Logout...") }
        }

        Rectangle {
            id: appMenuFooter
            width: parent.width
            height: 50
            anchors.bottom: parent.bottom
            color: parent.color

            Text {
                text: "SysAP v2.2.1"
                font.family: Style.font.name
                font.pixelSize: 12
                color: Style.cfLabel
                anchors.verticalCenter: parent.verticalCenter
                x: 20
            }
        }
    }

    Rectangle {
        id: appMenuToggleButton
        width: parent.width
        height: 50
        color: Style.mainBackground

        FontIcon {
            id: appMenuIcon
            icon: "15_049_expand"
            size: 40
            color: "white"
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 20
        }

        Text {
            font.pixelSize: 20
            font.family: Style.font.name
            color: "white"
            text: qsTr("App Menu")
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: "PointingHandCursor"
            onClicked: appMenu.close()
        }
    }
}
