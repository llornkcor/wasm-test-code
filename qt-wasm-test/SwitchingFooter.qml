import QtQuick 2.11
import QtGraphicalEffects 1.0
Rectangle {
    height: 84
    color: "transparent"

    Row {
        spacing: 20
        anchors.centerIn: parent

        FooterButton {
            iconSource: "15_035_menu_dashboard"

        }
        FooterButton {
            iconSource: "15_030_menu_floorplan"

        }
        FooterButton {
            iconSource: "15_146_menu_switching"
            selected: true
        }
    }
}
