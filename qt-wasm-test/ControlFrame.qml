import QtQuick 2.11
import QtGraphicalEffects 1.0
import com.bj.freeathome.cf 1.0

Rectangle {
    property string label

    id: controlFrame
    width: gradientRect.width + 20
    height: gradientRect.height + 20
    color: "transparent"

    DimmerController {
        id: cfController
    }

    DropShadow {
        width: gradientRect.width
        height: gradientRect.height
        anchors.centerIn: parent
        horizontalOffset: 0
        verticalOffset: 2
        color: Style.cfShadow
        source: gradientRect
    }

    Rectangle {
        id: gradientRect
        anchors.centerIn: parent
        width: 190
        height: 75
        visible: false

        LinearGradient {
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(0, parent.height)
            gradient: Gradient {
                GradientStop { position: 0.0; color: Style.cfGradientTop }
                GradientStop { position: 1.0; color: Style.cfGradientBottom }
            }
        }

        Rectangle {
            width: parent.width
            height: 2
            color: Style.functionLight
        }

        Label {
            color: Style.cfLabel
            text: controlFrame.label
            x: 6
            y: 2
        }

        Label {
            color: Style.cfLabel
            text: cfController.dimmingValue + "%"
            y: 2
            anchors.right: parent.right
            anchors.rightMargin: 6
        }

        Rectangle {  // minus button
            color: Style.cfLabel
            width: 20
            height: 4
            x: 22
            y: 47
        }

        Rectangle {  // plus button
            color: "transparent"
            width: 20
            height: 20
            x: 150
            y: 39

            Rectangle {
                color: Style.cfLabel
                width: parent.width
                height: 4
                anchors.centerIn: parent
            }

            Rectangle {
                color: Style.cfLabel
                width: 4
                height: parent.height
                anchors.centerIn: parent
            }
        }

        FontIcon {
            id: icon
            text: cfController.on ? "\uEEDD" : "\uEEDC"
            // icon: "icons/08_001_light-plane.svg"
            size: 40
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 9

            Behavior on text { PropertyAnimation {duration: 600} }
        }

//        DropShadow {
//            width: icon.width
//            height: icon.height
//            anchors.centerIn: icon
//            horizontalOffset: 0
//            verticalOffset: 2
//            color: Style.cfShadow
//            source: icon
//            radius: 2
//        }

        Image {
            id: glow
            opacity: cfController.dimmingValue / 100
            source: "icons/glow.png"
            anchors.centerIn: gradientRect

            Behavior on opacity {
                NumberAnimation {
                    duration: 250
                }
            }
        }

        RadialGradient {
            anchors.fill: icon
            source: icon
            verticalRadius: horizontalRadius
            horizontalRadius: 0.3 * icon.width
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#FFFFFF"
                }
                GradientStop {
                    position: 0.75
                    color: "#e3e3e3"
                }
                GradientStop {
                    position: 1
                    color: "#C6C6C6"
                }
            }
        }
    }

    MouseArea {
        id: decrease
        height: gradientRect.height
        width: gradientRect.width / 3
        cursorShape: "PointingHandCursor"
        onClicked: cfController.dimmingValue -= 10
    }

    MouseArea {
        id: toggle
        height: gradientRect.height
        width: gradientRect.width / 3
        anchors.left: decrease.right
        anchors.right: increase.left
        cursorShape: "PointingHandCursor"
        onClicked: cfController.toggle()
    }

    MouseArea {
        id: increase
        anchors.right: gradientRect.right
        height: gradientRect.height
        width: gradientRect.width / 3
        cursorShape: "PointingHandCursor"
        onClicked: cfController.dimmingValue += 10
    }
}
