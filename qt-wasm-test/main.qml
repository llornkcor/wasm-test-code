import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.2

Window {
    id: mainWindow
    title: qsTr("Playground")
    minimumWidth: 800
    minimumHeight: 600
    width: 1024
    height: 768
    visible: true
    color: Style.mainBackground

//    BubbleControlDimmer {
//        id: bc
//    }

//    AppMenu {
//        id: appMenu
//    }

    SwitchingHeader {
        width: parent.width
    }

//    Sidebar {
//        id: sidebar
//        width: 320

//        anchors.top: parent.top
//        anchors.bottom: parent.bottom
//        anchors.left: parent.left

//        anchors.topMargin: 50
//        anchors.bottomMargin: 85
//        anchors.leftMargin: 10
//    }

    MatrixView {
        id: matrixView
        visible: true || sidebar.selectedModel.uid !== "0A"

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: sidebar.right

        anchors.topMargin: 50
        anchors.rightMargin: 10
        anchors.bottomMargin: 85
        anchors.leftMargin: 8
    }

    Rectangle {
        id: controlFrames
        color: "transparent"
        visible: false && sidebar.selectedModel.uid === "0A"

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: sidebar.right

        anchors.topMargin: 50
        anchors.rightMargin: 10
        anchors.bottomMargin: 85
        anchors.leftMargin: 8

        ControlFrame {
            anchors.centerIn: parent
            label: "Ⓑ (SMA)"
        }
    }

//    Rectangle {  // Border between main area and footer
//        color: "#171e26"
//        anchors.top: matrixView.bottom
//        anchors.left: parent.left
//        anchors.right: parent.right
//        height: 1
//    }

    SwitchingFooter {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
