import QtQuick 2.11
import "util.js" as Util
import com.bj.freeathome.cf 1.0

Rectangle {
    id: slide

    property DimmerController channelController
    property string labelPostfix: ""

    readonly property bool isDragging: dragArea.pressed

    onVisibleChanged: slide.visible && initialAnimation.start()
    onIsDraggingChanged: isDragging && initialAnimation.stop()

    color: "transparent"

    Binding {
        target: sliderHandle
        property: "y"
        value: Util.mapRange(channelController.dimmingValue, 100, 0, dragArea.drag.minimumY, dragArea.drag.maximumY)
        when: !isDragging
    }

    Binding {
        target: channelController
        property: "dimmingValue"
        value: Util.mapRange(dragArea.mouseY, 30, dragArea.height - 30, 100, 0)
    }

    Rectangle {
        id: dragValueTooltip
        color: Style.slideAreaTooltip
        visible: isDragging
        width: dragText.width + 2 * 18
        height: dragText.height + 2 * 18
        radius: 14

        x: dragArea.mouseX - width / 2
        y: dragArea.mouseY - height / 2 - 70

        Text {
            id: dragText
            color: Style.fgS09
            font.family: Style.font.name
            font.pixelSize: 28
            text: channelController.dimmingValue + labelPostfix
            anchors.centerIn: parent
        }
    }

    Rectangle {
        color: "transparent"
        anchors.fill: parent
        clip: false

        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: sliderHandle.top

            FontIcon {
                id: arrowUp1
                icon: "12_005"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
            FontIcon {
                id: arrowUp2
                icon: "12_005"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
            FontIcon {
                id: arrowUp3
                icon: "12_005"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
        }
        Column {
            id: sliderHandle
            anchors.horizontalCenter: parent.horizontalCenter
            visible: !isDragging

            FontIcon {
                icon: "12_005"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: channelController.dimmingValue < 100 ? 1 : 0
            }

            Text {
                id: labelElement
                text: (Util.limit(Math.round(channelController.dimmingValue), 0, 100).toLocaleString()) + labelPostfix
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter

                font.pixelSize: 28
                font.family: Style.font.name
            }

            FontIcon {
                icon: "12_006"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: channelController.dimmingValue > 0 ? 1 : 0
            }
        }

        Column {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: sliderHandle.bottom

            FontIcon {
                id: arrowDown3
                icon: "12_006"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
            FontIcon {
                id: arrowDown2
                icon: "12_006"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
            FontIcon {
                id: arrowDown1
                icon: "12_006"
                size: 40
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: 0
            }
        }
    }

    // Initial "landing strip" animation
    ParallelAnimation {
        id: initialAnimation
        loops: 3

        onStopped: {
            arrowUp1.opacity = 0
            arrowUp2.opacity = 0
            arrowUp3.opacity = 0
            arrowDown1.opacity = 0
            arrowDown3.opacity = 0
            arrowDown2.opacity = 0
        }

        SequentialAnimation {
            NumberAnimation { target: arrowUp3; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowUp2; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowUp1; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowUp3; property: "opacity"; to: 0.0; duration: 1000 / 6; }
            NumberAnimation { target: arrowUp2; property: "opacity"; to: 0.0; duration: 1000 / 6; }
            NumberAnimation { target: arrowUp1; property: "opacity"; to: 0.0; duration: 1000 / 6; }
        }
        SequentialAnimation {
            NumberAnimation { target: arrowDown3; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowDown2; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowDown1; property: "opacity"; to: 0.3; duration: 1000 / 6; }
            NumberAnimation { target: arrowDown3; property: "opacity"; to: 0.0; duration: 1000 / 6; }
            NumberAnimation { target: arrowDown2; property: "opacity"; to: 0.0; duration: 1000 / 6; }
            NumberAnimation { target: arrowDown1; property: "opacity"; to: 0.0; duration: 1000 / 6; }
        }
    }

    MouseArea {
        id: dragArea
        anchors.fill: parent
        cursorShape: "PointingHandCursor"
        drag.target: sliderHandle
        drag.axis: Drag.YAxis
        drag.minimumY: -30
        drag.maximumY: height - sliderHandle.height + 30
    }
}
