import QtQuick 2.11

Row {
    property string label: ""

    spacing: 4

    Rectangle {
        height: 1
        width: 10
        color: "white"
        anchors.verticalCenter: parent.verticalCenter
    }

    Text {
        anchors.verticalCenter: parent.verticalCenter
        color: "white"
        font.pixelSize: 14
        font.family: Style.font.name
        text: parent.label
    }
}
