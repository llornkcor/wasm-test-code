import QtQuick 2.0

Rectangle {
    property string label

    color: "transparent"
    height: 28
    anchors.left: parent.left
    anchors.right: parent.right

    Rectangle {
        height: 1
        width: parent.width
        color: Style.appMenuItemBorderDark
    }
    Text {
        text: label
        font.pixelSize: 16
        font.family: Style.font.name
        color: Style.cfLabel
        anchors.verticalCenter: parent.verticalCenter
        x: 20
    }
    Rectangle {
        height: 1
        width: parent.width
        y: parent.height - 2
        color: Style.appMenuItemBorderDark
    }
    Rectangle {
        height: 1
        width: parent.width
        y: parent.height - 1
        color: Style.appMenuItemBorderLight
    }
}
