#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/bind.h>

int main(int argc, char *argv[])
{
    EM_ASM(
         document.addEventListener('copy', function(ev){
                                       const text = ev.clipboardData.getData('text/plain');
                                       console.log('Got text: ', text);
                                       ev.clipboardData.setData('text/plain', 'TEST TEST TEST: ' + new Date);
                                       const text2 = ev.clipboardData.getData('text/plain');
                                       console.log('Got text: ', text2);
                                   });

         document.addEventListener('paste', function(ev){
                                  const text = ev.clipboardData.getData('text/plain');
                                  console.log('Got pasted text: ', text);
                              });
   );


    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
