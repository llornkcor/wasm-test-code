#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWindow>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // window
    QWindow * window = new QWindow();
    window->showNormal();
}

void MainWindow::on_pushButton_2_clicked()
{
    //hidden window
    QWindow * window = new QWindow();
    window->setVisible(false);
    window->showNormal();
}

void MainWindow::on_pushButton_3_clicked()
{
    //child
//    QWindow * window = new QWindow(this);
//    window->showNormal();
}
