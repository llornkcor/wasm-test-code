#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <emscripten.h>
#include <emscripten/html5.h>
#include <QProcess>

//#include <stdio.h>
//#include <unistd.h>
//#include <sys/types.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <errno.h>
#include <QDebug>
//#include <fcntl.h>
//#include <unistd.h>
//#include <arpa/inet.h>
//#include <netinet/in.h>
//#include <sys/ioctl.h>
//#include <sys/socket.h>

static const int PORT = 12345;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

QProcess process;

//    int sock0;
//    struct sockaddr_in addr;
//    struct sockaddr_in client;
//    int len;
//    int sock;

//    sock0 = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
//    if (sock0 == -1) {
//      perror("cannot create socket");
////      exit(EXIT_FAILURE);
//    }

//    fcntl(sock0, F_SETFL, O_NONBLOCK);

//    memset(&addr, 0, sizeof(addr));
//    addr.sin_family = AF_INET;
//    addr.sin_port = htons(PORT);

////    addr.sin_addr.s_addr = INADDR_ANY;
//    if (inet_pton(AF_INET, "127.0.0.1", &addr.sin_addr) != 1) {
//       perror("inet_pton failed");
//   //    exit(EXIT_FAILURE);
//     }

//    bind(sock0, (struct sockaddr *)&addr, sizeof(addr));

//    int ret = listen(sock0, 5);
//    if (ret < 0)
//        qDebug() << "ERROR LISTEN" <<  strerror(errno);
//    //        while(true) {
//    printf("Waiting... \n");

//    sock = ::accept(sock0, (struct sockaddr *)&client, (socklen_t *)sizeof(client));

//    ::write(sock, "HELLO", 5);

//    printf("Accepted \n");
//    //      }

//    ::close(sock);

//    ::close(sock0);



        EM_ASM(
                    var http = require('http');
                    var fs = require('fs');

                    // Loading the index file . html displayed to the client
                    var server = http.createServer(function(req, res) {
                        fs.readFile('./index.html', 'utf-8', function(error, content) {
                            res.writeHead(200, {"Content-Type": "text/html"});
                            res.end(content);
                        });
                    });

                    // Loading socket.io
                    var io = require('socket.io').listen(server);

                    // When a client connects, we note it in the console
                    io.sockets.on('connection', function (socket) {
                        console.log('A client is connected!');
                    });


                    server.listen(8080);//                // server.js
//                var Server = require('ws').Server;
//                var port = process.env.PORT || 9030;
//                var ws = new Server({port: port});

//                ws.on('connection', function(w){

//                  w.on('message', function(msg){
//                    console.log('message from client');
//                  });

//                  w.on('close', function() {
//                    console.log('closing connection');
//                  });

//                });

                );
}

MainWindow::~MainWindow()
{
    delete ui;
}
