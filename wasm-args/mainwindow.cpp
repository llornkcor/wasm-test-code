#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList args = QCoreApplication::arguments();
    qWarning() << args;
    ui->textEdit->insertPlainText(QString(args.join(" ")));


}

MainWindow::~MainWindow()
{
    delete ui;
}
