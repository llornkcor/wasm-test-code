#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
      qDebug() << Q_FUNC_INFO << (quintptr)QThread::currentThreadId();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_connectButton_clicked()
{
  qDebug() << Q_FUNC_INFO << (quintptr)QThread::currentThreadId();

    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()),this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this, SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this, SLOT(readyRead()));

    qDebug() << "connecting...";
    QString urlStr = ui->serverLineEdit->text();
    int port = ui->portSpinBox->value();

    // this is not blocking call
    socket->connectToHost(urlStr, port);

    qDebug() << "we need to wait...";
    if (!socket->waitForConnected(5000)) {
        qDebug() << "Error: " << socket->errorString();
    }
}

void MainWindow::on_disconnectButton_clicked()
{

}

void MainWindow::on_sendButton_clicked()
{

}

void MainWindow::on_sendBinaryButton_clicked()
{

}

void MainWindow::on_downloadButton_clicked()
{
    if (socket->isOpen()) {
        QByteArray filename = "GET ";
        filename.append(ui->sendTextEdit->text());
        socket->write(filename);
    } else {
        qDebug() << "Socket is not open. Cannot proceed";
        ui->textEdit->setText("Socket is not open. Cannot proceed");
    }
}


void MainWindow::connected()
{
    qDebug() << "connected...";
    ui->textEdit->setText("connected...");

    // Hey server, tell me about you.
    socket->write("HEAD / HTTP/1.0\r\n\r\n\r\n\r\n");
}

void MainWindow::disconnected()
{
    qDebug() << "disconnected...";
    ui->textEdit->setText("disconnected...");

}

void MainWindow::bytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
    ui->textEdit->setText(" bytes written......");

}

void MainWindow::readyRead()
{
    qDebug() << "reading...";

    // read the data from the socket
    QByteArray data = socket->read(socket->bytesAvailable());

    qDebug() << data.size();
}

void MainWindow::on_clearButton_clicked()
{
        ui->textEdit->clear();
}
