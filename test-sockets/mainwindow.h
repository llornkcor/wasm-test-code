#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void connected();
    void disconnected();
    void bytesWritten(qint64 bytes);
    void readyRead();

    void on_connectButton_clicked();

    void on_disconnectButton_clicked();

    void on_sendButton_clicked();

    void on_sendBinaryButton_clicked();

    void on_downloadButton_clicked();

    void on_clearButton_clicked();

private:
    Ui::MainWindow *ui;

    QTcpSocket *socket;
};
#endif // MAINWINDOW_H
