#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <stdio.h>
#include <string.h>
#include <emscripten/fetch.h>
#include <emscripten.h>
#include <emscripten/html5.h>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    doFetch();
   // doJsFetch();
  //  doOldFetch();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::downloadSucceeded(emscripten_fetch_t *fetch)
{
    qDebug() << Q_FUNC_INFO << QString("Finished downloading %1 bytes from URL %2").arg( fetch->numBytes).arg(fetch->url);

//    printf("Finished downloading %llu bytes from URL %s.\n", fetch->numBytes, fetch->url);
    // The data is now available at fetch->data[0] through fetch->data[fetch->numBytes-1];
    emscripten_fetch_close(fetch); // Free data associated with the fetch.
}
void MainWindow::stateChange(emscripten_fetch_t *fetch)
{
    qDebug() << Q_FUNC_INFO  << "readyState" << fetch->readyState;
    if (fetch->readyState == /*HEADERS_RECEIVED*/ 2) {
        qDebug() << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<< HEADERS >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
        // readyState HEADERS_RECEIVED
        size_t headerLength = emscripten_fetch_get_response_headers_length(fetch);
        char *dst = nullptr;
        emscripten_fetch_get_response_headers(fetch, dst, headerLength + 1);
        std::string str = dst;
// const QString header = QString::fromUtf8(static_cast<char *>(emscripten_fetch_unpack_response_headers(const char *dst)));
//printf("%s\n", mscripten_fetch_unpack_response_headers(dst));
        qDebug() << Q_FUNC_INFO << headerLength << QString::fromStdString(str);

      //  emscripten_fetch_close(fetch);
      // emscripten_fetch_unpack_response_headers(const char *dst); // to array

    }
}

void MainWindow::downloadProgress(emscripten_fetch_t *fetch)
{
    //    printf("Downloading %s.. %.2f%%s complete. HTTP readyState: %d. HTTP status: %d.\n"
    //           "HTTP statusText: %s. Received chunk [%llu, %llu[\n",
    //           fetch->url, fetch->totalBytes > 0 ? (fetch->dataOffset + fetch->numBytes) * 100.0 / fetch->totalBytes : (fetch->dataOffset + fetch->numBytes),
    //           fetch->totalBytes > 0 ? "%" : " bytes",
    //           fetch->readyState, fetch->status, fetch->statusText,
    //           fetch->dataOffset, fetch->dataOffset + fetch->numBytes);

    qDebug() << Q_FUNC_INFO << "url" << fetch->url;
    qDebug() << Q_FUNC_INFO << "complete" << (fetch->totalBytes > 0 ? (fetch->dataOffset + fetch->numBytes) * 100.0 / fetch->totalBytes : (fetch->dataOffset + fetch->numBytes))
             <<  (fetch->totalBytes > 0 ? "%" : " bytes");
    qDebug() << Q_FUNC_INFO  << "status: " << fetch->status << fetch->statusText;
    qDebug() << Q_FUNC_INFO << " Received chunk" << fetch->dataOffset, fetch->dataOffset + fetch->numBytes;

    qDebug() << fetch->__attributes.requestHeaders;



    //if (fetch->readyState == DONE /*4*/) {
    //    qDebug() << fetch->data;
    //}

    // Process the partial data stream fetch->data[0] thru fetch->data[fetch->numBytes-1]
    // This buffer represents the file at offset fetch->dataOffset.
    //    for(size_t i = 0; i < fetch->numBytes; ++i)
    //        ; // Process fetch->data[i];
    //    qDebug() << fetch->data;
}

void MainWindow::downloadFailed(emscripten_fetch_t *fetch)
{
    qDebug() << Q_FUNC_INFO << QString("Downloading %1 failed, HTTP failure status code: %2").arg( fetch->url).arg(fetch->status);
  //printf("Downloading %s failed, HTTP failure status code: %d.\n", fetch->url, fetch->status);
    qDebug() << Q_FUNC_INFO  << "readyState" << fetch->readyState;
    qDebug() << Q_FUNC_INFO  << "status: " << fetch->status << fetch->statusText;

    if (fetch->status >= 400)
        emscripten_fetch_close(fetch); // Also free data on failure.
}

void MainWindow::doFetch()
{
    QString fetchUrl("http://192.168.1.124/target-file-1000M");

    emscripten_fetch_attr_t attr;

//    static const char* custom_headers[3] = {"Content-Type", "application/json", nullptr};
//    attr.requestHeaders = custom_headers;

    emscripten_fetch_attr_init(&attr);
    strcpy(attr.requestMethod, "GET");
//    attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY;
    attr.attributes =  EMSCRIPTEN_FETCH_LOAD_TO_MEMORY | EMSCRIPTEN_FETCH_PERSIST_FILE;
    //EMSCRIPTEN_FETCH_STREAM_DATA ;//| EMSCRIPTEN_FETCH_SYNCHRONOUS;
//    attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY | EMSCRIPTEN_FETCH_APPEND | EMSCRIPTEN_FETCH_STREAM_DATA;
    attr.onsuccess = MainWindow::downloadSucceeded;
    attr.onerror = MainWindow::downloadFailed;
    attr.onprogress = MainWindow::downloadProgress;
    attr.onreadystatechange = MainWindow::stateChange;
    attr.timeoutMSecs = 2 * 6000;
    emscripten_fetch(&attr, fetchUrl.toUtf8());
}

void MainWindow::doJsFetch()
{
    EM_ASM(
                var myHeaders = new Headers({
                  'Content-Type': 'application/json'
                });
            fetch('http://192.168.0.7/smartgit-18_1_4.deb', {
//            fetch('http://192.168.0.21:6931/target-file-1000M', {
                      headers: myHeaders,
                      method: 'GET',
                      mode: 'cors'
                  }).then(function(response) {
                         console.log("Status: "+response.status);     //=> number 100–599
                         console.log(response.statusText); //=> String
                         console.log("Headers: "+response.headers.keys());    //=> Headers
                         console.log("url: "+response.url);        //=> String
                         return response.text();
                  }).then(function(data) {
                       console.log(data.length);
                  }).catch(function(error) {
                       console.log('Looks like there was a problem: \n', error);
                       console.log(error.message );
                 });


                );
}

void MainWindow::doOldFetch()
{
  EM_ASM(
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", "http://192.168.0.7/smartgit-18_1_4.deb", true);
 // xmlhttp.setRequestHeader("Content-Type", "application/json");
 // xmlhttp.send(JSON.stringify({name:"John Rambo", time:"2pm"}));
  xmlhttp.onprogress = function (event) {
    console.log("Loaded: " + event.loaded);
    console.log("Total: " + event.total);
  };
  xmlhttp.send();
              );
}
