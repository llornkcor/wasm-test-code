import QtQuick 2.11
import QtGraphicalEffects 1.0

Rectangle {
    property var roomModel
    property string label
    property bool selected

    width: parent.width
    height: 50
    color: "red"
//    color: "transparent"

    Label {
        id: label
        text: parent.label
        anchors.left: parent.left
        anchors.leftMargin: 18
        anchors.verticalCenter: parent.verticalCenter
        color: selected ? "white" : Qt.rgba(152 / 255, 182 / 255, 219 / 255)
    }

    Rectangle {  // Border top
        height: 5
        width: parent.width
        color: "blue"
    }

    Rectangle {  // Border bottom
        height: 5
        y: parent.height - 5
        width: parent.width
        color: "green"
    }
}
