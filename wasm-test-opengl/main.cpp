#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
#include <emscripten.h>
#include <emscripten/html5.h>

#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    EmscriptenWebGLContextAttributes attributes;
    emscripten_webgl_init_context_attributes(&attributes);

    attributes.preferLowPowerToHighPerformance = false;
    attributes.failIfMajorPerformanceCaveat = false;
    attributes.antialias = true;
    attributes.enableExtensionsByDefault = true;
    attributes.depth = true;
    attributes.stencil = true;
    attributes.majorVersion = 1;
    attributes.minorVersion = 0;

//    assert(emscripten_webgl_get_current_context() == 0);
    EMSCRIPTEN_WEBGL_CONTEXT_HANDLE context = emscripten_webgl_create_context(0, &attributes);
    glGetError();
 //   assert(context > 0);

    if (context)  {
        EMSCRIPTEN_RESULT res = emscripten_webgl_make_context_current(context);
        assert(res == EMSCRIPTEN_RESULT_SUCCESS);
        assert(emscripten_webgl_get_current_context() == context);

        int numExtensions = 0;

        glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
        qDebug() << "GL_VERSION" << (char *)glGetString(GL_VERSION);
        qDebug()  << "GL_VENDOR" << (char *)glGetString(GL_VENDOR);
        qDebug()   << "GL_RENDERER" << (char *)glGetString(GL_RENDERER);
        qDebug()   << "GL_NUM_EXTENSIONS" << numExtensions;

        // Let's try enabling all extensions.

           for (int i = 0; i < numExtensions; ++i) {
             const char *ext = (const char *)glGetStringi(GL_EXTENSIONS, i);
             EM_BOOL supported = emscripten_webgl_enable_extension(context, ext);
             qDebug() << "  " << ext << " is supported?" << supported;
           }
           emscripten_webgl_destroy_context(context);
    } else {
        qDebug() << "context invalid";
    }

//    MainWindow w;
//    w.show();

    return a.exec();
}
