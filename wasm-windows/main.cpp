#include "mainwindow.h"
#include <QApplication>
#include <QLabel>


class Window : public QWidget /*QDialog*/
{
public:
    Window(const QString &str, QWidget *parent = 0): QWidget/*QDialog*/(parent), l(str, this)
    {
    }

private:
    QLabel l;
};


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Window *w1=new Window("w1");
    Window *w2=new Window("w2");
    Window *w3=new Window("w3");

    w1->show();
    w2->show();
    w3->show();


    return a.exec();
}


//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    MainWindow w;
//    w.show();

//    return a.exec();
//}
