// Copyright (C) 2022 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef QWASMRECORDER_H
#define QWASMRECORDER_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API. It exists purely as an
// implementation detail. This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//


#include <QObject>
#include <emscripten.h>
#include <emscripten/val.h>
#include <emscripten/bind.h>

class QWasmRecorder : public QObject
{
    Q_OBJECT
public:
    using QObject::QObject;
    static QWasmRecorder *instance();
    QWasmRecorder();

    void init();
    void setStream(emscripten::val stream);

    emscripten::val m_mediaStream = emscripten::val::undefined();
    static void streamCallback(emscripten::val event);
    static void exceptionCallback(emscripten::val event);
    static void dataAvailableCallback(emscripten::val dataEvent);

    void startStream();
    void stopStream();

private:


};

#endif // QWASMRECORDER_H
