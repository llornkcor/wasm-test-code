#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "qwasmrecorder_p.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    qDebug() << this;
    ui->setupUi(this);
    initAudio();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initAudio()
{
    QWasmRecorder::instance()->init();
}

void MainWindow::on_startAudioButton_clicked()
{
    qDebug() << Q_FUNC_INFO ;

    QWasmRecorder::instance()->startStream();
}

void MainWindow::on_stopButton_clicked()
{
     QWasmRecorder::instance()->stopStream();
}

