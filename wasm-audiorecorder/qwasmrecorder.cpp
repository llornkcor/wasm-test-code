// Copyright (C) 2022 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#include "qwasmrecorder.h"
#include <QtCore>


Q_GLOBAL_STATIC(QWasmRecorder, qwasmAudioRecorder)

QWasmRecorder *QWasmRecorder::instance()
{
    return qwasmAudioRecorder();
}

QWasmRecorder::QWasmRecorder()
{
}

void QWasmRecorder::init()
{
    qDebug() << Q_FUNC_INFO;
    /*
     * navigator.mediaDevices.enumerateDevices(),
     * */
    emscripten::val navigator = emscripten::val::global("navigator");
    emscripten::val mediaDevices = navigator["mediaDevices"];
    if (mediaDevices.isNull() || mediaDevices.isUndefined()) {
        qWarning() << "mediaDevices is busted";
        return;
    }
    emscripten::val allConstraints = mediaDevices.call<emscripten::val>("getSupportedConstraints");
    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", allConstraints);

    emscripten::val contraints = emscripten::val::object();

    emscripten::val audioParams = emscripten::val::object();

    /*     audioParams.set("sampleRate", emscripten::val(41000)); // not work
    audioParams.set("sampleSize", emscripten::val(8)); // not work..
                                                        * seems what hardware default is
                                                        * */
    audioParams.set("channelCount", emscripten::val(2)); // works

    contraints.set("audio", audioParams); // only audio here


    /* prompt user for permissions
     * constraints: audio or video
     * video: { width: 1280, height: 720 }
     *
     * To require a capability, use the keywords min, max, ideal or exact
     * video: { width: { min: 1280}, height: {720 } }
     *
     * video: facingMode,
     * { audio: true, video: { facingMode: "user" } }
     *
     * to specify device: deviceId
     * { video: { deviceId: { exact: myExactCameraOrBustDeviceId } } }
     *
     * MediaDevices.getSupportedConstraints()
     * getCapabilities()
     *
     * to change:
     * let constraints = {
     *     width: 1920,
     *     height: 1080,
     *     aspectRatio: 1.777777778,
     *     frameRate: { max: 30 }
     * };
     * myTrack.applyConstraints(constraints);
     *
     * track.getSettings().facingMode;
     *
     * */
    mediaDevices.call<emscripten::val>("getUserMedia", contraints)
            .call<emscripten::val>("then", emscripten::val::module_property("q_streamCallback"))
            .call<emscripten::val>("catch", emscripten::val::module_property("g_exceptionCallback"));
}

void QWasmRecorder::setStream(emscripten::val stream)
{
    qDebug() << Q_FUNC_INFO << this << "<<<<<<<<<<<<"<< "creating new MediaRecorder object";

    /*
     * MediaRecorder options:
     * mimeType
     * audioBitsOerSecond
     * videoBitsPerSecond // default  2.5Mbps
     * bitsPerSecond
     * */

    emscripten::val tracks = stream.call<emscripten::val>("getTracks");
    emscripten::val caps = tracks[0].call<emscripten::val>("getSettings");

    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", caps);

    emscripten::val mediaOptions = emscripten::val::object();
    mediaOptions.set("sampleRate", emscripten::val(44100));// does not work
    mediaOptions.set("channelCount", emscripten::val(2)); // does not work

    tracks[0].call<emscripten::val>("applyConstraints", mediaOptions)
            .call<emscripten::val>("then", emscripten::val::module_property("qt_trackOptionsApplied"))
            .call<emscripten::val>("catch", emscripten::val::module_property("qt_trackOptionsError"));


    emscripten::val caps2 = tracks[0].call<emscripten::val>("getSettings");
    console.call<void>("log", caps2);

    //    emscripten::val allSettings = stream.call<emscripten::val>("getSettings");
    //    emscripten::val console = emscripten::val::global("console");
    //    console.call<void>("log", allSettings);

    //    emscripten::val audioParams = emscripten::val::object();

    //    audioParams.set("sampleSize", emscripten::val(16));
    //    audioParams.set("channelCount", emscripten::val(2));
    //    audioParams.set("sample", emscripten::val(2));


    m_mediaStream = emscripten::val::global("MediaRecorder").new_(stream);

    m_mediaStream.call<void>("addEventListener", std::string("dataavailable"),
                             emscripten::val::module_property("qt_dataAvailableCallback"),
                             emscripten::val(false));
    m_mediaStream.set("data-context",
                      emscripten::val(quintptr(reinterpret_cast<void *>(this))));
}


void QWasmRecorder::startStream()
{
    qDebug() << Q_FUNC_INFO ;
    if (m_mediaStream.isUndefined() || m_mediaStream.isNull()) {
        qDebug() << Q_FUNC_INFO << "could not find MediaStream";
        return;
    }

    emscripten::val console = emscripten::val::global("console");
    //    emscripten::val tracks = m_mediaStream.call<emscripten::val>("getTracks");
    //        emscripten::val caps2 = tracks[0].call<emscripten::val>("getSettings");
    //        console.call<void>("log", caps2);


    //        emscripten::val allConstraints = m_mediaStream.call<emscripten::val>("getSettings");
    //        console.call<void>("log", allConstraints);


    m_mediaStream.call<void>("start", emscripten::val(2048));
    /* this method can optionally be passed a timeslice argument with a value in milliseconds.
     * If this is specified, the media will be captured in separate chunks of that duration,
     * rather than the default behavior of recording the media in a single large chunk.*/

    console.call<void>("log", m_mediaStream["state"]);

}

void QWasmRecorder::stopStream()
{
    qDebug() << Q_FUNC_INFO ;
    if (m_mediaStream.isUndefined() || m_mediaStream.isNull()) {
        qDebug() << Q_FUNC_INFO << "could not find MediaStream";
        return;
    }
    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", m_mediaStream["state"]);

    m_mediaStream.call<void>("stop");

    console.call<void>("log", m_mediaStream["state"]);
}

void streamCallback(emscripten::val stream)
{
    qDebug() << Q_FUNC_INFO << QString::fromStdString(stream["id"].as<std::string>());

    QWasmRecorder::instance()->setStream(stream);
}

void exceptionCallback(emscripten::val event)
{
    qDebug() << Q_FUNC_INFO <<  event.as<int>();
    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", event["target"]);
}

void dataAvailableCallback(emscripten::val blob)
{
    if (blob.isUndefined() || blob.isNull()) {
        qDebug() << "blob is null";
        return;
    }
    qDebug() << Q_FUNC_INFO << blob["target"]["data-context"].as<quintptr>()
            << blob["data"]["size"].as<int>();

}

void trackOptionsApplied(emscripten::val event)
{
    qDebug() << Q_FUNC_INFO ;
    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", event);

}

void trackOptionsError(emscripten::val event)
{
    qDebug() << Q_FUNC_INFO ;
    emscripten::val console = emscripten::val::global("console");
    console.call<void>("log", event["target"]);

}


EMSCRIPTEN_BINDINGS(camera) {
    function("q_streamCallback", &streamCallback);
    function("g_exceptionCallback", &exceptionCallback);
    function("qt_dataAvailableCallback", &dataAvailableCallback);
    function("qt_trackOptionsApplied", &trackOptionsApplied);
    function("qt_trackOptionsError", &trackOptionsError);
}


