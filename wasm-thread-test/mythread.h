#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QObject>
#include <QThread>

class MyThread
{
public:
    MyThread();
    QThread *thread;
};

#endif // MYTHREAD_H
