#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtWebSockets/QWebSocket>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileDialog>
#include <QtNetwork/QSslCertificate>
#include <QtNetwork/QSslKey>

#include <QWebSocketProtocol>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    useSsl(false)
{
    setupUi(this);
    connect(&webSocket, &QWebSocket::connected, this, &MainWindow::onConnected);
    connect(&webSocket, &QWebSocket::disconnected, this, &MainWindow::closed);
    connect(&webSocket, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error),
        [=](QAbstractSocket::SocketError /*error*/){
          textEdit->insertPlainText("WebSocket error...\n");
          textEdit->insertPlainText(webSocket.errorString() + "\n");
         });
}

MainWindow::~MainWindow()
{
}

void MainWindow::closed()
{
    qDebug() << Q_FUNC_INFO << "WebSocket connected";
    textEdit->insertPlainText("WebSocket closed...\n");
}

void MainWindow::onConnected()
{
    qDebug() << Q_FUNC_INFO << "WebSocket connected";
textEdit->insertPlainText("WebSocket connected...\n");
    connect(&webSocket, &QWebSocket::textMessageReceived,
            this, &MainWindow::onTextMessageReceived);
    connect(&webSocket, &QWebSocket::binaryMessageReceived,
            this, &MainWindow::onBinaryMessageReceived);
}

void MainWindow::onTextMessageReceived(QString message)
{
    qDebug() << "Text Message received:" << message;
//    QJsonDocument doc = QJsonDocument::fromJson(message.toUtf8());

    textEdit->insertPlainText(message + "\n");
    textEdit->ensureCursorVisible();

}

void MainWindow::onBinaryMessageReceived(QByteArray message)
{
    qDebug() << "Binary Message received:" << message;

    textEdit->insertPlainText(message + "\n");
    textEdit->ensureCursorVisible();
}

void MainWindow::on_connectButton_clicked()
{
    QString urlStr = serverLineEdit->text();
    if (urlStr.isEmpty())
        return;
    if (useSsl) {
        qDebug() << Q_FUNC_INFO << "using ssl";
        if (urlStr.left(5) != QStringLiteral("wss://"))
            urlStr.prepend(QStringLiteral("wss://"));

//        QSslConfiguration sslConfiguration;
//        QFile certFile(QStringLiteral(":/localhost.cert"));
//        QFile keyFile(QStringLiteral(":/localhost.key"));
//        certFile.open(QIODevice::ReadOnly);
//        keyFile.open(QIODevice::ReadOnly);
//        QSslCertificate certificate(&certFile, QSsl::Pem);
//        QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);
//        certFile.close();
//        keyFile.close();
//        sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
//        sslConfiguration.setLocalCertificate(certificate);
//        sslConfiguration.setPrivateKey(sslKey);
//        sslConfiguration.setProtocol(QSsl::AnyProtocol);
//        webSocket.setSslConfiguration(sslConfiguration);

    } else {
        if (urlStr.left(5) != QStringLiteral("ws://"))
            urlStr.prepend(QStringLiteral("ws://"));
    }
    qDebug() << Q_FUNC_INFO << urlStr
               << portSpinBox->value();
    url = QUrl(urlStr);
    port = portSpinBox->value();
    url.setPort(port);
    webSocket.open(url);

}

void MainWindow::on_disconnectButton_clicked()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    webSocket.close();
}

void MainWindow::on_sendButton_clicked()
{
    if (webSocket.isValid()) {
        QString msg = sendTextEdit->text();
//        QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8());
//        webSocket.sendBinaryMessage(doc.toBinaryData());
        webSocket.sendTextMessage(msg.toUtf8().data());
        textEdit->insertPlainText("SEND: " + msg + "\n");
    } else {
        textEdit->insertPlainText("SEND: socket not connected...\n");
    }
    sendTextEdit->clear();
}

void MainWindow::on_pushButton_clicked()
{
    QByteArray ba;// = new QByteArray();
    ba.resize(6);
    ba[0] = 0x3c;
    ba[1] = 0xb8;
    ba[2] = 0x64;
    ba[3] = 0x18;
    ba[4] = 0xca;
    ba[5] = 0xca;

    QByteArray bt = QByteArrayLiteral("A binary message");
    qDebug() << Q_FUNC_INFO << ba;
    webSocket.sendBinaryMessage(bt);
    webSocket.sendBinaryMessage(ba);
}

void MainWindow::on_requestButton_clicked()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    QByteArray filename = "GET ";
    filename.append(sendTextEdit->text() + " HTTP/1.0\r\n");
    qDebug() << Q_FUNC_INFO << filename;
//    webSocket.sendTextMessage("HEAD / HTTP/1.0\r\n\r\n\r\n\r\n");

    webSocket.sendTextMessage(filename);
}

void MainWindow::on_pushButton_2_clicked()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    textEdit->clear();
}

void MainWindow::on_pushButton_3_clicked()
{
    if (webSocket.isValid()) {
        auto fileContentReady = [=](const QString &fileName, const QByteArray &fileContent) {
            if (fileName.isEmpty()) {
                // No file was selected
            } else {
                // Use fileName and fileContent
                webSocket.sendBinaryMessage(fileContent);
            }
        };
        QFileDialog::getOpenFileContent("Images (*.png *.xpm *.jpg)",  fileContentReady);
    }
}


void MainWindow::on_sslCheckBox_clicked(bool checked)
{
useSsl = checked;
}
