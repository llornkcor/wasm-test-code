#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWebSockets/QWebSocket>
#include "ui_mainwindow.h"
//namespace Ui {
//class MainWindow;
//}

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QWebSocket webSocket;
    QUrl url;
    qreal port;
    bool useSsl;

private Q_SLOTS:
    void onConnected();
    void closed();

    void onTextMessageReceived(QString message);
    void onBinaryMessageReceived(QByteArray message);
    void on_connectButton_clicked();
    void on_disconnectButton_clicked();
    void on_sendButton_clicked();

    void on_pushButton_clicked();
    void on_requestButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_sslCheckBox_clicked(bool checked);
};

#endif // MAINWINDOW_H
