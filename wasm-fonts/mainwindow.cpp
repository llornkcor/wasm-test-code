#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFontDialog>

#include <QFile>
#include <QFontDatabase>
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    manager = new QNetworkAccessManager(this);
    addResourceFonts();
    qApp->installEventFilter( this );
    connect(ui->textEdit, &QTextEdit::textChanged, [=]() {
        qDebug() << Q_FUNC_INFO
                 << ui->textEdit->toPlainText()
                    << ui->textEdit->toPlainText().unicode()
                       << ui->textEdit->toPlainText().unicode()->unicode()
                        <<  static_cast<Qt::Key>(ui->textEdit->toPlainText().unicode()->unicode());
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // downloa
    qDebug() << Q_FUNC_INFO << __LINE__;
    QNetworkRequest request;
//    request.setUrl(QUrl("http://192.168.1.137/My_Moms_Font.ttf"));
    request.setUrl(QUrl("http://192.168.1.137/wasm/BoutiqueBitmap7x7.ttf"));
//    request.setUrl(QUrl("http://192.168.1.137/SawarabiMincho-Regular.ttf"));

    reply = manager->get(request);

    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(error(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(downloadProgress(qint64, qint64)),
            this, SLOT(updateProgress(qint64, qint64)));
    connect(reply, SIGNAL(finished()),
            this, SLOT(finished()));
}

void MainWindow::on_findFontButton_clicked()
{
    bool ok;
    QFontDialog *fontDialog = new QFontDialog(this);
    connect(fontDialog, &QFontDialog::fontSelected,
            this, &MainWindow::fontSelected);
    fontDialog->show();
}

void MainWindow::error(QNetworkReply::NetworkError err)
{
    qDebug() << Q_FUNC_INFO << reply->errorString();
    reply->deleteLater();
}

void MainWindow::updateProgress(qint64 read, qint64 total)
{
    qDebug() << Q_FUNC_INFO << read << total;
    ui->textEdit->insertPlainText(QString("read %1, total %1\n").arg(read).arg(total));

}

void MainWindow::finished()
{
    ui->textEdit->insertPlainText("Download finished\n");

    // Save the image here
    QByteArray b = reply->readAll();

    if (QFontDatabase::addApplicationFontFromData(b) < 0) {
        qDebug() << Q_FUNC_INFO << "Font not found";
    } else {
    }
    reply->deleteLater();
}

void MainWindow::fontSelected(const QFont &font)
{
    qDebug() << Q_FUNC_INFO;
    ui->textEdit->setFont(font);
}

void MainWindow::addResourceFonts()
{
    int id;
    id = QFontDatabase::addApplicationFont(":/fonts/FreeSans.ttf");
    if ( id < 0) {
        qDebug() << Q_FUNC_INFO << "FreeSans not found";
    }

    id = QFontDatabase::addApplicationFont(":/SawarabiMincho-Regular.ttf");
        if ( id < 0) {
            qDebug() << Q_FUNC_INFO << "SawarabiMincho-Regular not found";
        }
    id = QFontDatabase::addApplicationFont(":/fonts/FreeSansOblique.ttf");
        if ( id < 0) {
        qDebug() << Q_FUNC_INFO << "FreeSansBoldOblique not found";
    }
    id = QFontDatabase::addApplicationFont(":/IconFont.ttf");
        if ( id < 0) {
        qDebug() << Q_FUNC_INFO << "IconFont not found";
    }
    id = QFontDatabase::addApplicationFont(":/fonts/Japanese.ttf");
        if ( id < 0) {
        qDebug() << Q_FUNC_INFO << "Japanese not found";
        } else {
//            QStringList font_families = QFontDatabase::applicationFontFamilies(id);
//            qDebug()<< font_families;
//            ui->textEdit->setFontFamily(font_families.at(0));
     }
//    id = QFontDatabase::addApplicationFont(":/fonts/SourceHanSerif-Regular.otf");
//        if ( id < 0) {
//        qDebug() << Q_FUNC_INFO << "SourceHanSerif-Regular not found";
//    }
}


bool MainWindow::eventFilter(QObject* obj, QEvent* event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        qDebug() << Q_FUNC_INFO
                 << keyEvent->key()
                 << keyEvent->nativeScanCode()
                 << keyEvent->text()
                 << keyEvent->text().unicode()
                 << keyEvent->text().unicode()->unicode();
 //   return true;
    }

    return false;
}
