#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_findFontButton_clicked();

    void error(QNetworkReply::NetworkError err);
    void updateProgress(qint64 read, qint64 total);
    void finished();
    void fontSelected(const QFont &font);
    void addResourceFonts();
private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
     bool eventFilter(QObject* obj, QEvent* event);
};

#endif // MAINWINDOW_H
