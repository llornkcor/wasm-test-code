#include "mainwindow.h"
#include <QApplication>
#include <QLoggingCategory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    QLoggingCategory::setFilterRules("qt.wasm.*=true\n"
//                                     "qt.qpa.input.devices=true");

    MainWindow w;
    w.show();

    return a.exec();
}
