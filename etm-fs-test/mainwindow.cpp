#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QPushButton>
#include <emscripten.h>
#include <QFileInfo>
#include <emscripten/bind.h>
#include <QTimer>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
using namespace emscripten;
int baSize;

//extern "C" {

    void onerror2(void* /*arg*/)
    {
        qDebug() << Q_FUNC_INFO;
    }

    void oncheck(void* arg, int exists)
    {
        qDebug() << Q_FUNC_INFO << (exists ? "exists" : "does not exist");
        if (exists != 0)
            MainWindow::fileFound(arg);
    }
    static void onStore(void */*userData*/)
    {
        qDebug() << Q_FUNC_INFO << __LINE__;
    }

    void onload(void* arg, void* buffer, int sizeBuffer)
    {
        printf("loaded %s\n", buffer);
    //    MainWindow *wasm = reinterpret_cast<MainWindow *>(arg);
        qDebug() << Q_FUNC_INFO << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        qDebug() << Q_FUNC_INFO << sizeof(buffer)<< sizeBuffer << (const char *)buffer;

            QByteArray bFile = QByteArray::fromRawData(( char *)buffer, strlen(( char *)buffer));
    baSize = bFile.size();
            qDebug() << bFile;

            QDir mydir(QDir::homePath() + QLatin1String("/.wincc_oa-cache"));
            QDir homeDir(QDir::homePath());

            if (!mydir.exists()) {
                qDebug() << Q_FUNC_INFO << "mydir" << mydir.absolutePath() << "DOES NOT exist";
                qDebug() << Q_FUNC_INFO << "mkdir";
                if (!homeDir.mkpath(QLatin1String("/.wincc_oa-cache"))) {
                    qDebug() << Q_FUNC_INFO << "mkdir failed: ";
                }
            } else {
                qDebug() << Q_FUNC_INFO << "mydir" << mydir << "exists";
            }

            QFile file(mydir.absolutePath() + "/settings.ini");
            if (file.open(QIODevice::WriteOnly)) {
                file.write(bFile);
                file.close();

            } else {
                qDebug() << "Could not open: "+ mydir.absolutePath() + "/settings.ini" << file.errorString();
            }
            qDebug() << "exists" << QFile(mydir.absolutePath() + "/settings.ini").exists();

            MainWindow::checkFiles();
        }


//}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QCoreApplication::setOrganizationName("llornkcor");
    QCoreApplication::setOrganizationDomain("llornkcor.com");
    QCoreApplication::setApplicationName("test-qsettings");

    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(buttonPushed()));
    connect(ui->pushButton_2,SIGNAL(clicked()),qApp, SLOT(quit()));

 //   MainWindow::checkFiles();
//    bool syncIsDone = emscripten_run_script_int("Module.syncDone");

//    if ( emscripten_run_script_int("Module.syncDone") == 1) {
//            qDebug() << Q_FUNC_INFO << "syncIsDone";
//    } else {
//        qDebug() << Q_FUNC_INFO << "we need a single shit timer....";

//        emscripten_idb_async_exists("llornkcor",
//                                    "/home/web_user/.wincc_oa-cache/settings.ini",
//                                    (void *)this,
//                                    oncheck,
//                                    onerror2);

//        emscripten_idb_async_load("llornkcor",
//                                  "/home/web_user/.wincc_oa-cache/settings.ini",
//                                  (void *)this,
//                                  onload,
//                                  onerror2);

        init();
        //   QTimer::singleShot(1000,this,SLOT(init()));
//    }
}

MainWindow::~MainWindow()
{
//    EM_ASM( FS.syncfs(function(err) { console.log('syncfs finished: err: ' + err); }); ); //save to persist
    delete ui;
}

void MainWindow::fileFound(void *obj)
{
    qDebug() << Q_FUNC_INFO;
    emscripten_idb_async_load("llornkcor",
                              "/home/web_user/.wincc_oa-cache/settings.ini",
                              obj,
                              onload,
                              onerror2);

    MainWindow::checkFiles();
}

void MainWindow::init()
{
  //  qDebug() << Q_FUNC_INFO <<  QDir::homePath() + QLatin1String("/.wincc_oa-cache/");

     EM_ASM({
//console.log(FS.readdir("/"))
 var WebSocketServer = require('ws').Server;

               var server = new WebSocketServer({
                    host: "127.0.0.1",
                    port: "8888"
                    // TODO support backlog
                  });

                 });

    //    EM_ASM(
    //           //create your directory where we keep our persistent data
    //           FS.mkdir('/home/web_user');
    //           //mount persistent directory as IDBFS
    //           FS.mount(IDBFS,{},'/home/web_user');
    //           Module.print("start file sync..");
    //           //flag to check when data are synchronized
    //           Module.syncdone = 0;
    //           //populate persistent_data directory with existing persistent source data
    //          //stored with Indexed Db
    //          //first parameter = "true" mean synchronize from Indexed Db to
    //          //Emscripten file system,
    //          // "false" mean synchronize from Emscripten file system to Indexed Db
    //          //second parameter = function called when data are synchronized
    //          FS.syncfs(true, function(err) {
    //                           assert(!err);
    //                           Module.print("end file sync..");
    //                           Module.syncdone = 1;
    //                        Module.derp();
    //          });
    //    );

//        QString myDirPath = QDir::homePath() + QLatin1String("/.wincc_oa-cache/");

//        qDebug() << "exists" << QFile(myDirPath + "settings.ini").exists();

//        QSettings settings2(myDirPath + "settings.ini",
//                            QSettings::IniFormat);
//        qDebug() << Q_FUNC_INFO << settings2.status() << settings2.value("grant");

}

void MainWindow::printDrive(QDir dir)
{
    dir.setFilter( QDir::NoDotAndDotDot | QDir::NoSymLinks);

    qDebug() << "Scanning: " << dir.path();

    QStringList fileList = dir.entryList();
    for (int i = 0; i < fileList.count(); i++) {
        qDebug() << Q_FUNC_INFO << "Dir: " << fileList.at(i);
    }

    dir.setFilter( QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList dirList = dir.entryList();
    for (int i = 0; i < dirList.size(); ++i) {
        QString newPath = QString("%1/%2").arg(dir.absolutePath()).arg(dirList.at(i));
        printDrive(QDir(newPath));
    }
}

void MainWindow::buttonPushed()
{
    qDebug() << Q_FUNC_INFO << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<";
    QString mydir =  QDir::homePath() + QLatin1String("/.wincc_oa-cache/");
    bool fileExists = QFile(mydir + "/settings.ini").exists();

    qDebug() << Q_FUNC_INFO <<"settings.ini exists:" << QFile(mydir + "/settings.ini").exists();

    if (!fileExists) {
        QSettings settings2(mydir + "/settings.ini", QSettings::IniFormat);

        qDebug() << Q_FUNC_INFO  << "SET" << settings2.status() << settings2.value("grant");

        settings2.setValue("grant","password");
        qDebug() << Q_FUNC_INFO  << settings2.status() << settings2.value("grant");
        settings2.sync();

        QFile setFile(mydir+ "/settings.ini");
        qDebug() << Q_FUNC_INFO << "size:" << setFile.size();
        if (setFile.open(QIODevice::WriteOnly)) {
            QByteArray data = setFile.readAll();

            emscripten_idb_async_store("llornkcor",
                                       "/home/web_user/.wincc_oa-cache/settings.ini",
                                       reinterpret_cast<void *>(data.data()),
                                       data.length(),
                                       reinterpret_cast<void*>(this),
                                       onStore,
                                       onerror2);
            setFile.close();
        }
        //        emscripten_idb_async_exists("llornkcor",
//                                    "/home/web_user/.wincc_oa-cache/settings.ini",
//                                    (void *)this,
//                                    oncheck,
//                                    onerror2);
    } else {
        checkFiles();
    }
}

void MainWindow::checkFiles()
{
    qDebug() << Q_FUNC_INFO << __LINE__;

    emscripten_idb_async_exists("llornkcor",
                                "/home/web_user/.wincc_oa-cache/settings.ini",
                                0,
                                oncheck,
                                onerror2);

    if (QFileInfo(QDir::homePath()).exists()) {
        qDebug() << QDir::homePath() << "exists";

        if (QFileInfo(QDir::homePath()+ QLatin1String("/.wincc_oa-cache")).exists()) {
            qDebug() << Q_FUNC_INFO << ".wincc_oa-cache/ exists";

            if (QFileInfo(QDir::homePath()+ QLatin1String("/.wincc_oa-cache/settings.ini")).exists()) {
                qDebug() << Q_FUNC_INFO << "settings.ini exists";
            } else {
                qDebug() << Q_FUNC_INFO << "settings.ini DOES NOT exist";
            }
        } else {
            qDebug() << Q_FUNC_INFO <<  QDir::homePath() + "/.wincc_oa-cache DOES NOT exist";
        }
    } else {
        qDebug() << QDir::homePath() << "DOES NOT exist";
    }
}

void MainWindow::on_pushButton_3_clicked()
{

//    emscripten_idb_async_load("/home/web_user",
//                               "/home/web_user/.wincc_oa-cache/settings.ini",
//                               obj,
//                               onload,
////                               onerror2);

//    emscripten_idb_async_store("/home/web_user",
//                               "/home/web_user/.wincc_oa-cache/settings.ini",
//                               (void *)this,
//                               oncheck,
//                               onerror2);
}
