#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileInfo>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static void fileFound(void *);
    static void checkFiles();

public slots:
    void buttonPushed();
    void init();
private slots:
    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    void printDrive(QDir dir);

};

#endif // MAINWINDOW_H
