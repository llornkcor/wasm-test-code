#include "socketthread.h"

#include <emscripten.h>
#include <emscripten/bind.h>
#include <QDebug>
//#include <QRunnable>
//#include <QThreadPool>

using namespace emscripten;

class SocketThread;
static void q_onSocketErrorCallback(val event)
{
    val target = event["target"];

    qDebug() << Q_FUNC_INFO << __LINE__;

    SocketThread *wsp = reinterpret_cast<SocketThread*>(target["data-pointercontext"].as<quintptr>());
    Q_ASSERT (wsp);

//    emit wsp->q_func()->error(wsp->error());
}

static void q_onSocketCloseCallback(val event)
{
    val target = event["target"];
    qDebug() << Q_FUNC_INFO << __LINE__;

    SocketThread *wsp = reinterpret_cast<SocketThread*>(target["data-pointercontext"].as<quintptr>());
    Q_ASSERT (wsp);

//    wsp->setSocketState(QAbstractSocket::UnconnectedState);
//    emit wsp->q_func()->disconnected();
}

void q_onSocketOpenCallback(val event)
{
    val target = event["target"];
//#ifdef QNATIVESOCKETENGINE_DEBUG
//#endif
    qDebug() << "readyState" << target["readyState"].as<int>();

    SocketThread *wsp = reinterpret_cast<SocketThread*>(event["data-pointercontext"].as<quintptr>());
    qDebug() << Q_FUNC_INFO << wsp;

//    Q_ASSERT (wsp);
//    wsp->socketOpened();
//    wsp->socketState = QAbstractSocket::ConnectedState;
 //   emit wsp->q_func()->connected();
}

void q_onSocketIncomingMessageCallback(val event)
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    val target = event["target"];
 //   qDebug() << Q_FUNC_INFO << event["data"].typeOf().as<std::string>();

    if (event["data"].typeOf().as<std::string>() == "string") {
//        SocketThread *wsp = reinterpret_cast<SocketThread*>(target["data-context"].as<quintptr>());
//        Q_ASSERT (wsp);

        const QString message = QString::fromStdString(event["data"].as<std::string>());
        qDebug() << Q_FUNC_INFO << message;
//        if (!message.isEmpty())
//            wsp->q_func()->textMessageReceived(message);
    } else {
        qDebug() << Q_FUNC_INFO << __LINE__;
        val reader = val::global("FileReader").new_();
        reader.set("onload", val::module_property("QNativeSocketEnginePrivate_readBlob"));
        reader.set("data-pointercontext", target["data-pointercontext"]);
        reader.call<void>("readAsArrayBuffer", event["data"]);
    }
}

static void q_readSocketBlob(val event)
{
    val fileReader = event["target"];
    qDebug() << Q_FUNC_INFO << __LINE__;

    SocketThread *wsp = reinterpret_cast<SocketThread*>(fileReader["data-context"].as<quintptr>());
    Q_ASSERT (wsp);

    // Set up source typed array
    val result = fileReader["result"]; // ArrayBuffer
    val Uint8Array = val::global("Uint8Array");
    val sourceTypedArray = Uint8Array.new_(result);

    // Allocate and set up destination typed array
    const size_t size = result["byteLength"].as<size_t>();
    QByteArray buffer(size, Qt::Uninitialized);

    val destinationTypedArray = Uint8Array.new_(val::module_property("HEAPU8")["buffer"],
                                                reinterpret_cast<quintptr>(buffer.data()), size);
    destinationTypedArray.call<void>("set", sourceTypedArray);

  //  wsp->q_func()->binaryMessageReceived(buffer);
}

EMSCRIPTEN_BINDINGS(wasm_socketmodule) {
    function("QNativeSocketEngine_onErrorCallback", &q_onSocketErrorCallback);
    function("QNativeSocketEngine_onCloseCallback", &q_onSocketCloseCallback);
    function("QNativeSocketEngine_onOpenCallback", q_onSocketOpenCallback);
    function("QNativeSocketEngine_onIncomingMessageCallback", &q_onSocketIncomingMessageCallback);
    function("QNativeSocketEnginereadBlob", &q_readSocketBlob);
}


//// runs function on a thread from the global QThreadPool, returns a
//// function that can be called to vait for the function to finish.
//std::function<void()> QNativeSocketEngine_runOnThread(std::function<void()> fn, int msecs, bool *timedOut)
//{
//    // This function starts a thread and is asynchronous. Allocate
//    // state/context which whill keep track of the running thread.
//    struct WaitContext {
//        bool done = false;
//        QMutex mutex;
//        QWaitCondition threadDone;
//    };
//    WaitContext *context = new WaitContext();

//    // runs function and signals the waiter when done
//    auto threadFn = [=](){
//        fn();
//        QMutexLocker lock(&context->mutex);
//        context->done = true;
//        context->threadDone.wakeOne();
//    };

//    // wait for done
//    auto waitFn = [=](){
//        {
//            QMutexLocker lock(&context->mutex);
//            if (context->done)
//                return;
//            context->threadDone.wait(&context->mutex);
//        }
//        delete context;
//    };

//    // acquire a thread from the global thread pool
//    class Runnable : public QRunnable
//    {
//    public:
//        Runnable(const std::function<void()> &fn) :m_fn(fn) { }
//        void run() override  { m_fn(); }
//        std::function<void()> m_fn;
//    };
//    QThreadPool::globalInstance()->start(new Runnable(threadFn));

//    return waitFn;
//}

SocketThread::SocketThread(QObject *parent)
{

}

void SocketThread::run()
{
  //  forever {

      //  renderer->render(centerX, centerY, scaleFactor, resultSize, resultDevicePixelRatio);
        startSocket();
//        {
//            qDebug() << Q_FUNC_INFO << __LINE__;
//            QMutexLocker locker(&mutex);
//            if (m_abort)
//                return;
//            if (!restart)
//                condition.wait(&mutex);
//            restart = false;
//            qDebug() << Q_FUNC_INFO << __LINE__;
//        }
    //}
    qDebug() << Q_FUNC_INFO << __LINE__;
}

SocketThread::~SocketThread()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    m_abort = true;
}
void SocketThread::startSocket()
{
    val webSocket = val::global("WebSocket");
qDebug() << Q_FUNC_INFO <<this;
    socketContext = webSocket.new_(/*urlbytes*/(std::string("ws://127.0.0.1:8080")));

    socketContext.set("onerror", val::module_property("QNativeSocketEngine_onErrorCallback"));
    socketContext.set("onclose", val::module_property("QNativeSocketEngine_onCloseCallback"));
    socketContext.set("onopen", val::module_property("QNativeSocketEngine_onOpenCallback"));
    socketContext.set("onmessage", val::module_property("QNativeSocketEngine_onIncomingMessageCallback"));
    socketContext.set("data-pointercontext", val(size_t(reinterpret_cast<void *>(this))));


}

void SocketThread::socketOpened()
{
    emscripten::val state = socketContext["readyState"];
    qDebug() << Q_FUNC_INFO << "readyState:" << state.as<int>();
}
