#ifndef SOCKETTHREAD_H
#define SOCKETTHREAD_H

#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <emscripten/val.h>

QT_BEGIN_NAMESPACE

class SocketThread : public QThread
{
    Q_OBJECT
public:
    SocketThread(QObject *parent = nullptr);
~SocketThread();
    emscripten::val socketContext = emscripten::val::null();


    void run() override;
    void startSocket();
public slots:
    void socketOpened();
private:
    QMutex mutex;
    QWaitCondition condition;

    bool restart;
    bool m_abort;
};

#endif // SOCKETTHREAD_H
