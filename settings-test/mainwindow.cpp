#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>
#include <QTimer>
#include <QDebug>
#include <QDir>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    settings(/*"MySoft", "Star Runner"*/)
{
    ui->setupUi(this);

    //   /home/web_user/.config/MySoft/StarRunner.conf

//    QDir dir("/home/web_user/.config");
//    dir.removeRecursively();
  // settings.clear();
//qDebug() << QCoreApplication::organizationName();
    it = 0;
    //it = settings.value("bogus").toInt();
//    if (settings.status() == QSettings::AccessError) {
//        qDebug() << Q_FUNC_INFO << settings.status();
//    }
    if (settings.isWritable())
        qDebug() << Q_FUNC_INFO << "<<<<<<<<"<< settings.value("bogus").toInt();

    //  //  settings.setValue("bogus", ++it);

    QTimer::singleShot(100,this,SLOT(doRead()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::doRead()
{
    qDebug() << Q_FUNC_INFO << settings.allKeys();

    if (settings.isWritable()) {
    //    if (settings.contains("bogus")) {
            it = settings.value("bogus").toInt();

        qDebug()  << Q_FUNC_INFO << it;

        settings.setValue("bogus", ++it);
//        }

    }

    QTimer::singleShot(1500,this,SLOT(doRead()));
}
