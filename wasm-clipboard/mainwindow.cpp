#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <emscripten.h>
#include <emscripten/html5.h>
#include <emscripten/bind.h>
#include <emscripten/val.h>
#include <QDebug>

using namespace emscripten;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qDebug() << Q_FUNC_INFO << __LINE__;

    val clipboard = val::global("navigator")["clipboard"];
    qDebug() << Q_FUNC_INFO << __LINE__;

    val browserProtocol =  val::global("window")["location"]["protocol"];
    qDebug() << Q_FUNC_INFO << __LINE__;

   bool hasClipboardApi = (!clipboard.isUndefined() && !clipboard["readText"].isUndefined());
    qDebug() << Q_FUNC_INFO << hasClipboardApi;

//    bool hasReadText = clipboard.isUndefined();
//    qDebug() << Q_FUNC_INFO << hasReadText;

//    hasReadText = clipboard["readText"] != val::undefined();
//    qDebug() << Q_FUNC_INFO << hasReadText;

//    hasReadText = clipboard["readText"].isUndefined();

//    EM_ASM(
//         document.addEventListener('copy', function(ev){
//                                       const text = ev.clipboardData.getData('text/plain');
//                                       console.log('Got text: ', text);
//                                       ev.clipboardData.setData('text/plain', 'TEST TEST TEST: ' + new Date);
//                                       const text2 = ev.clipboardData.getData('text/plain');
//                                       console.log('Got text: ', text2);
//                                   });

//         document.addEventListener('paste', function(ev){
//                                  const text = ev.clipboardData.getData('text/plain');
//                                  console.log('Got pasted text: ', text);
//                              });
//   );

}
// ev.preventDefault();
//   Module.pasteClipboard(data);

MainWindow::~MainWindow()
{
    delete ui;
}
