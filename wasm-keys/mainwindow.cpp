#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *key)
{
    if (key->type() == QKeyEvent::KeyPress) {

        qDebug() << "Qt::Key(key->key())";
        qDebug() << Qt::Key(key->key());
        qDebug() << "\nint ev->key()";
        qDebug() <<  key->key();
        qDebug() << "\nint ev->count()";
        qDebug() <<  key->count();
        qDebug() << "\nQFlags ev->modifiers()";

        Qt::KeyboardModifiers mods = key->modifiers();
        qDebug() << "NoModifier          :" << mods.testFlag(Qt::NoModifier);
        qDebug() << "ShiftModifier       :" << mods.testFlag(Qt::ShiftModifier);
        qDebug() << "ControlModifier     :" << mods.testFlag(Qt::ControlModifier);
        qDebug() << "AltModifier         :" << mods.testFlag(Qt::AltModifier);
        qDebug() << "MetaModifier        :" << mods.testFlag(Qt::MetaModifier);
        qDebug() << "KeypadModifier      :" << mods.testFlag(Qt::KeypadModifier);
        qDebug() << "GroupSwitchModifier :" << mods.testFlag(Qt::GroupSwitchModifier);

        qDebug() << "\nQEvent::Type ev->type()\n" << key->type();
        qDebug() << "\nunsigned int ev->nativeModifiers()\n" << key->nativeModifiers();
        qDebug() << "\nunsigned int ev->nativeScanCode()\n" << key->nativeScanCode();
        qDebug() << "\nunsigned int ev->nativeVirtualKey()\n" << key->nativeVirtualKey();
        qDebug() << "\n";

        QStringList msg;

        msg << "Qt::Key(key->key())";
        msg << QString("%1").arg(Qt::Key(key->key()));
        msg << "\nhexs ev->key()";
        msg <<   QString::number( key->key(), 16 );//QString("%1").arg(key->key());
        msg << "\nint ev->count()";
        msg <<   QString("%1").arg(key->count());
        msg << "\nQFlags ev->modifiers()";


        msg << QString("NoModifier          : %1").arg(mods.testFlag(Qt::NoModifier));
        msg << QString("ShiftModifier       : %1" ).arg( mods.testFlag(Qt::ShiftModifier));
        msg << QString("ControlModifier     : %1" ).arg( mods.testFlag(Qt::ControlModifier));
        msg << QString("AltModifier         : %1" ).arg( mods.testFlag(Qt::AltModifier));
        msg << QString("MetaModifier        : %1" ).arg( mods.testFlag(Qt::MetaModifier));
        msg << QString("KeypadModifier      : %1" ).arg( mods.testFlag(Qt::KeypadModifier));
        msg << QString("GroupSwitchModifier : %1" ).arg( mods.testFlag(Qt::GroupSwitchModifier));
        msg << QString("KeyboardModifierMask : %1" ).arg( mods.testFlag(Qt::KeyboardModifierMask));

        msg << QString("\nQEvent::Type ev->type()\n%1" ).arg( key->type());
        msg << QString("\nunsigned int ev->nativeModifiers()\n%1" ).arg( key->nativeModifiers());
        msg << QString("\nunsigned int ev->nativeScanCode()\n%1" ).arg( key->nativeScanCode());
        msg << QString("\nunsigned int ev->nativeVirtualKey()\n%1 ").arg( key->nativeVirtualKey());
        msg << "\n";
//        ui->textEdit->insertPlainText(msg.join("\n"));
//        ui->textEdit->ensureCursorVisible();
    }
}
